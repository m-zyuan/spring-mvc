/*
MySQL Data Transfer
Source Host: 127.0.0.1
Source Database: task
Target Host: 127.0.0.1
Target Database: task
Date: 2014/3/24 0:10:56
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for module
-- ----------------------------
CREATE TABLE `module` (
  `id` int(11) NOT NULL auto_increment COMMENT '节点ID',
  `name` varchar(20) NOT NULL default '' COMMENT '节点名称',
  `icon` varchar(100) default '' COMMENT '节点的图标',
  `file` varchar(100) default '' COMMENT '页面路径或者请求的后台路径',
  `pId` int(11) default '0' COMMENT '父节点',
  `open` int(1) default '0' COMMENT '节点是不是打开',
  `iconOpen` varchar(100) default '' COMMENT '节点打开时的图标',
  `iconClose` varchar(100) default '' COMMENT '节点关闭时的图标',
  `tabIcon` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role
-- ----------------------------
CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role_module
-- ----------------------------
CREATE TABLE `role_module` (
  `id` int(11) NOT NULL auto_increment,
  `role_id` int(11) default NULL,
  `module_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for urls_info
-- ----------------------------
CREATE TABLE `urls_info` (
  `id` int(11) NOT NULL auto_increment,
  `target_url` varchar(512) NOT NULL,
  `source_url` varchar(512) NOT NULL,
  `task_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL auto_increment,
  `account` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `real_name` varchar(255) default NULL,
  `sex` char(1) default NULL,
  `email` varchar(255) default NULL,
  `mobile` varchar(255) default NULL,
  `address` varchar(255) default NULL,
  `birthday` datetime default NULL,
  `company` varchar(255) default NULL,
  `id_number` varchar(255) default NULL,
  `register_date` datetime default NULL,
  `last_login_date` datetime default NULL,
  `error_count` int(11) default '0',
  `last_login_ip` varchar(255) default NULL,
  `remark` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `role_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xiaocai_admin
-- ----------------------------
CREATE TABLE `xiaocai_admin` (
  `id` int(11) NOT NULL auto_increment,
  `admin_name` varchar(50) NOT NULL,
  `admin_password` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xiaocai_charge
-- ----------------------------
CREATE TABLE `xiaocai_charge` (
  `id` int(11) NOT NULL auto_increment,
  `buy_user_id` int(11) NOT NULL,
  `sell_user_id` int(11) NOT NULL,
  `sell_point` double(20,0) NOT NULL,
  `sell_date` date NOT NULL,
  `sell_status` tinyint(4) NOT NULL,
  `deal_date` date default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xiaocai_input
-- ----------------------------
CREATE TABLE `xiaocai_input` (
  `id` int(11) NOT NULL auto_increment,
  `input_money` double(20,2) NOT NULL,
  `input_status` tinyint(4) NOT NULL,
  `user_id_p` int(11) NOT NULL,
  `admin_id_p` int(11) NOT NULL,
  `input_date` date NOT NULL,
  `input_pass_date` date default NULL,
  `input_bak` varchar(500) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xiaocai_urls
-- ----------------------------
CREATE TABLE `xiaocai_urls` (
  `url_id` int(11) NOT NULL auto_increment,
  `user_id_p` int(11) NOT NULL,
  `target_url` varchar(512) NOT NULL,
  `source_urls` varchar(2048) NOT NULL,
  `source_is_jump` tinyint(4) NOT NULL default '0',
  `inner_is_jump` tinyint(4) NOT NULL,
  `pass_is` tinyint(4) NOT NULL,
  `current_point` bigint(20) NOT NULL,
  `time1_ip_count` int(11) NOT NULL default '0',
  `time2_ip_count` int(11) NOT NULL default '0',
  `time3_ip_count` int(11) NOT NULL default '0',
  `time4_ip_count` int(11) NOT NULL default '0',
  `time5_ip_count` int(11) NOT NULL default '0',
  `time6_ip_count` int(11) NOT NULL default '0',
  `time7_ip_count` int(11) NOT NULL default '0',
  `time8_ip_count` int(11) NOT NULL default '0',
  `time9_ip_count` int(11) NOT NULL default '0',
  `time10_ip_count` int(11) NOT NULL default '0',
  `time11_ip_count` int(11) NOT NULL default '0',
  `time12_ip_count` int(11) NOT NULL default '0',
  `time13_ip_count` int(11) NOT NULL default '0',
  `time14_ip_count` int(11) NOT NULL default '0',
  `time15_ip_count` int(11) NOT NULL default '0',
  `time16_ip_count` int(11) NOT NULL default '0',
  `time17_ip_count` int(11) NOT NULL default '0',
  `time18_ip_count` int(11) NOT NULL default '0',
  `time19_ip_count` int(11) NOT NULL default '0',
  `time20_ip_count` int(11) NOT NULL default '0',
  `time21_ip_count` int(11) NOT NULL default '0',
  `time22_ip_count` int(11) NOT NULL default '0',
  `time23_ip_count` int(11) NOT NULL default '0',
  `time24_ip_count` int(11) NOT NULL default '0',
  `finished_time1_ip_count` int(11) NOT NULL default '0',
  `finished_time2_ip_count` int(11) NOT NULL default '0',
  `finished_time3_ip_count` int(11) NOT NULL default '0',
  `finished_time4_ip_count` int(11) NOT NULL default '0',
  `finished_time5_ip_count` int(11) NOT NULL default '0',
  `finished_time6_ip_count` int(11) NOT NULL default '0',
  `finished_time7_ip_count` int(11) NOT NULL default '0',
  `finished_time8_ip_count` int(11) NOT NULL default '0',
  `finished_time9_ip_count` int(11) NOT NULL default '0',
  `finished_time10_ip_count` int(11) NOT NULL default '0',
  `finished_time11_ip_count` int(11) NOT NULL default '0',
  `finished_time12_ip_count` int(11) NOT NULL default '0',
  `finished_time13_ip_count` int(11) NOT NULL default '0',
  `finished_time14_ip_count` int(11) NOT NULL default '0',
  `finished_time15_ip_count` int(11) NOT NULL default '0',
  `finished_time16_ip_count` int(11) NOT NULL default '0',
  `finished_time17_ip_count` int(11) NOT NULL default '0',
  `finished_time18_ip_count` int(11) NOT NULL default '0',
  `finished_time19_ip_count` int(11) NOT NULL default '0',
  `finished_time20_ip_count` int(11) NOT NULL default '0',
  `finished_time21_ip_count` int(11) NOT NULL default '0',
  `finished_time22_ip_count` int(11) NOT NULL default '0',
  `finished_time23_ip_count` int(11) NOT NULL default '0',
  `finished_time24_ip_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`url_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xiaocai_user
-- ----------------------------
CREATE TABLE `xiaocai_user` (
  `id` int(11) NOT NULL auto_increment,
  `user_password` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_qq` varchar(50) default NULL,
  `user_point` bigint(11) NOT NULL,
  `frozen_point` bigint(11) NOT NULL,
  `user_money` double(11,2) NOT NULL,
  `frozen_money` double(11,2) NOT NULL,
  `user_level` int(4) NOT NULL,
  `user_level_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `module` VALUES ('1', '用户管理', '/resources/jquery-zTree-3.5.15/css/zTreeStyle/img/diy/user.png', '/admin/user', '0', '0', null, null, 'icon-user');
INSERT INTO `module` VALUES ('2', '角色管理', '/resources/jquery-zTree-3.5.15/css/zTreeStyle/img/diy/role.png', 'role', '0', '0', '', '', 'icon-role');
INSERT INTO `module` VALUES ('3', '权限管理', '/resources/jquery-zTree-3.5.15/css/zTreeStyle/img/diy/right.png', null, '0', '0', null, null, 'icon-right');
INSERT INTO `module` VALUES ('4', '模块管理', '/resources/jquery-zTree-3.5.15/css/zTreeStyle/img/diy/module.png', null, '0', '0', null, null, 'icon-module');
INSERT INTO `module` VALUES ('5', '系统监控', '/resources/jquery-zTree-3.5.15/css/zTreeStyle/img/diy/monitor.png', null, '0', '0', null, null, 'icon-monitor');
INSERT INTO `module` VALUES ('6', '监控设置', '/resources/jquery-zTree-3.5.15/css/zTreeStyle/img/diy/monitor_setting.png', null, '0', '0', null, null, 'icon-monitor_setting');
INSERT INTO `module` VALUES ('7', 'ttt', '', '', '1', '0', '', '', '');
INSERT INTO `role` VALUES ('1', '超级管理员', '超级管理员');
INSERT INTO `role_module` VALUES ('1', '1', '1');
INSERT INTO `role_module` VALUES ('2', '1', '2');
INSERT INTO `role_module` VALUES ('3', '1', '3');
INSERT INTO `role_module` VALUES ('4', '1', '4');
INSERT INTO `role_module` VALUES ('5', '1', '5');
INSERT INTO `role_module` VALUES ('6', '1', '6');
INSERT INTO `urls_info` VALUES ('1', 'http://www.youyangwang.com', 'http://www.youyangwang.com', '有氧网');
INSERT INTO `user_info` VALUES ('1', 'admin', 'admin', '超级管理员', null, null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `user_info` VALUES ('2', 'chen1eng@dist.co1.cn', '123456', '陈萌', '0', 'chen1eng@dist.co1.cn', '10116', '北京市海淀区苏州街3号大恒科技大厦北座6层', '1900-01-01 00:00:00', '', '010-116321', '2012-12-23 11:13:38', null, '0', null, null);
INSERT INTO `user_info` VALUES ('3', 'info@shineway.co1', '123456', '贾新格', '0', 'info@shineway.co1', '13831193762', '河北省石家庄栾城县城南一公里', '1900-01-01 00:00:00', '', '0282', '2011-09-01 13:59:14', null, '0', null, null);
INSERT INTO `user_info` VALUES ('4', 'guest', '123456', '李锦花', '0', 'guest', '10125', '北京市海淀区蓝靛厂南路25号牛顿办公区南座6层', '1900-01-01 00:00:00', '', '010-125321', '2011-09-01 13:59:15', null, '0', null, null);
INSERT INTO `user_info` VALUES ('5', 'zjl1008@sina.co1', '123456', '张俊莲', '0', 'zjl1008@sina.co1', '10130', '北京市三环东路11号北京中医药大学京港湾宾馆239房间', '1900-01-01 00:00:00', '', '010-130321', '2011-09-01 13:59:16', null, '0', null, null);
INSERT INTO `user_info` VALUES ('6', 'baiguoshouyue@sina.co1', '123456', '曹守月', '0', 'baiguoshouyue@sina.co1', '10142', '北京市丰台区定安东里18号楼4门4031室', '1900-01-01 00:00:00', '', '010-142321', '2011-09-01 13:59:17', null, '0', null, null);
INSERT INTO `user_info` VALUES ('7', 'anna_yang@roiland.co1', '123456', '杨立华', '1', 'anna_yang@roiland.co1', '13942697025', '北京市朝阳区大屯路风林西奥中心A座8D', '1900-01-01 00:00:00', '', '010-186321', '2011-09-01 13:59:18', null, '0', null, null);
INSERT INTO `user_info` VALUES ('8', 'jane.dai@parker.co1', '123456', '戴影兰', '0', 'jane.dai@parker.co1', '21044', '上海金桥出口加工区云桥路280号', '1900-01-01 00:00:00', '', '021-044321', '2011-09-01 13:59:19', null, '0', null, null);
INSERT INTO `user_info` VALUES ('9', 'guest', '123456', '赵国良', '1', 'guest', '21127', '上海市虹口区银欣路38号麒麟大厦607室', '1900-01-01 00:00:00', '', '021-127321', '2011-09-01 13:59:20', null, '0', null, null);
INSERT INTO `user_info` VALUES ('10', 'reception.china@eurother1.co1', '123456', '朱晨弘', '1', 'reception.china@eurother1.co1', '21151', '上海莘庄光华路968号', '1900-01-01 00:00:00', '', '021-151321', '2011-09-01 13:59:21', null, '0', null, null);
INSERT INTO `user_info` VALUES ('11', 'wujiali@scaltex.cn', '123456', '邬佳丽', '0', 'wujiali@scaltex.cn', '21166', '上海市徐汇区虹桥路808号加华上午中心A-8510', '1900-01-01 00:00:00', '', '021-166321', '2011-09-01 13:59:22', null, '0', null, null);
INSERT INTO `user_info` VALUES ('12', 'sunheng@jrseac.co1', '123456', '孙恒', '1', 'sunheng@jrseac.co1', '13918381512', '上海市浦东新区张江中心路218弄66号2楼', '1900-01-01 00:00:00', '', '021-215321', '2011-09-01 13:59:23', null, '0', null, null);
INSERT INTO `user_info` VALUES ('13', 'y.wang@argus.net.cn', '123456', '王莹', '0', 'y.wang@argus.net.cn', '21224', '上海市嘉定区封浜宝园五路301号', '1900-01-01 00:00:00', '', '021-224321', '2011-09-01 13:59:24', null, '0', null, null);
INSERT INTO `user_info` VALUES ('14', 'yvonne.zhang@cn.bureauveritas.co1', '123456', '张彗', '0', 'yvonne.zhang@cn.bureauveritas.co1', '21243', '上海市浦东蛾山路91弄120号浦东软件园8号楼8楼', '1900-01-01 00:00:00', '', '021-243321', '2011-09-01 13:59:25', null, '0', null, null);
INSERT INTO `user_info` VALUES ('15', 'jiangzhaohong@51shsd.co1', '123456', '蒋昭红', '0', 'jiangzhaohong@51shsd.co1', '13801837369', '上海市西康路223号南三楼', '1900-01-01 00:00:00', '', '021-293321', '2011-09-01 13:59:26', null, '0', null, null);
INSERT INTO `user_info` VALUES ('16', 'guest', '123456', '陈瑶', '0', 'guest', '21345', '上海市徐汇区中山西路1800号21楼J1座', '1900-01-01 00:00:00', '', '021-345321', '2011-09-01 13:59:27', null, '0', null, null);
INSERT INTO `user_info` VALUES ('17', 'zhangz@hbsanhuan.co1.cn', '123456', '张峥', '1', 'zhangz@hbsanhuan.co1.cn', '27108', '湖北省武汉市东湖高新区武大科技园', '1900-01-01 00:00:00', '', '027-108321', '2011-09-01 13:59:28', null, '0', null, null);
INSERT INTO `user_info` VALUES ('18', 'sarah_feng@newsoft.co1.cn', '123456', '冯彬玲', '0', 'sarah_feng@newsoft.co1.cn', '29107', '西安市科技二路68号西安软件园汉韵阁B座101', '1900-01-01 00:00:00', '', '029-107321', '2011-09-01 13:59:29', null, '0', null, null);
INSERT INTO `user_info` VALUES ('19', 'ypl@bcstainless.co1', '123456', '叶鹏来', '1', 'ypl@bcstainless.co1', '510106', '江苏省无锡市锡山区北环路118号', '1900-01-01 00:00:00', '', '0510-106321', '2011-09-01 13:59:30', null, '0', null, null);
INSERT INTO `user_info` VALUES ('20', 'chen_xi@cvicse.co1', '123456', '陈曦', '1', 'chen_xi@cvicse.co1', '531101', '山东济南千佛东路41-1号', '1900-01-01 00:00:00', '', '0531-101321', '2011-09-01 13:59:31', null, '0', null, null);
INSERT INTO `user_info` VALUES ('21', 'a1anda@ztex.cn', '123456', '王惠兰', '0', 'a1anda@ztex.cn', '571120', '浙江省杭州市萧山区南阳经济技术开发区', '1900-01-01 00:00:00', '', '0571-120321', '2011-09-01 13:59:32', null, '0', null, null);
INSERT INTO `user_info` VALUES ('22', 'llcx2002@163.co1', '123456', '陈效', '1', 'llcx2002@163.co1', '571124', '杭州市西湖区西溪路128号新湖商务大楼5楼', '1900-01-01 00:00:00', '', '0571-124321', '2011-09-01 13:59:33', null, '0', null, null);
INSERT INTO `user_info` VALUES ('23', 'fenpi8499@sina.co1', '123456', '裴芬', '0', 'fenpi8499@sina.co1', '571125', '杭州市天目山路160号国际花园东11A', '1900-01-01 00:00:00', '', '0571-125321', '2011-09-01 13:59:34', null, '0', null, null);
INSERT INTO `user_info` VALUES ('24', 'cy_qing@163.co1', '123456', '曹阳', '1', 'cy_qing@163.co1', '13770848687', '', '1972-05-06 00:00:00', '', '32010619720506042x', '2012-08-07 13:41:28', null, '0', null, null);
INSERT INTO `user_info` VALUES ('25', 'ge1ini9991@g1ail.co1', '123456', '孙旸', '1', 'ge1ini9991@g1ail.co1', '13764090424', '上海市瑞金二路129弄6号', '1984-05-23 00:00:00', '', '310104198405232812', '2012-08-07 13:41:29', null, '0', null, null);
INSERT INTO `user_info` VALUES ('26', '74jingjing@163.co1', '123456', 'jingjing', '0', '74jingjing@163.co1', '15871377772', '-', '1974-04-03 00:00:00', '', '420105197404030028', '2011-09-22 13:48:41', null, '0', null, null);
INSERT INTO `user_info` VALUES ('27', 'ipavee@163.co1', '123456', '潘国鹏', '1', 'ipavee@163.co1', '13561928865', '山东省日照市北京路中段', '1981-03-19 00:00:00', '', '370683198103193217', '2012-08-08 05:59:17', null, '0', null, null);
INSERT INTO `user_info` VALUES ('28', 'zheng1ing_xu520@sina.co1.cn', '123456', '徐争鸣', '1', 'zheng1ing_xu520@sina.co1.cn', '13396056892', '武汉市珞狮南路517号', '1968-09-07 00:00:00', '', '420106196809074071', '2011-06-02 07:49:01', null, '0', null, null);
INSERT INTO `user_info` VALUES ('29', 'cy_1019@hot1ail.co1', '123456', '陈妍', '0', 'cy_1019@hot1ail.co1', '15942693196', '-', '1983-10-19 00:00:00', '', '210212831019104', '2011-06-02 07:49:02', null, '0', null, null);
INSERT INTO `user_info` VALUES ('30', 'velax_wu@sohu.co1', '123456', '吴晓龙', '1', 'velax_wu@sohu.co1', '13916082774', '-', '1984-05-11 00:00:00', '', '360430198405111111', '2012-08-16 06:16:15', null, '0', null, null);
INSERT INTO `user_info` VALUES ('31', 'zd.wh@163.co1', '123456', '王洵', '1', 'zd.wh@163.co1', '13915503066', '-', '1973-10-21 00:00:00', '', '320502197310210545', '2010-09-07 18:51:17', null, '0', null, null);
INSERT INTO `user_info` VALUES ('32', 'hd6418@hot1ail.co1', '123456', '周建华', '1', 'hd6418@hot1ail.co1', '13817627228', '上海市普陀区石湾路7弄51号503室', '1965-11-30 00:00:00', '', '320602196511302041', '2010-09-07 18:51:18', null, '0', null, null);
INSERT INTO `user_info` VALUES ('33', '228302198@qq.co1', '123456', '王蓉蓉', '0', '228302198@qq.co1', '15809225561', '西安市太白南路181号西部电子社区A-A-405', '1982-12-29 00:00:00', '', '610104198212296167', '2010-09-07 18:51:19', null, '0', null, null);
INSERT INTO `user_info` VALUES ('34', 'hnch@sohu.co1', '123456', '于泳', '0', 'hnch@sohu.co1', '13020151291', '-', '1970-01-30 00:00:00', '', '510212197001300311', '2010-09-07 18:51:20', null, '0', null, null);
INSERT INTO `user_info` VALUES ('35', '1anyuan123@vip.sina.co1', '123456', '袁满', '1', '1anyuan123@vip.sina.co1', '13331119399', '-', '1976-01-23 00:00:00', '', '510104197601230278', '2010-09-07 18:51:21', null, '0', null, null);
INSERT INTO `user_info` VALUES ('36', 'hukunk@yahoo.co1.tw', '123456', '胡坤', '1', 'hukunk@yahoo.co1.tw', '886921881249', '-', '1972-04-05 00:00:00', '', '212959748', '2010-09-07 18:51:22', null, '0', null, null);
INSERT INTO `user_info` VALUES ('37', 'ericshi0907@hot1ail.co1', '123456', '石', '1', 'ericshi0907@hot1ail.co1', '13501880996', '-', '1982-09-07 00:00:00', '', '310105820907209', '2010-09-07 18:51:23', null, '0', null, null);
INSERT INTO `user_info` VALUES ('38', 'zhuweizhun@163.co1', '123456', '朱为准', '1', 'zhuweizhun@163.co1', '15821178545', '-', '1984-03-27 00:00:00', '', '330302198403275618', '2010-09-07 18:51:24', null, '0', null, null);
INSERT INTO `user_info` VALUES ('39', 'guest', '123456', '钟敏', '0', 'guest', '', '', '1984-01-10 00:00:00', '', '310109198401104575', '2012-09-23 06:26:25', null, '0', null, null);
INSERT INTO `user_info` VALUES ('40', 'jsslxhf@sina.co1', '123456', '徐海峰', '1', 'jsslxhf@sina.co1', '13905113599', '-', '1972-04-16 00:00:00', '', '32092219720416241x', '2012-08-04 10:46:57', null, '0', null, null);
INSERT INTO `user_info` VALUES ('42', 'nan2222@sohu.co1', '123456', '种亚男', '1', 'nan2222@sohu.co1', '13438009040', '-', '1978-09-22 00:00:00', '', '110103197809221521', '2012-08-04 10:46:59', null, '0', null, null);
INSERT INTO `user_info` VALUES ('43', '940588@qq.co1', '123456', '唐亮', '1', '940588@qq.co1', '13971010483', '武汉市紫阳路147号', '1983-10-09 00:00:00', '', '340221198310090417', '2012-12-15 13:54:32', null, '0', null, null);
INSERT INTO `user_info` VALUES ('44', 'sunjian33d@126.co1', '123456', '孙剑', '1', 'sunjian33d@126.co1', '13665198208', '无锡市学前东路与江海东路交叉口', '1986-02-13 00:00:00', '', '32118119860213597X', '2012-06-04 06:49:03', null, '0', null, null);
INSERT INTO `user_info` VALUES ('45', 'hxtliyou@163.co1', '123456', 'GG', '1', 'hxtliyou@163.co1', '13300335259', '-', '1986-09-16 00:00:00', '', '420106198609160031', '2012-10-02 15:17:38', null, '0', null, null);
INSERT INTO `user_info` VALUES ('46', 'wanglijunktv@163.co1', '123456', '王礼军', '1', 'wanglijunktv@163.co1', '13586509963', '-', '1984-01-14 00:00:00', '', '33062419840114589X', '2012-12-10 02:33:04', null, '0', null, null);
INSERT INTO `user_info` VALUES ('47', 'co1ent1@163.co1', '123456', '王琴', '0', 'co1ent1@163.co1', '13637971797', '重庆市沙坪坝区马道子102号7-3', '1982-03-17 00:00:00', '', '510215198203178346', '2012-08-03 09:13:04', null, '0', null, null);
INSERT INTO `user_info` VALUES ('48', 'paisn001@to1.co1', '123456', '张奉瑜', '1', 'paisn001@to1.co1', '13561608145', '-', '1972-09-10 00:00:00', '', '370302720910291', '2011-04-20 13:44:34', null, '0', null, null);
INSERT INTO `user_info` VALUES ('49', 'du1eidu1ei@126.co1', '123456', '杜梅', '0', 'du1eidu1ei@126.co1', '13452896906', '重庆市江北区鲤鱼池153号', '1975-11-21 00:00:00', '', '510202197511215528', '2010-09-07 20:09:18', null, '0', null, null);
INSERT INTO `user_info` VALUES ('50', 'sea.gu@163.co1', '123456', '顾蓉', '0', 'sea.gu@163.co1', '13810150511', '-', '1982-11-14 00:00:00', '', '110105198211145429', '2011-01-23 03:08:03', null, '0', null, null);
INSERT INTO `user_info` VALUES ('51', 'guest', '123456', '赵峰', '1', 'guest', '13991934485', '.', '1984-02-14 00:00:00', '', '610103198402140431', '2012-10-21 20:19:37', null, '0', null, null);
INSERT INTO `user_info` VALUES ('52', 'jiaxiaoyan813@163.co1', '123456', '贾晓燕', '0', 'jiaxiaoyan813@163.co1', '13933078983', '-', '1982-08-13 00:00:00', '', '130127198208131028', '2012-05-14 14:21:19', null, '0', null, null);
INSERT INTO `user_info` VALUES ('54', 'eiddejang@126.co1', '123456', '姜楠', '0', 'eiddejang@126.co1', '13998000329', '-', '1982-01-08 00:00:00', '', '210302198201081519', '2010-12-14 17:18:16', null, '0', null, null);
INSERT INTO `user_role` VALUES ('1', '1', '1');
INSERT INTO `xiaocai_admin` VALUES ('1', 'admin', 'admin');
