package com.util;

/**
 * 封装一系列常量
 *
 * @author leaves, QQ：1330771552
 * @since 2012年7月4日22:36:43
 */
public class MsgConst {
    /**
     * 该数据已经被其他用户修改过，请查询最新记录！
     */
    public static String CHECK_VERSION_ERR = "该数据已经被其他用户修改过，请查询最新记录！";

    /**
     * 部分数据被其他用户修改过，请查询最新记录！
     */
    public static String SOME_CHECK_VERSION_ERR = "部分数据被其他用户修改过，请查询最新记录！";

    /**
     * 保存成功
     */
    public static String SAVE_SUCCESS = "保存成功！";

    /**
     * 删除成功
     */
    public static String DELETE_SUCCESS = "删除成功！";
    /**
     * 默认密码（123456）
     */
    public static String DEFAULT_PASS = "12345678";

    /**
     * 系统错误。无法找到数据。
     */
    public static String NO_DATA_ERR = "系统错误。无法找到数据。";

}
