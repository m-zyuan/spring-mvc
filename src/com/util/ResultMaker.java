package com.util;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * 返回json消息的构造器
 *
 * @author leaves, QQ：1330771552
 * @since 2012年7月4日22:36:43
 */

public class ResultMaker {

    /**
     * 前台判断后台操作是否成功的标示
     */
    public final String RESP_SUCCfLG = "success";
    /**
     * 前台接受的处理结果消息的标示
     */
    public final String RESP_MSG = "msg";
    /**
     * form-load的时候用的
     */
    public final String FORM_DATA = "data";

    /**
     * 结果的个数
     */
    public final String  COUNT= "count";

    /**
     * 构建json容器
     */
    private Map<String, Object> jsonOrigMap = new HashMap<String, Object>();

    /**
     * 无参构造方法
     */
    public ResultMaker() {
        jsonOrigMap.put(RESP_SUCCfLG, true);
    }

    /**
     * 构造制造器
     */
    public ResultMaker(boolean success) {
        jsonOrigMap.put(RESP_SUCCfLG, success);
    }

    /**
     * form专用构造方法
     *
     * @param object data
     */
    public ResultMaker(Object object) {
        if (object == null) {
            jsonOrigMap.put(RESP_MSG, new String(MsgConst.NO_DATA_ERR));
            jsonOrigMap.put(RESP_SUCCfLG, false);
        } else {
            jsonOrigMap.put(FORM_DATA, object);
            jsonOrigMap.put(RESP_SUCCfLG, true);
        }
    }

    /**
     * 重置内容
     */
    public ResultMaker reset() {
        jsonOrigMap.clear();
        return this;
    }

    /**
     * 添加json属性<br>
     * 添加的属性不能是success要添加这个属性药调用
     *
     * @param key   json属性
     * @param value json值
     * @return 他的指针？
     */
    public ResultMaker addProperty(String key, Object value) {
        if (!key.equals(RESP_SUCCfLG) && !key.equals(RESP_MSG))
            jsonOrigMap.put(key, value);
        return this;
    }

    /**
     * 设置是否成功
     *
     * @param success 是否成功?
     * @return 他的指针？
     */
    public ResultMaker setSuccess(boolean success) {
        jsonOrigMap.put(RESP_SUCCfLG, success);
        return this;
    }

    /**
     * 检查是否成功
     *
     * @return 是否成功
     */
    public boolean checkSuccess() {
        return jsonOrigMap.get(RESP_SUCCfLG).equals(true);
    }

    /**
     * 设置返回的消息
     *
     * @param msg 返回信息
     * @return 他的指针？
     */
    public ResultMaker setMsg(String msg) {
        jsonOrigMap.put(RESP_MSG, msg);
        return this;
    }

    /**
     * 设置返回的消息
     *
     * @param obj 返回信息
     * @return 他的指针？
     */
    public ResultMaker setData(Object obj) {
        jsonOrigMap.put(FORM_DATA, obj);
        return this;
    }
    /**
     * 设置返回的list的记录的个数
     *
     * @param obj 返回信息
     * @return 他的指针？
     */
    public ResultMaker setCount(int count) {
        jsonOrigMap.put(COUNT, count);
        return this;
    }
    /**
     * 变成json字符串
     */
    public String toString() {
        return JSON.toJSONString(jsonOrigMap, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.PrettyFormat);
    }


}
