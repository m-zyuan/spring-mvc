package com.util;

/**
 * web中使用的常量
 * 
 */
public class WebConstants {
	/** 保存session中的admin用户key */
	public static final String CURRENT_USER = "CURRENT_USER";
	public static final String CURRENT_ADMIN = "CURRENT_ADMIN";
	
	public static final String PERMISSION_Code = "permissionCode";
	public static final String AUTH_CODE = "auth_code";

}
