package com.webservice;

public interface HelloWorld {
	/**
	 * 访问地址:http://localhost/services/HelloWorldWebService/greeting
	 * HelloWorldWebService为service.xml中配置的sevice名称
	 * @param name
	 * @return
	 */
	 public String greeting(String name);
	 public String print();
}
