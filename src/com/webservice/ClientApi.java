package com.webservice;

import java.util.List;

import com.model.XiaocaiResultMsg;
import com.webservice.model.XiaocaiChargeApi;
import com.webservice.model.XiaocaiInputApi;
import com.webservice.model.XiaocaiNoticeApi;
import com.webservice.model.XiaocaiSoftUpdate;
import com.webservice.model.XiaocaiUrlsApi;
import com.webservice.model.XiaocaiUserApi;
import com.webservice.model.XiaocaiUserLogApi;

public interface ClientApi {
	//用户登录
	public XiaocaiResultMsg login(String userName,String passWord);
	//获取用户信息
	public XiaocaiUserApi getUserInfo(String userName,String passWord);
	//获取网站公告
	public List<XiaocaiNoticeApi> getWebNotice(int page);
	//获取可以刷的网址
	public List<XiaocaiUrlsApi> getXiaocaiUrlsByUserId(long userId);
	//用户加分
	public boolean addPoint(String userName,String passWord,List<Long> urlId,String extCode);
	//用户留言(直接发邮件给配置的用户即可)
	public XiaocaiResultMsg leaveMessage(String userName,String passWord,String message);
	//获取扩展密码
	public String getExtCodeByUserId(String userName,String passWord);
	//用户注册
	public XiaocaiResultMsg reg(XiaocaiUserApi xiaocaiUserApi);
	//我的推广
	public List<XiaocaiUserApi> getMyRefer(String userName,String passWord,int page);
	//修改密码
	public XiaocaiResultMsg changePwd(String userName,String passWord,String newPassword);
	//交易记录
	public List<XiaocaiChargeApi> getMyChargeList(String userName,String passWord,int page);
	//用户提现
	public XiaocaiResultMsg payOut(String userName,String passWord,float payOutMoney);
	//用户充值表单提交
	public XiaocaiResultMsg pay(String userName,String passWord,float payMoney);
	//用户财务明细
	public List<XiaocaiInputApi> money(String userName,String passWord,int page);
	//用户购买积分
	public XiaocaiResultMsg pointBuy(String userName,String passWord,float point);
	//用户兑换积分
	public XiaocaiResultMsg exchange(String userName,String passWord,float point);
	//用户转移积分
	public XiaocaiResultMsg movePoint(String userName,String passWord,String acceptUserName,float point);
	//用户确认积分转移
	public XiaocaiResultMsg acceptMovePoint(String userName,String passWord,long chargeId);
	
	//用户添加url
	public XiaocaiResultMsg addUrl(String userName,String passWord,XiaocaiUrlsApi xiaocaiUrlsApi);
	//获取url信息
	public XiaocaiUrlsApi getXiaocaiUrlsApiInfo(String userName, String passWord,long urlId);
	//编辑url
	public XiaocaiResultMsg editXiaoCaiUrls(String userName, String passWord,XiaocaiUrlsApi xiaocaiUrlsApi);
	//删除url
	public XiaocaiResultMsg delXiaoCaiUrls(String userName, String passWord,int urlId);
	//我的urls
	public List<XiaocaiUrlsApi> myXiaocaiUrls(String userName,String passWord,int page);
	//查询操作日志
	public List<XiaocaiUserLogApi> myXiaocaiUserLog(String userName,String passWord,int page);
	//获取要升级的文件名字及其路径
	public List<XiaocaiSoftUpdate> getXiaocaiSoftUpdate();
	//获取当前最新软件版本号
	public double getSoftVersion();
}
