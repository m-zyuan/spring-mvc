package com.webservice.model;

import java.util.Date;

public class XiaocaiChargeApi {
	private int id;
	private long buyUserId;//积分购买人ID
	private long sellUserId;//积分出售人ID
	private float chargePoint;//出售积分数
	private float chargeMoney;//交易金额
	private Date sellDate;//出售时间
	private int sellStatus;//出售状态
	private Date dealDate;//积分交易处理时间
	private int chargeType;//交易类型（购买还是出售，0为出售，1为购买）
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getBuyUserId() {
		return buyUserId;
	}
	public void setBuyUserId(long buyUserId) {
		this.buyUserId = buyUserId;
	}
	public long getSellUserId() {
		return sellUserId;
	}
	public void setSellUserId(long sellUserId) {
		this.sellUserId = sellUserId;
	}
	public float getChargePoint() {
		return chargePoint;
	}
	public void setChargePoint(float chargePoint) {
		this.chargePoint = chargePoint;
	}
	public float getChargeMoney() {
		return chargeMoney;
	}
	public void setChargeMoney(float chargeMoney) {
		this.chargeMoney = chargeMoney;
	}
	public Date getSellDate() {
		return sellDate;
	}
	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}
	public int getSellStatus() {
		return sellStatus;
	}
	public void setSellStatus(int sellStatus) {
		this.sellStatus = sellStatus;
	}
	public Date getDealDate() {
		return dealDate;
	}
	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}
	public int getChargeType() {
		return chargeType;
	}
	public void setChargeType(int chargeType) {
		this.chargeType = chargeType;
	}
	
}
