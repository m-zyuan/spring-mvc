package com.webservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.xfire.transport.http.XFireServletController;
import org.springframework.stereotype.Service;
import org.w3c.dom.ls.LSException;

import com.common.Criteria;
import com.google.gson.Gson;
import com.model.XiaocaiArticle;
import com.model.XiaocaiCharge;
import com.model.XiaocaiInput;
import com.model.XiaocaiIpTimesInfo;
import com.model.XiaocaiResultMsg;
import com.model.XiaocaiUrls;
import com.model.XiaocaiUser;
import com.model.XiaocaiUserLog;
import com.model.XiaocaiWebConfig;
import com.model.XiaocaiWorkInfo;
import com.service.WebConfigInfoService;
import com.service.XiaocaiArticleService;
import com.service.XiaocaiChargeService;
import com.service.XiaocaiInputService;
import com.service.XiaocaiUrlsService;
import com.service.XiaocaiUserLogService;
import com.service.XiaocaiUserService;
import com.service.impl.RedisBase;
import com.util.SendMail;
import com.util.WebConstants;
import com.webservice.model.XiaocaiChargeApi;
import com.webservice.model.XiaocaiInputApi;
import com.webservice.model.XiaocaiNoticeApi;
import com.webservice.model.XiaocaiSoftUpdate;
import com.webservice.model.XiaocaiUrlsApi;
import com.webservice.model.XiaocaiUserApi;
import com.webservice.model.XiaocaiUserLogApi;

@Service
public class ClientApiBean implements ClientApi {
	// http://127.0.0.1/services/ClientApi?wsdl
	@Resource
	private XiaocaiUserService xiaocaiUserService;
	@Resource
	private XiaocaiUrlsService xiaocaiUrlsService;
	@Resource
	private WebConfigInfoService webConfigInfoService;
	@Resource
	private XiaocaiArticleService xiaocaiArticleService;
	@Resource
	private XiaocaiChargeService xiaocaiChargeService;
	@Resource
	private XiaocaiUserLogService xiaocaiUserLogService;
	@Resource
	private XiaocaiInputService xiaocaiInputService;
	@Resource
	private RedisBase redisBase;
	XiaocaiResultMsg xiaocaiResultMsg;

	@Override
	public XiaocaiResultMsg login(String userName, String passWord) {
		xiaocaiResultMsg = new XiaocaiResultMsg();
		XiaocaiUserApi xiaocaiUserApi = getUserInfo(userName, passWord);
		if (null == xiaocaiUserApi) {
			xiaocaiResultMsg.setMsg("用户名或密码错误！");
			xiaocaiResultMsg.setResult(false);
		} else {
			xiaocaiResultMsg.setMsg("登录成功！");
			xiaocaiResultMsg.setResult(true);
		}
		return xiaocaiResultMsg;
	}

	@Override
	public XiaocaiUserApi getUserInfo(String userName, String passWord) {
		XiaocaiUser xiaocaiUser = new XiaocaiUser();
		xiaocaiUser.setUserEmail(userName);
		xiaocaiUser.setUserPassword(passWord);
		xiaocaiUser = xiaocaiUserService.login(xiaocaiUser);
		XiaocaiUserApi xiaocaiUserApi = new XiaocaiUserApi();
		xiaocaiUserApi = getXiaocaiUserApi(xiaocaiUser);
		return xiaocaiUserApi;
	}

	private XiaocaiUserApi getXiaocaiUserApi(XiaocaiUser xiaocaiUser) {
		if (null != xiaocaiUser) {
			XiaocaiUserApi xiaocaiUserApi = new XiaocaiUserApi();
			xiaocaiUserApi.setBanedIs(xiaocaiUser.getBanedIs());
			xiaocaiUserApi.setUserMoney(xiaocaiUser.getUserMoney());
			xiaocaiUserApi.setFrozenMoney(xiaocaiUser.getFrozenMoney());
			xiaocaiUserApi.setFrozenPoint(xiaocaiUser.getFrozenPoint());
			xiaocaiUserApi.setId(xiaocaiUser.getId());
			xiaocaiUserApi.setReferUserId(xiaocaiUser.getReferUserId());
			xiaocaiUserApi.setUserEmail(xiaocaiUser.getUserEmail());
			xiaocaiUserApi.setUserLevel(xiaocaiUser.getUserLevel());
			xiaocaiUserApi.setUserLevelName(xiaocaiUser.getUserLevelName());
			xiaocaiUserApi.setUserPassword(xiaocaiUser.getUserPassword());
			xiaocaiUserApi.setUserPoint(xiaocaiUser.getUserPoint());
			xiaocaiUserApi.setUserQQ(xiaocaiUser.getUserQQ());
			xiaocaiUserApi.setUserRegDate(xiaocaiUser.getUserRegDate());
			xiaocaiUserApi.setWarnedTimes(xiaocaiUser.getWarnedTimes());
			return xiaocaiUserApi;
		}
		return null;
	}

	@Override
	public List<XiaocaiNoticeApi> getWebNotice(int page) {
		List<XiaocaiNoticeApi> listXiaocaiNoticeApi = new ArrayList();
		String domainString = webConfigInfoService.getXiaocaiWebConfig()
				.getWeb_domain();
		Criteria criteria = new Criteria();
		criteria.setPage(1);
		criteria.setRowCount(10);
		Map< String, Object> condition=new HashMap<String, Object>();
		condition.put("articleTypeIdP", 1);
		List<XiaocaiArticle> listXiaocaiArticle = xiaocaiArticleService
				.selectByCriteriaPagination(criteria);
		for (XiaocaiArticle xiaocaiArticle : listXiaocaiArticle) {
			XiaocaiNoticeApi xiaocaiNoticeApi = new XiaocaiNoticeApi();
			xiaocaiNoticeApi.setId(xiaocaiArticle.getId());
			xiaocaiNoticeApi.setTitle(xiaocaiArticle.getArticleTitle());
			xiaocaiNoticeApi.setUrl("http://" + domainString + "/article/"
					+ xiaocaiArticle.getId());
			listXiaocaiNoticeApi.add(xiaocaiNoticeApi);
		}
		return listXiaocaiNoticeApi;
	}

	/**
	 * 通过用户获取随机任务网址
	 */
	@Override
	public List<XiaocaiUrlsApi> getXiaocaiUrlsByUserId(long userId) {
		List<XiaocaiUrls> listXiaocaiUrls = xiaocaiUrlsService
				.getXiaocaiUrlsListFromRedis();
		if (null != listXiaocaiUrls) {
			List<XiaocaiUrlsApi> list = new ArrayList<XiaocaiUrlsApi>();
			for (XiaocaiUrls xiaocaiUrls : listXiaocaiUrls) {
				XiaocaiUrlsApi e = getXiaocaiUrlsApi(xiaocaiUrls);
				list.add(e);
			}
			return list;
		}
		return null;
	}

	private XiaocaiUrlsApi getXiaocaiUrlsApi(XiaocaiUrls xiaocaiUrls) {
		XiaocaiUrlsApi xiaocaiUrlsApi = new XiaocaiUrlsApi();
		xiaocaiUrlsApi.setCurrentPoint(xiaocaiUrls.getCurrentPoint());
		xiaocaiUrlsApi.setSourceIsJump(xiaocaiUrls.getSourceIsJump());
		xiaocaiUrlsApi.setSourceUrls(xiaocaiUrls.getSourceUrls());
		xiaocaiUrlsApi.setTargetUrl(xiaocaiUrls.getTargetUrl());
		xiaocaiUrlsApi.setUrlId(xiaocaiUrls.getUrlId());
		xiaocaiUrlsApi.setUserIdP(xiaocaiUrls.getUserIdP());
		xiaocaiUrlsApi.setPassIs(xiaocaiUrls.getPassIs());
		xiaocaiUrlsApi.setInnerIsJump(xiaocaiUrls.getInnerIsJump());
		xiaocaiUrlsApi.setInnerIsJump(xiaocaiUrls.getInnerIsJump());
		xiaocaiUrlsApi.setTime10IpCount(xiaocaiUrls.getTime10IpCount());
		xiaocaiUrlsApi.setTime1IpCount(xiaocaiUrls.getTime1IpCount());
		xiaocaiUrlsApi.setTime2IpCount(xiaocaiUrls.getTime2IpCount());
		xiaocaiUrlsApi.setTime3IpCount(xiaocaiUrls.getTime3IpCount());
		xiaocaiUrlsApi.setTime4IpCount(xiaocaiUrls.getTime4IpCount());
		xiaocaiUrlsApi.setTime5IpCount(xiaocaiUrls.getTime5IpCount());
		xiaocaiUrlsApi.setTime6IpCount(xiaocaiUrls.getTime6IpCount());
		xiaocaiUrlsApi.setTime7IpCount(xiaocaiUrls.getTime7IpCount());
		xiaocaiUrlsApi.setTime8IpCount(xiaocaiUrls.getTime8IpCount());
		xiaocaiUrlsApi.setTime9IpCount(xiaocaiUrls.getTime9IpCount());
		xiaocaiUrlsApi.setTime11IpCount(xiaocaiUrls.getTime11IpCount());
		xiaocaiUrlsApi.setTime12IpCount(xiaocaiUrls.getTime12IpCount());
		xiaocaiUrlsApi.setTime13IpCount(xiaocaiUrls.getTime13IpCount());
		xiaocaiUrlsApi.setTime14IpCount(xiaocaiUrls.getTime14IpCount());
		xiaocaiUrlsApi.setTime15IpCount(xiaocaiUrls.getTime15IpCount());
		xiaocaiUrlsApi.setTime16IpCount(xiaocaiUrls.getTime16IpCount());
		xiaocaiUrlsApi.setTime17IpCount(xiaocaiUrls.getTime17IpCount());
		xiaocaiUrlsApi.setTime18IpCount(xiaocaiUrls.getTime18IpCount());
		xiaocaiUrlsApi.setTime19IpCount(xiaocaiUrls.getTime19IpCount());
		xiaocaiUrlsApi.setTime20IpCount(xiaocaiUrls.getTime20IpCount());
		xiaocaiUrlsApi.setTime21IpCount(xiaocaiUrls.getTime21IpCount());
		xiaocaiUrlsApi.setTime22IpCount(xiaocaiUrls.getTime22IpCount());
		xiaocaiUrlsApi.setTime23IpCount(xiaocaiUrls.getTime23IpCount());
		xiaocaiUrlsApi.setTime24IpCount(xiaocaiUrls.getTime24IpCount());
		return xiaocaiUrlsApi;
	}

	@Override
	public boolean addPoint(String userName, String passWord, List<Long> urlId,
			String extCode) {
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null == xiaocaiUser) {
			return false;
		}
		// 找不到数据直接返回删除（作弊数据）
		String key = "user_" + xiaocaiUser.getId() + "_rand_" + extCode;
		if (redisBase.STRINGS.get(key) == null) {
			return true;
		}
		redisBase.KEYS.del(key);
		// 未过期数据给用户加分
		if (null != urlId && urlId.size() > 0)
			for (int i = 0; i < urlId.size(); i++) {
				long id = urlId.get(i);
				long userId = xiaocaiUser.getId();
				XiaocaiUrls xiaocaiUrls = xiaocaiUrlsService
						.getXiaocaiUrlsById(id);
				// 获取当前小时
				Calendar c = Calendar.getInstance();// 可以对每个时间域单独修改
				int hour = c.get(Calendar.HOUR_OF_DAY);
				XiaocaiWorkInfo xiaocaiWorkInfo = new XiaocaiWorkInfo();
				xiaocaiWorkInfo.setUrlId(id);
				xiaocaiWorkInfo.setPoint(1);
				xiaocaiWorkInfo.setUserId(userId);
				xiaocaiWorkInfo.setUrlsBelongToUserId(xiaocaiUrls.getUserIdP());
				xiaocaiWorkInfo.setFinishedHour(hour);
				Gson gson = new Gson();
				// 对此链接进行减分操作
				String keyString = "urlId_"
						+ String.valueOf(xiaocaiUrls.getUrlId()) + "_userId_"
						+ String.valueOf(userId);
//				redisBase.HASH.hset("urls_sql", keyString,
//						gson.toJson(xiaocaiWorkInfo));
				redisBase.LISTS.lpush("urls_sql",gson.toJson(xiaocaiWorkInfo));
				
				// 记录当前访问人刷的次数
				keyString = "userId_" + String.valueOf(userId);
				XiaocaiIpTimesInfo xiaocaiIpTimesInfoOld = (XiaocaiIpTimesInfo) gson
						.fromJson(
								redisBase.HASH.hget("urls_minute", keyString),
								XiaocaiIpTimesInfo.class);
				XiaocaiIpTimesInfo xiaocaiIpTimesInfo = new XiaocaiIpTimesInfo();
				xiaocaiIpTimesInfo.setUserId(userId);
				if (null == xiaocaiIpTimesInfoOld) {
					xiaocaiIpTimesInfo.setClickCount(1);
				} else {
					xiaocaiIpTimesInfo.setClickCount(xiaocaiIpTimesInfoOld
							.getClickCount() + 1);
				}
				redisBase.HASH.hset("urls_minute", keyString,
						gson.toJson(xiaocaiIpTimesInfo));
				// 记录当前访问的IP地址（设置1分钟过期？）
				HttpServletRequest request = XFireServletController
						.getRequest();
				keyString = "customer_" + "userId_" + String.valueOf(userId)
						+ "_ip_" + this.getIpAddr(request).replace(".", "_");
				redisBase.STRINGS.setEx(keyString, 61, String.valueOf(1));
			}
		// System.out.println(urlId.size());
		return true;
	}

	protected String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 留言
	 */
	@Override
	public XiaocaiResultMsg leaveMessage(String userName, String passWord,
			String message) {
		XiaocaiResultMsg xiaocaiResultMsg = new XiaocaiResultMsg();
		StringBuffer sendContentBuffer = new StringBuffer();
		XiaocaiUserApi xiaocaiUser = getUserInfo(userName, passWord);
		sendContentBuffer.append("用户ID为" + xiaocaiUser.getId() + ",帐户名为："
				+ xiaocaiUser.getUserEmail() + "的用户留言：");
		sendContentBuffer.append(message);
		SendMail sendMail = new SendMail("lipeifengde1986@126.com",
				"383063671@qq.com", "lipeifengde1986", "19860706", "用户留言",
				message);
		if (sendMail.send()) {
			xiaocaiResultMsg.setResult(true);
			xiaocaiResultMsg.setMsg("留言成功");
		} else {
			xiaocaiResultMsg.setMsg("留言失败");
			xiaocaiResultMsg.setResult(false);
		}
		return xiaocaiResultMsg;
	}

	@Override
	public String getExtCodeByUserId(String userName, String passWord) {
		String randString = "";
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null != xiaocaiUser) {
			randString = getRandStr(8);
			String keyString = "user_" + xiaocaiUser.getId() + "_rand_"
					+ randString;
			redisBase.STRINGS.setEx(keyString, 61, String.valueOf(1));
		}
		return randString;
	}

	/**
	 * 获取随机字符串
	 * 
	 * @return
	 */
	private String getRandStr(int length) {
		Random randGen = new Random();
		char[] numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz"
				+ "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
		char[] randBuffer = new char[length];
		for (int i = 0; i < randBuffer.length; i++) {
			randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
		}
		return new String(randBuffer);
	}

	/**
	 * 注册
	 */
	@Override
	public XiaocaiResultMsg reg(XiaocaiUserApi xiaocaiUserApi) {
		XiaocaiUser xiaocaiUser = getXiaocaiUser(xiaocaiUserApi);
		xiaocaiResultMsg = new XiaocaiResultMsg();
		if (xiaocaiUser.getUserEmail() == null
				|| (xiaocaiUser.getUserEmail().equals(""))
				|| (xiaocaiUser.getUserPassword() == null)
				|| (xiaocaiUser.getUserPassword().equals(""))) {
			xiaocaiResultMsg.setResult(false);
			xiaocaiResultMsg.setMsg("帐户名密码必须填写！");
			return xiaocaiResultMsg;
		}
		boolean isExist = xiaocaiUserService.isExistByXiaocaiUser(xiaocaiUser);
		if (isExist) {
			xiaocaiResultMsg.setResult(false);
			xiaocaiResultMsg.setMsg("帐户名已存在");
			return xiaocaiResultMsg;
		}
		xiaocaiUser.setUserLevel(0);
		xiaocaiUser.setUserLevelName("普通会员");
		xiaocaiUser.setUserRegDate(new Date());
		xiaocaiUser = xiaocaiUserService.add(xiaocaiUser);
		if (xiaocaiUser == null) {
			xiaocaiResultMsg.setResult(false);
			xiaocaiResultMsg.setMsg("注册失败！");
			return xiaocaiResultMsg;
		}
		xiaocaiResultMsg.setResult(true);
		xiaocaiResultMsg.setMsg("注册成功！");
		return xiaocaiResultMsg;
	}

	private XiaocaiUser getXiaocaiUser(String userName, String passWord) {
		if (null == userName || null == passWord) {
			return null;
		}
		XiaocaiUser xiaocaiUser = new XiaocaiUser();
		xiaocaiUser.setUserEmail(userName);
		xiaocaiUser.setUserPassword(passWord);
		xiaocaiUser = xiaocaiUserService.login(xiaocaiUser);
		return xiaocaiUser;
	}

	private XiaocaiUser getXiaocaiUser(XiaocaiUserApi xiaocaiUserApi) {
		if (null != xiaocaiUserApi) {
			XiaocaiUser xiaocaiUser = new XiaocaiUser();
			xiaocaiUser.setBanedIs(xiaocaiUserApi.getBanedIs());
			xiaocaiUser.setFrozenMoney(xiaocaiUserApi.getFrozenMoney());
			xiaocaiUser.setFrozenPoint(xiaocaiUserApi.getFrozenPoint());
			xiaocaiUser.setId(xiaocaiUserApi.getId());
			xiaocaiUser.setReferUserId(xiaocaiUserApi.getReferUserId());
			xiaocaiUser.setUserEmail(xiaocaiUserApi.getUserEmail());
			xiaocaiUser.setUserLevel(xiaocaiUserApi.getUserLevel());
			xiaocaiUser.setUserLevelName(xiaocaiUserApi.getUserLevelName());
			xiaocaiUser.setUserPassword(xiaocaiUserApi.getUserPassword());
			xiaocaiUser.setUserPoint(xiaocaiUserApi.getUserPoint());
			xiaocaiUser.setUserQQ(xiaocaiUserApi.getUserQQ());
			xiaocaiUser.setUserRegDate(xiaocaiUserApi.getUserRegDate());
			xiaocaiUser.setWarnedTimes(xiaocaiUserApi.getWarnedTimes());
			return xiaocaiUser;
		}
		return null;
	}

	@Override
	public List<XiaocaiUserApi> getMyRefer(String userName, String passWord,
			int page) {
		// 获取下线列表
		XiaocaiUser XiaocaiUser = getXiaocaiUser(userName, passWord);
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("referUserId", XiaocaiUser.getId());
		Criteria criteria = new Criteria();
		int pageSize = 10;
		criteria.setPageSize(pageSize);
		criteria.setPage(page);
		criteria.setPageStart((page - 1) * pageSize);
		criteria.setCondition(condition);
		List<XiaocaiUser> list = xiaocaiUserService
				.selectByCriteriaPagination(criteria);
		List<XiaocaiUserApi> listXiaocaiUserApi = new ArrayList<XiaocaiUserApi>();
		for (XiaocaiUser xiaocaiUser2 : list) {
			xiaocaiUser2.setUserPassword("");
			listXiaocaiUserApi.add(getXiaocaiUserApi(xiaocaiUser2));
		}
		if (listXiaocaiUserApi.size() < 0) {
			return null;
		}
		return listXiaocaiUserApi;
	}

	@Override
	public XiaocaiResultMsg changePwd(String userName, String passWord,
			String newPassword) {
		XiaocaiResultMsg xiaocaiResultMsg = new XiaocaiResultMsg();
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null == xiaocaiUser) {
			xiaocaiResultMsg.setMsg("旧密码错误");
			xiaocaiResultMsg.setResult(false);
		} else {
			xiaocaiUser = xiaocaiUserService.selectByPrimaryKey(xiaocaiUser
					.getId());
			xiaocaiUser.setUserPassword(newPassword);
			xiaocaiUserService.updateByPrimaryKey(xiaocaiUser);
			xiaocaiResultMsg.setMsg("修改成功");
			xiaocaiResultMsg.setResult(true);
		}
		return xiaocaiResultMsg;
	}

	@Override
	public List<XiaocaiChargeApi> getMyChargeList(String userName,
			String passWord, int page) {
		// 获取交易记录
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null == xiaocaiUser) {
			return null;
		}
		Criteria criteria = new Criteria();
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("sellUserId", xiaocaiUser.getId());
		criteria.setCondition(condition);
		int pageSize = 20;
		criteria.setPageSize(pageSize);
		criteria.setPage(page);
		criteria.setPageStart((page - 1) * pageSize);
		List<XiaocaiCharge> list = xiaocaiChargeService
				.selectByCriteriaPagination(criteria);
		if (null == list || list.size() == 0) {
			return null;
		}
		List<XiaocaiChargeApi> listApis = new ArrayList<XiaocaiChargeApi>();
		for (XiaocaiCharge xiaocaiCharge : list) {
			XiaocaiChargeApi e = new XiaocaiChargeApi();
			e = getXiaocaiChargeApi(xiaocaiCharge);
			listApis.add(e);
		}
		return listApis;
	}

	// 对象复制
	private XiaocaiChargeApi getXiaocaiChargeApi(XiaocaiCharge xiaocaiCharge) {
		XiaocaiChargeApi xiaocaiChargeApi = new XiaocaiChargeApi();
		xiaocaiChargeApi.setBuyUserId(xiaocaiCharge.getBuyUserId());
		xiaocaiChargeApi.setChargeMoney(xiaocaiCharge.getChargeMoney());
		xiaocaiChargeApi.setChargePoint(xiaocaiCharge.getChargePoint());
		xiaocaiChargeApi.setChargeType(xiaocaiCharge.getChargeType());
		xiaocaiChargeApi.setId(xiaocaiCharge.getId());
		xiaocaiChargeApi.setSellDate(xiaocaiCharge.getSellDate());
		xiaocaiChargeApi.setSellStatus(xiaocaiCharge.getSellStatus());
		xiaocaiChargeApi.setSellUserId(xiaocaiCharge.getSellUserId());
		return xiaocaiChargeApi;
	}

	@Override
	public XiaocaiResultMsg payOut(String userName, String passWord,
			float payOutMoney) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XiaocaiResultMsg pay(String userName, String passWord, float payMoney) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<XiaocaiInputApi> money(String userName, String passWord,
			int page) {
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null == xiaocaiUser) {
			return null;
		}
		Criteria criteria = new Criteria();
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("userIdP", xiaocaiUser.getId());
		criteria.setCondition(condition);
		int pageSize = 20;
		criteria.setPageSize(pageSize);
		criteria.setPage(page);
		criteria.setPageStart((page - 1) * pageSize);
		List<XiaocaiInput> list = xiaocaiInputService
				.selectByCriteriaPagination(criteria);
		if (null == list || list.size() == 0) {
			return null;
		}
		List<XiaocaiInputApi> listXiaocaiInputApi = new ArrayList<XiaocaiInputApi>();
		for (XiaocaiInput xiaocaiInput : list) {
			XiaocaiInputApi e = getXiaocaiInputApi(xiaocaiInput);
			listXiaocaiInputApi.add(e);
		}
		return listXiaocaiInputApi;
	}

	private XiaocaiInputApi getXiaocaiInputApi(XiaocaiInput xiaocaiInput) {
		XiaocaiInputApi xiaocaiInputApi = new XiaocaiInputApi();
		xiaocaiInputApi.setAdminIdP(xiaocaiInput.getAdminIdP());
		xiaocaiInputApi.setId(xiaocaiInput.getId());
		xiaocaiInputApi.setInputBak(xiaocaiInput.getInputBak());
		xiaocaiInputApi.setInputBank(xiaocaiInput.getInputBank());
		xiaocaiInputApi.setInputDate(xiaocaiInput.getInputDate());
		xiaocaiInputApi.setInputMoney(xiaocaiInput.getInputMoney());
		xiaocaiInputApi.setInputPassDate(xiaocaiInput.getInputPassDate());
		xiaocaiInputApi.setInputStatus(xiaocaiInput.getInputStatus());
		xiaocaiInputApi.setInputType(xiaocaiInput.getInputType());
		xiaocaiInputApi.setInputUserName(xiaocaiInput.getInputUserName());
		xiaocaiInputApi.setUserIdP(xiaocaiInput.getUserIdP());
		return xiaocaiInputApi;
	}
	private int getCanBuyPointByUserLevel(int userLevel) {
		XiaocaiWebConfig xiaocaiWebConfig=webConfigInfoService.getXiaocaiWebConfig();
		int pricePoint=10000;
		switch (userLevel) {
		case 0:
			pricePoint=xiaocaiWebConfig.getPoint_price_1();
			break;
		case 1:
			pricePoint=xiaocaiWebConfig.getPoint_price_2();
			break;	
		case 2:
			pricePoint=xiaocaiWebConfig.getPoint_price_3();
			break;	
		case 3:
			pricePoint=xiaocaiWebConfig.getPoint_price_4();
			break;	
		default:
			break;
		}
		return pricePoint;
	}

	/**
	 * 购买积分
	 */
	@Override
	public XiaocaiResultMsg pointBuy(String userName, String passWord,
			float point) {
		XiaocaiResultMsg xiaocaiResultMsg=new XiaocaiResultMsg();
		xiaocaiResultMsg.setResult(false);
		int minBuyPoint=webConfigInfoService.getXiaocaiWebConfig().getMin_buy_point();
		XiaocaiUser userObject =getXiaocaiUser(userName, passWord);
		if (null == userObject || point <= minBuyPoint) {
			xiaocaiResultMsg.setMsg("积分购买失败，积分最少"+minBuyPoint);
			return xiaocaiResultMsg;
		}
		XiaocaiUser xiaocaiUser = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		int canBuyPoint=getCanBuyPointByUserLevel(xiaocaiUser.getUserLevel());
		if (xiaocaiUser.getUserMoney() < point / canBuyPoint) {
			xiaocaiResultMsg.setMsg("用户金额不足");
			return xiaocaiResultMsg;
		}
		// 购买成功，积分增加，现金减少
		xiaocaiUser.setUserPoint(xiaocaiUser.getUserPoint() + point);
		xiaocaiUser.setUserMoney(xiaocaiUser.getUserMoney()
				- (point / canBuyPoint));
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUser);
		// 添加交易记录
		XiaocaiCharge xiaocaiCharge = new XiaocaiCharge();
		xiaocaiCharge.setSellUserId(xiaocaiUser.getId());
		xiaocaiCharge.setChargePoint(point);
		xiaocaiCharge.setChargeMoney(((float) point / canBuyPoint));
		xiaocaiCharge.setDealDate(new Date());
		xiaocaiCharge.setSellDate(new Date());
		xiaocaiCharge.setChargeType(1);// 设置交易为购买
		xiaocaiChargeService.addXiaocaiChargeService(xiaocaiCharge);
		// 积分购买日志
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_BUY_POINT, "购买积分" + point + "个");
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_BUY_POINT_USE_MONEY, "购买积分花费" + point
						/ canBuyPoint + "元");
		xiaocaiResultMsg.setResult(true);
		xiaocaiResultMsg.setMsg("兑换成功");
		return xiaocaiResultMsg;
	}

	@Override
	public XiaocaiResultMsg exchange(String userName, String passWord,
			float point) {
		// 积分兑换
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null == xiaocaiUser) {
			xiaocaiResultMsg.setResult(false);
			xiaocaiResultMsg.setMsg("帐户名或密码错误");
			return xiaocaiResultMsg;
		}
		if (xiaocaiUser.getUserPoint() < point) {
			xiaocaiResultMsg.setResult(false);
			xiaocaiResultMsg.setMsg("积分不足");
			return xiaocaiResultMsg;
		}
		int minExchangePoint =webConfigInfoService.getXiaocaiWebConfig().getMin_exchange_point();
		if (point <= minExchangePoint) {
			xiaocaiResultMsg.setResult(false);
			xiaocaiResultMsg.setMsg("兑换失败，兑换积分最少"+minExchangePoint);
			return xiaocaiResultMsg;
		}
		int oneRmbExchangePoint = webConfigInfoService.getXiaocaiWebConfig()
				.getOne_rmb_exchange_point();
		float money = point / oneRmbExchangePoint;
		xiaocaiUserService.addMoney(xiaocaiUser.getId(), money);
		xiaocaiUserService.addPoint(xiaocaiUser.getId(), 0 - point);
		// 添加log
		// 添加交易记录
		XiaocaiCharge xiaocaiCharge = new XiaocaiCharge();
		xiaocaiCharge.setSellUserId(xiaocaiUser.getId());
		xiaocaiCharge.setChargePoint((float) point);
		xiaocaiCharge.setChargeMoney(((float) point / oneRmbExchangePoint));
		xiaocaiCharge.setDealDate(new Date());
		xiaocaiCharge.setSellDate(new Date());
		xiaocaiChargeService.addXiaocaiChargeService(xiaocaiCharge);
		xiaocaiUserLogService.addXiaocaiUserLogService(xiaocaiUser.getId(),
				XiaocaiUserLog.USER_SELL_POINT, "出售积分" + point + "个");
		xiaocaiUserLogService.addXiaocaiUserLogService(xiaocaiUser.getId(),
				XiaocaiUserLog.USER_SELL_POINT_GET_MONEY, "出售积分得到" + point
						/ oneRmbExchangePoint + "元");
		xiaocaiResultMsg.setResult(true);
		xiaocaiResultMsg.setMsg("交易成功！");
		return xiaocaiResultMsg;
	}

	@Override
	public XiaocaiResultMsg movePoint(String userName, String passWord,
			String acceptUserName, float point) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XiaocaiResultMsg acceptMovePoint(String userName, String passWord,
			long chargeId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XiaocaiResultMsg addUrl(String userName, String passWord,
			XiaocaiUrlsApi xiaocaiUrlsApi) {
		XiaocaiResultMsg xiaocaiResultMsg=new XiaocaiResultMsg();
		try {
			xiaocaiResultMsg.setResult(false);
			XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
			if (null == xiaocaiUser) {
				xiaocaiResultMsg.setMsg("用户名或密码不正确");
				return xiaocaiResultMsg;
			}
			xiaocaiUrlsApi.setUserIdP(xiaocaiUser.getId());
			XiaocaiUrls xiaocaiUrls=getXiaocaiUrls(xiaocaiUrlsApi);
			xiaocaiUrlsService.add(xiaocaiUrls);
			xiaocaiResultMsg.setMsg("新增成功！");
			xiaocaiResultMsg.setResult(true);
		} catch (Exception e) {
			xiaocaiResultMsg.setMsg("新增失败");
		}
		return xiaocaiResultMsg;
	}
	/**
	 * 获取网址ID对应的网址所有信息，用于编辑
	 * @param userName
	 * @param passWord
	 * @param urlId
	 * @return
	 */
	public XiaocaiUrlsApi getXiaocaiUrlsApiInfo(String userName, String passWord,long urlId) {
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null != xiaocaiUser) {
			XiaocaiUrls xiaocaiUrls=xiaocaiUrlsService.getXiaocaiUrlsById(urlId);
			if (xiaocaiUrls.getUserIdP()==xiaocaiUser.getId()) {
				XiaocaiUrlsApi xiaocaiUrlsApi=getXiaocaiUrlsApi(xiaocaiUrls);
				return xiaocaiUrlsApi;
			}
		}
		return null;
	}
	/**
	 * 编辑网址信息
	 * @param userName
	 * @param passWord
	 * @param xiaocaiUrlsApi
	 * @return
	 */
	public XiaocaiResultMsg editXiaoCaiUrls(String userName, String passWord,XiaocaiUrlsApi xiaocaiUrlsApi) {
		XiaocaiResultMsg xiaocaiResultMsg=new XiaocaiResultMsg();
		xiaocaiResultMsg.setResult(false);
		xiaocaiResultMsg.setMsg("修改失败");
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null != xiaocaiUser) {
			XiaocaiUrls xiaocaiUrlsOld=xiaocaiUrlsService.getXiaocaiUrlsById(xiaocaiUrlsApi.getUrlId());
			XiaocaiUrls xiaocaiUrlsNew=getXiaocaiUrls(xiaocaiUrlsApi);
			xiaocaiUrlsNew.setCurrentPoint(xiaocaiUrlsOld.getCurrentPoint());
			xiaocaiUrlsNew.setUserIdP(xiaocaiUrlsOld.getUserIdP());
			if (xiaocaiUser.getId()==xiaocaiUrlsNew.getUserIdP()) {
				xiaocaiUrlsService.updateByPrimaryKey(xiaocaiUrlsNew);
				xiaocaiResultMsg.setResult(true);
				xiaocaiResultMsg.setMsg("修改成功");
			}
		}
		return xiaocaiResultMsg;
	}
	public XiaocaiResultMsg delXiaoCaiUrls(String userName, String passWord,int urlId) {
		XiaocaiResultMsg xiaocaiResultMsg=new XiaocaiResultMsg();
		xiaocaiResultMsg.setResult(false);
		xiaocaiResultMsg.setMsg("删除失败");
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null != xiaocaiUser) {
			XiaocaiUrls xiaocaiUrls=xiaocaiUrlsService.getXiaocaiUrlsById(urlId);
			if (xiaocaiUser.getId()==xiaocaiUrls.getUserIdP()) {
				int[] ids=new int[1];
				ids[0]=urlId;
				xiaocaiUrlsService.deleteBatch(ids);
				long urlsCurrentPoint=xiaocaiUrls.getCurrentPoint();
				// 用户积分增加
				xiaocaiUserService.addPoint(xiaocaiUser.getId(), urlsCurrentPoint);
				xiaocaiUserLogService.addXiaocaiUserLogService(xiaocaiUser.getId(),
						XiaocaiUserLog.DEL_TASK_BACK_POINT, "删除任务积分退回"
								+ urlsCurrentPoint + "个");
				xiaocaiResultMsg.setResult(true);
				xiaocaiResultMsg.setMsg("修改成功");
			}
		}
		return xiaocaiResultMsg;
	}
	
	private XiaocaiUrls getXiaocaiUrls(XiaocaiUrlsApi xiaocaiUrlsApi) {
		XiaocaiUrls xiaocaiUrls=new XiaocaiUrls();
		xiaocaiUrls.setCurrentPoint(xiaocaiUrlsApi.getCurrentPoint());
		xiaocaiUrls.setSourceIsJump(xiaocaiUrlsApi.getSourceIsJump());
		xiaocaiUrls.setSourceUrls(xiaocaiUrlsApi.getSourceUrls());
		xiaocaiUrls.setTargetUrl(xiaocaiUrlsApi.getTargetUrl());
		xiaocaiUrls.setUrlId(xiaocaiUrlsApi.getUrlId());
		xiaocaiUrls.setUserIdP(xiaocaiUrlsApi.getUserIdP());
		xiaocaiUrls.setPassIs(0);
		xiaocaiUrls.setInnerIsJump(xiaocaiUrlsApi.getInnerIsJump());
		xiaocaiUrls.setTime10IpCount(xiaocaiUrlsApi.getTime10IpCount());
		xiaocaiUrls.setTime1IpCount(xiaocaiUrlsApi.getTime1IpCount());
		xiaocaiUrls.setTime2IpCount(xiaocaiUrlsApi.getTime2IpCount());
		xiaocaiUrls.setTime3IpCount(xiaocaiUrlsApi.getTime3IpCount());
		xiaocaiUrls.setTime4IpCount(xiaocaiUrlsApi.getTime4IpCount());
		xiaocaiUrls.setTime5IpCount(xiaocaiUrlsApi.getTime5IpCount());
		xiaocaiUrls.setTime6IpCount(xiaocaiUrlsApi.getTime6IpCount());
		xiaocaiUrls.setTime7IpCount(xiaocaiUrlsApi.getTime7IpCount());
		xiaocaiUrls.setTime8IpCount(xiaocaiUrlsApi.getTime8IpCount());
		xiaocaiUrls.setTime9IpCount(xiaocaiUrlsApi.getTime9IpCount());
		xiaocaiUrls.setTime11IpCount(xiaocaiUrlsApi.getTime11IpCount());
		xiaocaiUrls.setTime12IpCount(xiaocaiUrlsApi.getTime12IpCount());
		xiaocaiUrls.setTime13IpCount(xiaocaiUrlsApi.getTime13IpCount());
		xiaocaiUrls.setTime14IpCount(xiaocaiUrlsApi.getTime14IpCount());
		xiaocaiUrls.setTime15IpCount(xiaocaiUrlsApi.getTime15IpCount());
		xiaocaiUrls.setTime16IpCount(xiaocaiUrlsApi.getTime16IpCount());
		xiaocaiUrls.setTime17IpCount(xiaocaiUrlsApi.getTime17IpCount());
		xiaocaiUrls.setTime18IpCount(xiaocaiUrlsApi.getTime18IpCount());
		xiaocaiUrls.setTime19IpCount(xiaocaiUrlsApi.getTime19IpCount());
		xiaocaiUrls.setTime20IpCount(xiaocaiUrlsApi.getTime20IpCount());
		xiaocaiUrls.setTime21IpCount(xiaocaiUrlsApi.getTime21IpCount());
		xiaocaiUrls.setTime22IpCount(xiaocaiUrlsApi.getTime22IpCount());
		xiaocaiUrls.setTime23IpCount(xiaocaiUrlsApi.getTime23IpCount());
		xiaocaiUrls.setTime24IpCount(xiaocaiUrlsApi.getTime24IpCount());
		return xiaocaiUrls;
	}

	/**
	 * 查询我的网址
	 */
	@Override
	public List<XiaocaiUrlsApi> myXiaocaiUrls(String userName, String passWord,
			int page) {
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null == xiaocaiUser) {
			return null;
		}
		Criteria criteria = new Criteria();
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("userIdP", xiaocaiUser.getId());
		criteria.setCondition(condition);
		int pageSize = 20;
		criteria.setPageSize(pageSize);
		criteria.setPage(page);
		criteria.setPageStart((page - 1) * pageSize);
		criteria.setOrder("url_id");
		List<XiaocaiUrls> listXiaocaiUrls=xiaocaiUrlsService.selectByCriteriaPagination(criteria);
		if (null!=listXiaocaiUrls&&listXiaocaiUrls.size()>0) {
			List<XiaocaiUrlsApi> listXiaocaiUrlsApi=new ArrayList<XiaocaiUrlsApi>();
			for (int i = 0; i < listXiaocaiUrls.size(); i++) {
				listXiaocaiUrlsApi.add(getXiaocaiUrlsApi(listXiaocaiUrls.get(i)));
			}
			return listXiaocaiUrlsApi;
		}
		return null;
	}

	@Override
	public List<XiaocaiUserLogApi> myXiaocaiUserLog(String userName,
			String passWord, int page) {
		// 获取用户操作日志
		XiaocaiUser xiaocaiUser = getXiaocaiUser(userName, passWord);
		if (null == xiaocaiUser) {
			return null;
		}
		Criteria criteria = new Criteria();
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("userIdP", xiaocaiUser.getId());
		criteria.setCondition(condition);
		int pageSize = 20;
		criteria.setPageSize(pageSize);
		criteria.setPage(page);
		criteria.setPageStart((page - 1) * pageSize);
		List<XiaocaiUserLog> list = xiaocaiUserLogService
				.selectByCriteriaPagination(criteria);
		if (null == list || list.size() == 0) {
			return null;
		}
		List<XiaocaiUserLogApi> listXiaocaiUserLogApi = new ArrayList<XiaocaiUserLogApi>();
		for (XiaocaiUserLog xiaocaiUserLog : list) {
			XiaocaiUserLogApi e = getXiaocaiUserLogApi(xiaocaiUserLog);
			listXiaocaiUserLogApi.add(e);
		}
		return listXiaocaiUserLogApi;
	}

	private XiaocaiUserLogApi getXiaocaiUserLogApi(XiaocaiUserLog xiaocaiUserLog) {
		XiaocaiUserLogApi xiaocaiUserLogApi = new XiaocaiUserLogApi();
		xiaocaiUserLogApi.setId(xiaocaiUserLog.getId());
		xiaocaiUserLogApi.setLogContent(xiaocaiUserLog.getLogContent());
		xiaocaiUserLogApi.setLogDate(xiaocaiUserLog.getLogDate());
		xiaocaiUserLogApi.setLogTypeId(xiaocaiUserLog.getLogTypeId());
		xiaocaiUserLogApi.setUserIdP(xiaocaiUserLog.getUserIdP());
		return xiaocaiUserLogApi;
	}

	@Override
	public List<XiaocaiSoftUpdate> getXiaocaiSoftUpdate() {
		String exeDirString=webConfigInfoService.getXiaocaiWebConfig().getWeb_domain()+"/resources/soft/";
		List<XiaocaiSoftUpdate> list=new ArrayList<XiaocaiSoftUpdate>();
		XiaocaiSoftUpdate xiaocaiSoftUpdate=new XiaocaiSoftUpdate();
		xiaocaiSoftUpdate.setId(0);
		xiaocaiSoftUpdate.setSoftName("liuliangClient.exe");
		xiaocaiSoftUpdate.setDownLoadUrl("http://"+exeDirString+"liuliangClient.exe");
		list.add(xiaocaiSoftUpdate);
		return list;
	}

	@Override
	public double getSoftVersion() {
		// TODO Auto-generated method stub
		return 1.01f;
	}

}
