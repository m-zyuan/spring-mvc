package com.model;

public class XiaocaiResultMsg {
	private String msg;//返回的消息
	private boolean result;//返回的bool值
	private int status;//返回的状态值
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
