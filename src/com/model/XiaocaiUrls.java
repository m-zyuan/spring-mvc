package com.model;

import java.io.Serializable;

import com.base.BaseModel;

public class XiaocaiUrls extends BaseModel implements Serializable
{
    private long urlId;//网址ID
    private long userIdP;//网址所属用户Id
    private String targetUrl;//要刷的网址
    private String sourceUrls;//来源网址页网址集合
    private int sourceIsJump;//是否开启来源页
    private int innerIsJump;//是否进行内转
    private int passIs;//是否通过审核
    private long currentPoint;//可用积分点
    private int time1IpCount;//某个时间段内的IP数
    private int time2IpCount;
    private int time3IpCount;
    private int time4IpCount;
    private int time5IpCount;
    private int time6IpCount;
    private int time7IpCount;
    private int time8IpCount;
    private int time9IpCount;
    private int time10IpCount;
    private int time11IpCount;
    private int time12IpCount;
    private int time13IpCount;
    private int time14IpCount;
    private int time15IpCount;
    private int time16IpCount;
    private int time17IpCount;
    private int time18IpCount;
    private int time19IpCount;
    private int time20IpCount;
    private int time21IpCount;
    private int time22IpCount;
    private int time23IpCount;
    private int time24IpCount;
    
    private int[] timeIpCountArray;
    
    public int[] getTimeIpCountArray() {
    	timeIpCountArray= new int[24];
    	timeIpCountArray[0]=time1IpCount;
    	timeIpCountArray[1]=time2IpCount;
    	timeIpCountArray[2]=time3IpCount;
    	timeIpCountArray[3]=time4IpCount;
    	timeIpCountArray[4]=time5IpCount;
    	timeIpCountArray[5]=time6IpCount;
    	timeIpCountArray[6]=time7IpCount;
    	timeIpCountArray[7]=time8IpCount;
    	timeIpCountArray[8]=time9IpCount;
    	timeIpCountArray[9]=time10IpCount;
    	timeIpCountArray[10]=time11IpCount;
    	timeIpCountArray[11]=time12IpCount;
    	timeIpCountArray[12]=time13IpCount;
    	timeIpCountArray[13]=time14IpCount;
    	timeIpCountArray[14]=time15IpCount;
    	timeIpCountArray[15]=time16IpCount;
    	timeIpCountArray[16]=time17IpCount;
    	timeIpCountArray[17]=time18IpCount;
    	timeIpCountArray[18]=time19IpCount;
    	timeIpCountArray[19]=time20IpCount;
    	timeIpCountArray[20]=time21IpCount;
    	timeIpCountArray[21]=time22IpCount;
    	timeIpCountArray[22]=time23IpCount;
    	timeIpCountArray[23]=time24IpCount;
		return timeIpCountArray;
	}
	public void setTimeIpCountArray(int[] timeIpCountArray) {
		this.timeIpCountArray = timeIpCountArray;
	}
	private int[] finishedTimeIpCountArray;
	
	public int[] getFinishedTimeIpCountArray() {
		
		finishedTimeIpCountArray= new int[24];
    	finishedTimeIpCountArray[0]=finishedTime1IpCount;
    	finishedTimeIpCountArray[1]=finishedTime2IpCount;
    	finishedTimeIpCountArray[2]=finishedTime3IpCount;
    	finishedTimeIpCountArray[3]=finishedTime4IpCount;
    	finishedTimeIpCountArray[4]=finishedTime5IpCount;
    	finishedTimeIpCountArray[5]=finishedTime6IpCount;
    	finishedTimeIpCountArray[6]=finishedTime7IpCount;
    	finishedTimeIpCountArray[7]=finishedTime8IpCount;
    	finishedTimeIpCountArray[8]=finishedTime9IpCount;
    	finishedTimeIpCountArray[9]=finishedTime10IpCount;
    	finishedTimeIpCountArray[10]=finishedTime11IpCount;
    	finishedTimeIpCountArray[11]=finishedTime12IpCount;
    	finishedTimeIpCountArray[12]=finishedTime13IpCount;
    	finishedTimeIpCountArray[13]=finishedTime14IpCount;
    	finishedTimeIpCountArray[14]=finishedTime15IpCount;
    	finishedTimeIpCountArray[15]=finishedTime16IpCount;
    	finishedTimeIpCountArray[16]=finishedTime17IpCount;
    	finishedTimeIpCountArray[17]=finishedTime18IpCount;
    	finishedTimeIpCountArray[18]=finishedTime19IpCount;
    	finishedTimeIpCountArray[19]=finishedTime20IpCount;
    	finishedTimeIpCountArray[20]=finishedTime21IpCount;
    	finishedTimeIpCountArray[21]=finishedTime22IpCount;
    	finishedTimeIpCountArray[22]=finishedTime23IpCount;
    	finishedTimeIpCountArray[23]=finishedTime24IpCount;
		
		
		return finishedTimeIpCountArray;
	}
	public void setFinishedTimeIpCountArray(int[] finishedTimeIpCountArray) {
		this.finishedTimeIpCountArray = finishedTimeIpCountArray;
	}
	private int finishedTime1IpCount;//当天某个时间段内的完成IP数
    private int finishedTime2IpCount;
    private int finishedTime3IpCount;
    private int finishedTime4IpCount;
    private int finishedTime5IpCount;
    private int finishedTime6IpCount;
    private int finishedTime7IpCount;
    private int finishedTime8IpCount;
    private int finishedTime9IpCount;
    private int finishedTime10IpCount;
    private int finishedTime11IpCount;
    private int finishedTime12IpCount;
    private int finishedTime13IpCount;
    private int finishedTime14IpCount;
    private int finishedTime15IpCount;
    private int finishedTime16IpCount;
    private int finishedTime17IpCount;
    private int finishedTime18IpCount;
    private int finishedTime19IpCount;
    private int finishedTime20IpCount;
    private int finishedTime21IpCount;
    private int finishedTime22IpCount;
    private int finishedTime23IpCount;
    private int finishedTime24IpCount;
	public long getUrlId() {
		return urlId;
	}
	public void setUrlId(long urlId) {
		this.urlId = urlId;
	}
	public long getUserIdP() {
		return userIdP;
	}
	public void setUserIdP(long userIdP) {
		this.userIdP = userIdP;
	}
	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	public String getSourceUrls() {
		return sourceUrls;
	}
	public void setSourceUrls(String sourceUrls) {
		this.sourceUrls = sourceUrls;
	}
	public int getSourceIsJump() {
		return sourceIsJump;
	}
	public void setSourceIsJump(int sourceIsJump) {
		this.sourceIsJump = sourceIsJump;
	}
	public int getInnerIsJump() {
		return innerIsJump;
	}
	public void setInnerIsJump(int innerIsJump) {
		this.innerIsJump = innerIsJump;
	}
	public int getPassIs() {
		return passIs;
	}
	public void setPassIs(int passIs) {
		this.passIs = passIs;
	}
	public long getCurrentPoint() {
		return currentPoint;
	}
	public void setCurrentPoint(long currentPoint) {
		this.currentPoint = currentPoint;
	}
	public int getTime1IpCount() {
		return time1IpCount;
	}
	public void setTime1IpCount(int time1IpCount) {
		this.time1IpCount = time1IpCount;
	}
	public int getTime2IpCount() {
		return time2IpCount;
	}
	public void setTime2IpCount(int time2IpCount) {
		this.time2IpCount = time2IpCount;
	}
	public int getTime3IpCount() {
		return time3IpCount;
	}
	public void setTime3IpCount(int time3IpCount) {
		this.time3IpCount = time3IpCount;
	}
	public int getTime4IpCount() {
		return time4IpCount;
	}
	public void setTime4IpCount(int time4IpCount) {
		this.time4IpCount = time4IpCount;
	}
	public int getTime5IpCount() {
		return time5IpCount;
	}
	public void setTime5IpCount(int time5IpCount) {
		this.time5IpCount = time5IpCount;
	}
	public int getTime6IpCount() {
		return time6IpCount;
	}
	public void setTime6IpCount(int time6IpCount) {
		this.time6IpCount = time6IpCount;
	}
	public int getTime7IpCount() {
		return time7IpCount;
	}
	public void setTime7IpCount(int time7IpCount) {
		this.time7IpCount = time7IpCount;
	}
	public int getTime8IpCount() {
		return time8IpCount;
	}
	public void setTime8IpCount(int time8IpCount) {
		this.time8IpCount = time8IpCount;
	}
	public int getTime9IpCount() {
		return time9IpCount;
	}
	public void setTime9IpCount(int time9IpCount) {
		this.time9IpCount = time9IpCount;
	}
	public int getTime10IpCount() {
		return time10IpCount;
	}
	public void setTime10IpCount(int time10IpCount) {
		this.time10IpCount = time10IpCount;
	}
	public int getTime11IpCount() {
		return time11IpCount;
	}
	public void setTime11IpCount(int time11IpCount) {
		this.time11IpCount = time11IpCount;
	}
	public int getTime12IpCount() {
		return time12IpCount;
	}
	public void setTime12IpCount(int time12IpCount) {
		this.time12IpCount = time12IpCount;
	}
	public int getTime13IpCount() {
		return time13IpCount;
	}
	public void setTime13IpCount(int time13IpCount) {
		this.time13IpCount = time13IpCount;
	}
	public int getTime14IpCount() {
		return time14IpCount;
	}
	public void setTime14IpCount(int time14IpCount) {
		this.time14IpCount = time14IpCount;
	}
	public int getTime15IpCount() {
		return time15IpCount;
	}
	public void setTime15IpCount(int time15IpCount) {
		this.time15IpCount = time15IpCount;
	}
	public int getTime16IpCount() {
		return time16IpCount;
	}
	public void setTime16IpCount(int time16IpCount) {
		this.time16IpCount = time16IpCount;
	}
	public int getTime17IpCount() {
		return time17IpCount;
	}
	public void setTime17IpCount(int time17IpCount) {
		this.time17IpCount = time17IpCount;
	}
	public int getTime18IpCount() {
		return time18IpCount;
	}
	public void setTime18IpCount(int time18IpCount) {
		this.time18IpCount = time18IpCount;
	}
	public int getTime19IpCount() {
		return time19IpCount;
	}
	public void setTime19IpCount(int time19IpCount) {
		this.time19IpCount = time19IpCount;
	}
	public int getTime20IpCount() {
		return time20IpCount;
	}
	public void setTime20IpCount(int time20IpCount) {
		this.time20IpCount = time20IpCount;
	}
	public int getTime21IpCount() {
		return time21IpCount;
	}
	public void setTime21IpCount(int time21IpCount) {
		this.time21IpCount = time21IpCount;
	}
	public int getTime22IpCount() {
		return time22IpCount;
	}
	public void setTime22IpCount(int time22IpCount) {
		this.time22IpCount = time22IpCount;
	}
	public int getTime23IpCount() {
		return time23IpCount;
	}
	public void setTime23IpCount(int time23IpCount) {
		this.time23IpCount = time23IpCount;
	}
	public int getTime24IpCount() {
		return time24IpCount;
	}
	public void setTime24IpCount(int time24IpCount) {
		this.time24IpCount = time24IpCount;
	}
	public int getFinishedTime1IpCount() {
		return finishedTime1IpCount;
	}
	public void setFinishedTime1IpCount(int finishedTime1IpCount) {
		this.finishedTime1IpCount = finishedTime1IpCount;
	}
	public int getFinishedTime2IpCount() {
		return finishedTime2IpCount;
	}
	public void setFinishedTime2IpCount(int finishedTime2IpCount) {
		this.finishedTime2IpCount = finishedTime2IpCount;
	}
	public int getFinishedTime3IpCount() {
		return finishedTime3IpCount;
	}
	public void setFinishedTime3IpCount(int finishedTime3IpCount) {
		this.finishedTime3IpCount = finishedTime3IpCount;
	}
	public int getFinishedTime4IpCount() {
		return finishedTime4IpCount;
	}
	public void setFinishedTime4IpCount(int finishedTime4IpCount) {
		this.finishedTime4IpCount = finishedTime4IpCount;
	}
	public int getFinishedTime5IpCount() {
		return finishedTime5IpCount;
	}
	public void setFinishedTime5IpCount(int finishedTime5IpCount) {
		this.finishedTime5IpCount = finishedTime5IpCount;
	}
	public int getFinishedTime6IpCount() {
		return finishedTime6IpCount;
	}
	public void setFinishedTime6IpCount(int finishedTime6IpCount) {
		this.finishedTime6IpCount = finishedTime6IpCount;
	}
	public int getFinishedTime7IpCount() {
		return finishedTime7IpCount;
	}
	public void setFinishedTime7IpCount(int finishedTime7IpCount) {
		this.finishedTime7IpCount = finishedTime7IpCount;
	}
	public int getFinishedTime8IpCount() {
		return finishedTime8IpCount;
	}
	public void setFinishedTime8IpCount(int finishedTime8IpCount) {
		this.finishedTime8IpCount = finishedTime8IpCount;
	}
	public int getFinishedTime9IpCount() {
		return finishedTime9IpCount;
	}
	public void setFinishedTime9IpCount(int finishedTime9IpCount) {
		this.finishedTime9IpCount = finishedTime9IpCount;
	}
	public int getFinishedTime10IpCount() {
		return finishedTime10IpCount;
	}
	public void setFinishedTime10IpCount(int finishedTime10IpCount) {
		this.finishedTime10IpCount = finishedTime10IpCount;
	}
	public int getFinishedTime11IpCount() {
		return finishedTime11IpCount;
	}
	public void setFinishedTime11IpCount(int finishedTime11IpCount) {
		this.finishedTime11IpCount = finishedTime11IpCount;
	}
	public int getFinishedTime12IpCount() {
		return finishedTime12IpCount;
	}
	public void setFinishedTime12IpCount(int finishedTime12IpCount) {
		this.finishedTime12IpCount = finishedTime12IpCount;
	}
	public int getFinishedTime13IpCount() {
		return finishedTime13IpCount;
	}
	public void setFinishedTime13IpCount(int finishedTime13IpCount) {
		this.finishedTime13IpCount = finishedTime13IpCount;
	}
	public int getFinishedTime14IpCount() {
		return finishedTime14IpCount;
	}
	public void setFinishedTime14IpCount(int finishedTime14IpCount) {
		this.finishedTime14IpCount = finishedTime14IpCount;
	}
	public int getFinishedTime15IpCount() {
		return finishedTime15IpCount;
	}
	public void setFinishedTime15IpCount(int finishedTime15IpCount) {
		this.finishedTime15IpCount = finishedTime15IpCount;
	}
	public int getFinishedTime16IpCount() {
		return finishedTime16IpCount;
	}
	public void setFinishedTime16IpCount(int finishedTime16IpCount) {
		this.finishedTime16IpCount = finishedTime16IpCount;
	}
	public int getFinishedTime17IpCount() {
		return finishedTime17IpCount;
	}
	public void setFinishedTime17IpCount(int finishedTime17IpCount) {
		this.finishedTime17IpCount = finishedTime17IpCount;
	}
	public int getFinishedTime18IpCount() {
		return finishedTime18IpCount;
	}
	public void setFinishedTime18IpCount(int finishedTime18IpCount) {
		this.finishedTime18IpCount = finishedTime18IpCount;
	}
	public int getFinishedTime19IpCount() {
		return finishedTime19IpCount;
	}
	public void setFinishedTime19IpCount(int finishedTime19IpCount) {
		this.finishedTime19IpCount = finishedTime19IpCount;
	}
	public int getFinishedTime20IpCount() {
		return finishedTime20IpCount;
	}
	public void setFinishedTime20IpCount(int finishedTime20IpCount) {
		this.finishedTime20IpCount = finishedTime20IpCount;
	}
	public int getFinishedTime21IpCount() {
		return finishedTime21IpCount;
	}
	public void setFinishedTime21IpCount(int finishedTime21IpCount) {
		this.finishedTime21IpCount = finishedTime21IpCount;
	}
	public int getFinishedTime22IpCount() {
		return finishedTime22IpCount;
	}
	public void setFinishedTime22IpCount(int finishedTime22IpCount) {
		this.finishedTime22IpCount = finishedTime22IpCount;
	}
	public int getFinishedTime23IpCount() {
		return finishedTime23IpCount;
	}
	public void setFinishedTime23IpCount(int finishedTime23IpCount) {
		this.finishedTime23IpCount = finishedTime23IpCount;
	}
	public int getFinishedTime24IpCount() {
		return finishedTime24IpCount;
	}
	public void setFinishedTime24IpCount(int finishedTime24IpCount) {
		this.finishedTime24IpCount = finishedTime24IpCount;
	}
    
}
