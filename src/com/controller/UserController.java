package com.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.base.BaseControllers;
import com.common.Criteria;
import com.common.GridReturn;
import com.model.XiaocaiCharge;
import com.model.XiaocaiInput;
import com.model.XiaocaiUrls;
import com.model.XiaocaiUser;
import com.model.XiaocaiUserLog;
import com.model.XiaocaiWebConfig;
import com.service.UserService;
import com.service.WebConfigInfoService;
import com.service.XiaocaiChargeService;
import com.service.XiaocaiInputService;
import com.service.XiaocaiUrlsService;
import com.service.XiaocaiUserLogService;
import com.service.XiaocaiUserService;
import com.util.JSONUtil;
import com.util.SessionUtils;
import com.util.WebConstants;

/**
 * 控控制层
 * 
 * @author tiangai
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseControllers {
	@Resource
	private UserService userService;
	@Resource
	private XiaocaiUrlsService xiaocaiUrlsService;
	@Resource
	private XiaocaiUserService xiaocaiUserService;
	@Resource
	private XiaocaiInputService xiaocaiInputService;
	@Resource
	private XiaocaiChargeService xiaocaiChargeService;
	@Resource
	private WebConfigInfoService webConfigInfoService;
	@Resource
	private XiaocaiUserLogService xiaocaiUserLogService;

	
	
	
	@RequestMapping({ "/index", "", "/" })
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		XiaocaiUser userSession = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		userSession = xiaocaiUserService
				.selectByPrimaryKey(userSession.getId());
		session.setAttribute(WebConstants.CURRENT_USER, userSession);
		Map<String, Object> context = new HashMap<String, Object>();
		XiaocaiWebConfig xiaocaiWebConfig = webConfigInfoService
				.getXiaocaiWebConfig();
		context.put("percent1",
				(float) xiaocaiWebConfig.getLevel_point_refer_one() / 100);
		context.put("percent2",
				(float) xiaocaiWebConfig.getLevel_point_refer_two() / 100);
		context.put("percent3",
				(float) xiaocaiWebConfig.getLevel_point_refer_three() / 100);
		context.put("percent4",
				(float) xiaocaiWebConfig.getLevel_point_refer_four() / 100);
		
		context.put("refer_percent1",
				(float) xiaocaiWebConfig.getLevel_refer_agent1() );
		context.put("refer_percent2",
				(float) xiaocaiWebConfig.getLevel_refer_agent2() );
		context.put("refer_percent3",
				(float) xiaocaiWebConfig.getLevel_refer_agent3());
		context.put("refer_percent4",
				(float) xiaocaiWebConfig.getLevel_refer_agent4());
		
		
		
		context.put("domain", xiaocaiWebConfig.getWeb_domain());
		return forword("user/index", context);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ModelAndView logout(HttpSession session) {
		Map<String, Object> context = getRootMap();
		session.setAttribute(WebConstants.CURRENT_USER, null);
		return forword("login", context);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void delete(String ids, HttpServletResponse response) {
		// 将json字符串转成int []
		int[] id = JSONUtil.fromJson(ids, new TypeReference<int[]>() {
		});
		userService.deleteBatch(id);
		writerJson(response, successMsg("删除成功"));

	}

	/**
	 * 添加网址
	 * 
	 * @return
	 */
	@RequestMapping({ "/add_url" })
	public ModelAndView addUrl() {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/urls_add", context);
	}

	/**
	 * 添加网址form表单
	 * 
	 * @param xiaocaiUrls
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/add_urls.json")
	@ResponseBody
	public void addUrls(XiaocaiUrls xiaocaiUrls, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (null != userObject)
			xiaocaiUrls.setUserIdP(userObject.getId());
		// 查看积分是否足够
		if (userObject.getUserPoint() < xiaocaiUrls.getCurrentPoint()) {
			writerJson(response, errorMsg("积分不足!"));
			return;
		}
		xiaocaiUrls.setPassIs(1);//默认审核通过
		xiaocaiUrls = xiaocaiUrlsService.add(xiaocaiUrls);
		if (null == xiaocaiUrls) {
			writerJson(response, errorMsg("添加任务失败，稍后添加!"));
			return;
		}
		// 扣除相应的积分
		xiaocaiUserService.addPoint(userObject.getId(),
				0 - xiaocaiUrls.getCurrentPoint());

		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_TASK_USE_POINT,
				"成功添加任务，消费积分" + xiaocaiUrls.getCurrentPoint() + "个");
		writerJson(response, successMsg("添加成功!"));
	}

	@RequestMapping({ "/urls_list" })
	public ModelAndView urlsList() {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/urls_list", context);
	}

	/**
	 * 返回url列表
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/list_urls.json")
	@ResponseBody
	public void listUrls(Criteria criteria, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		criteria.setOrder("url_id");
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("userIdP", userObject.getId());
		criteria.setCondition(condition);
		int total = xiaocaiUrlsService.getTotal(criteria);
		List<XiaocaiUrls> list = xiaocaiUrlsService
				.selectByCriteriaPagination(criteria);
		writerJson(response, new GridReturn(total, list).toString());
	}

	/**
	 * 批量删除网址
	 * 
	 * @param ids
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/delete_urls.json")
	@ResponseBody
	public void deleteUrls(int[] ids, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (null == ids) {
			writerJson(response, errorMsg("最少选择一个任务"));
			return;
		}
		boolean isError = false;
		long urlsCurrentPoint = 0;
		for (int i = 0; i < ids.length; i++) {
			XiaocaiUrls xiaocaiUrls = xiaocaiUrlsService
					.getXiaocaiUrlsById(ids[i]);
			urlsCurrentPoint += xiaocaiUrls.getCurrentPoint();
			if (xiaocaiUrls.getUserIdP() != userObject.getId()) {
				writerJson(response, errorMsg("非法删除任务"));
				isError = true;
				break;
			}
		}
		if (isError)
			return;
		// 积分退回
		xiaocaiUrlsService.deleteBatch(ids);
		// 用户积分增加
		XiaocaiUser xiaocaiUser = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		xiaocaiUser.setUserPoint(xiaocaiUser.getUserPoint() + urlsCurrentPoint);
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUser);
		userObject.setUserPoint(xiaocaiUser.getUserPoint() + urlsCurrentPoint);
		request.getSession()
				.setAttribute(WebConstants.CURRENT_USER, userObject);
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.DEL_TASK_BACK_POINT, "删除任务积分退回"
						+ urlsCurrentPoint + "个");

		writerJson(response, successMsg("删除成功"));
	}

	/***
	 * 充入积分
	 * 
	 * @param urlId
	 * @param currentPoint
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/add_urls_point.json")
	@ResponseBody
	public void addUrlsPoint(int urlId, long currentPoint,
			HttpServletRequest request, HttpServletResponse response) {
		if (0 >= currentPoint || 0 >= urlId) {
			writerJson(response, errorMsg("非法操作"));
			return;
		}

		XiaocaiUrls xiaocaiUrls = xiaocaiUrlsService.getXiaocaiUrlsById(urlId);
		if (null == xiaocaiUrls) {
			writerJson(response, errorMsg("任务不存在"));
			return;
		}
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		XiaocaiUser xiaocaiUser = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		if (xiaocaiUser.getUserPoint() < currentPoint) {
			writerJson(response, errorMsg("用户积分不足"));
			return;
		}
		// 用户积分减少
		xiaocaiUser.setUserPoint(xiaocaiUser.getUserPoint() - currentPoint);
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUser);
		// 网址积分增加
		xiaocaiUrls.setCurrentPoint(xiaocaiUrls.getCurrentPoint()
				+ currentPoint);
		xiaocaiUrlsService.updateByPrimaryKey(xiaocaiUrls);
		userObject.setUserPoint(xiaocaiUser.getUserPoint());
		request.getSession()
				.setAttribute(WebConstants.CURRENT_USER, userObject);
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_TASK_USE_POINT, "成功给任务加积分，消费积分"
						+ xiaocaiUrls.getCurrentPoint() + "个");
		writerJson(response, successMsg("充入积分成功"));
	}

	/**
	 * 跳转到编辑任务页
	 * 
	 * @param urlId
	 * @return
	 */
	@RequestMapping({ "/urls_edit.html" })
	public ModelAndView urlsEdit(int urlId) {
		Map<String, Object> context = new HashMap<String, Object>();
		// 获取urlId对应的网址
		XiaocaiUrls xiaocaiUrls = xiaocaiUrlsService.getXiaocaiUrlsById(urlId);
		context.put("xiaocaiUrls", xiaocaiUrls);
		return forword("user/urls_edit", context);
	}

	/**
	 * 修改网址任务
	 * 
	 * @param xiaocaiUrls
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/edit_urls.json")
	@ResponseBody
	public void editUrls(XiaocaiUrls xiaocaiUrls, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (null == xiaocaiUrls || (null == userObject)) {
			writerJson(response, errorMsg("修改失败"));
			return;
		}
		XiaocaiUrls xiaocaiUrlsOld = xiaocaiUrlsService
				.getXiaocaiUrlsById(xiaocaiUrls.getUrlId());
		if (xiaocaiUrlsOld.getUserIdP() != userObject.getId()) {
			writerJson(response, errorMsg("非法修改任务"));
			return;
		}
		xiaocaiUrls.setCurrentPoint(xiaocaiUrlsOld.getCurrentPoint());
		xiaocaiUrls.setUserIdP(userObject.getId());
		xiaocaiUrls.setPassIs(1);//默认审核通过
		xiaocaiUrlsService.updateByPrimaryKey(xiaocaiUrls);
		writerJson(response, successMsg("修改成功"));
	}

	/**
	 * 修改密码
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/password")
	public ModelAndView passwordEdit(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/password", context);
	}

	/**
	 * 修改密码
	 * 
	 * @param xiaocaiUrls
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/re_password.json")
	@ResponseBody
	public void rePassword(String oldpass, String userPassword,
			HttpServletRequest request, HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (null == userPassword || (null == userObject || null == oldpass)) {
			writerJson(response, errorMsg("修改失败"));
			return;
		}
		XiaocaiUser xiaocaiUserOld = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		if (null == xiaocaiUserOld
				|| (!xiaocaiUserOld.getUserPassword().equals(oldpass))) {
			writerJson(response, errorMsg("原密码不正确"));
			return;
		}
		xiaocaiUserOld.setUserPassword(userPassword);// 修改密码
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUserOld);
		writerJson(response, successMsg("修改成功"));
	}

	/**
	 * 在线充值
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/pay")
	public ModelAndView payEdit(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/pay", context);
	}

	@RequestMapping(value = "/pay.json")
	@ResponseBody
	public void payForm(XiaocaiInput xiaocaiInput, String authCodeReg,
			HttpServletRequest request, HttpServletResponse response) {
		String vcode = SessionUtils.getValidateCode(request);
		if (null != vcode && !"".equals(vcode)) {
			// 清除验证码，确保验证码只能用一次
			SessionUtils.removeValidateCode(request);
			if (StringUtils.isBlank(authCodeReg)) {
				writerJson(response, errorMsg("验证码不能为空"));
				return;
			}
			// 判断验证码是否正确
			if (!(vcode.toLowerCase()).equals(authCodeReg)
					&& !authCodeReg.equals(vcode)) {
				writerJson(response, errorMsg("验证码输入错误"));
				return;
			}
		} else {
			writerJson(response, errorMsg("验证码已失效，请刷新验证码"));
			return;
		}
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (null == xiaocaiInput || (null == userObject)) {
			writerJson(response, errorMsg("充值失败"));
			return;
		}
		xiaocaiInput.setInputDate(new Date());
		xiaocaiInput.setUserIdP(userObject.getId());
		xiaocaiInputService.addxiaocaiInputService(xiaocaiInput);
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_INPUT_MONEY,
				"充值申请提交" + xiaocaiInput.getInputMoney() + "元");

		writerJson(response, successMsg("提交成功"));
	}

	/**
	 * 提现
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/payout")
	public ModelAndView payoutEdit(XiaocaiUser xiaocaiUser) {
		int payOutMoney=webConfigInfoService.getXiaocaiWebConfig().getMin_out_pay_money();
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("payOutMoney", payOutMoney);
		return forword("user/payout", context);
	}

	@RequestMapping(value = "/payout.json")
	@ResponseBody
	public void payoutForm(XiaocaiInput xiaocaiInput, String authCodeReg,
			HttpServletRequest request, HttpServletResponse response) {
		String vcode = SessionUtils.getValidateCode(request);
		if (null != vcode && !"".equals(vcode)) {
			// 清除验证码，确保验证码只能用一次
			SessionUtils.removeValidateCode(request);
			if (StringUtils.isBlank(authCodeReg)) {
				writerJson(response, errorMsg("验证码不能为空"));
				return;
			}
			// 判断验证码是否正确
			if (!(vcode.toLowerCase()).equals(authCodeReg)
					&& !authCodeReg.equals(vcode)) {
				writerJson(response, errorMsg("验证码输入错误"));
				return;
			}
		} else {
			writerJson(response, errorMsg("验证码已失效，请刷新验证码"));
			return;
		}
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (null == xiaocaiInput || (null == userObject)) {
			writerJson(response, errorMsg("提现失败"));
			return;
		}
		int minPayOutMoney=webConfigInfoService.getXiaocaiWebConfig().getMin_out_pay_money();
		if (minPayOutMoney > xiaocaiInput.getInputMoney()) {
			writerJson(response, errorMsg("提现金额最少"+minPayOutMoney));
			return;
		}
		XiaocaiUser xiaocaiUser = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		if (xiaocaiUser.getUserMoney() < xiaocaiInput.getInputMoney()) {
			writerJson(response, errorMsg("提现金额不足"));
			return;
		}
		xiaocaiInput.setInputType(1);
		xiaocaiInput.setInputDate(new Date());
		xiaocaiInput.setUserIdP(userObject.getId());
		xiaocaiInputService.addxiaocaiInputService(xiaocaiInput);
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_PAY_OUT_MONEY,
				"提现申请提交" + xiaocaiInput.getInputMoney() + "元");

		writerJson(response, successMsg("提交成功"));
	}

	/**
	 * 财务明细
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/money")
	public ModelAndView moneyEdit(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/money", context);
	}

	@RequestMapping(value = "/list_money.json")
	@ResponseBody
	public void listMoney(Criteria criteria, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("userIdP", userObject.getId());
		criteria.setCondition(condition);
		int total = xiaocaiInputService.getTotal(criteria);
		List<XiaocaiInput> list = xiaocaiInputService
				.selectByCriteriaPagination(criteria);
		writerJson(response, new GridReturn(total, list).toString());
	}

	/**
	 * 购买积分
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/point_buy")
	public ModelAndView pointBuy(HttpServletRequest request) {
		Map<String, Object> context = new HashMap<String, Object>();
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		//获取不同级别人员购买积分的价格
		XiaocaiWebConfig xiaocaiWebConfig=webConfigInfoService.getXiaocaiWebConfig();
		context.put("price", getCanBuyPointByUserLevel((int)userObject.getId()));
		context.put("price1", xiaocaiWebConfig.getPoint_price_1());
		context.put("price2", xiaocaiWebConfig.getPoint_price_2());
		context.put("price3", xiaocaiWebConfig.getPoint_price_3());
		context.put("price4", xiaocaiWebConfig.getPoint_price_4());
		return forword("user/pointbuy", context);
	}

	/**
	 * 积分购买
	 * 
	 * @param inputPoint
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/pointbuy.json")
	@ResponseBody
	public void pointbuy(int inputPoint, HttpServletRequest request,
			HttpServletResponse response) {
		int minBuyPoint=webConfigInfoService.getXiaocaiWebConfig().getMin_buy_point();
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (null == userObject || inputPoint <= minBuyPoint) {
			writerJson(response, errorMsg("积分购买失败，积分最少"+minBuyPoint));
			return;
		}
		XiaocaiUser xiaocaiUser = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		int canBuyPoint=getCanBuyPointByUserLevel(xiaocaiUser.getUserLevel());
		if (xiaocaiUser.getUserMoney() < inputPoint / canBuyPoint) {
			writerJson(response, errorMsg("用户金额不足"));
			return;
		}
		// 购买成功，积分增加，现金减少
		xiaocaiUser.setUserPoint(xiaocaiUser.getUserPoint() + inputPoint);
		xiaocaiUser.setUserMoney(xiaocaiUser.getUserMoney()
				- (inputPoint / canBuyPoint));
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUser);
		// 添加交易记录
		XiaocaiCharge xiaocaiCharge = new XiaocaiCharge();
		xiaocaiCharge.setSellUserId(xiaocaiUser.getId());
		xiaocaiCharge.setChargePoint(inputPoint);
		xiaocaiCharge.setChargeMoney(((float) inputPoint / canBuyPoint));
		xiaocaiCharge.setDealDate(new Date());
		xiaocaiCharge.setSellDate(new Date());
		xiaocaiCharge.setChargeType(1);// 设置交易为购买
		xiaocaiChargeService.addXiaocaiChargeService(xiaocaiCharge);
		// 积分购买日志
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_BUY_POINT, "购买积分" + inputPoint + "个");
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_BUY_POINT_USE_MONEY, "购买积分花费" + inputPoint
						/ canBuyPoint + "元");

		writerJson(response, successMsg("购买成功"));

	}

	private int getCanBuyPointByUserLevel(int userLevel) {
		XiaocaiWebConfig xiaocaiWebConfig=webConfigInfoService.getXiaocaiWebConfig();
		int pricePoint=10000;
		switch (userLevel) {
		case 0:
			pricePoint=xiaocaiWebConfig.getPoint_price_1();
			break;
		case 1:
			pricePoint=xiaocaiWebConfig.getPoint_price_2();
			break;	
		case 2:
			pricePoint=xiaocaiWebConfig.getPoint_price_3();
			break;	
		case 3:
			pricePoint=xiaocaiWebConfig.getPoint_price_4();
			break;	
		default:
			break;
		}
		return pricePoint;
	}

	@RequestMapping(value = "/exchange")
	public ModelAndView exchangeEdit(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		int oneRmbExchangePoint=webConfigInfoService.getXiaocaiWebConfig().getOne_rmb_exchange_point();
		context.put("oneRmbExchangePoint", oneRmbExchangePoint);
		return forword("user/exchange", context);
	}

	/**
	 * 积分兑换
	 * 
	 * @param inputPoint
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/exchange.json")
	@ResponseBody
	public void exchange(int inputPoint, String authCodeReg,
			HttpServletRequest request, HttpServletResponse response) {
		String vcode = SessionUtils.getValidateCode(request);
		if (null != vcode && !"".equals(vcode)) {
			// 清除验证码，确保验证码只能用一次
			SessionUtils.removeValidateCode(request);
			if (StringUtils.isBlank(authCodeReg)) {
				writerJson(response, errorMsg("验证码不能为空"));
				return;
			}
			// 判断验证码是否正确
			if (!(vcode.toLowerCase()).equals(authCodeReg)
					&& !authCodeReg.equals(vcode)) {
				writerJson(response, errorMsg("验证码输入错误"));
				return;
			}
		} else {
			writerJson(response, errorMsg("验证码已失效，请刷新验证码"));
			return;
		}
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		int minExchangePoint =webConfigInfoService.getXiaocaiWebConfig().getMin_exchange_point();
		if (null == userObject || inputPoint <= minExchangePoint) {
			writerJson(response, errorMsg("兑换失败，兑换积分最少"+minExchangePoint));
			return;
		}
		XiaocaiUser xiaocaiUser = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		if (xiaocaiUser.getUserPoint() < inputPoint) {
			writerJson(response, errorMsg("用户积分不足"));
			return;
		}
		int oneRmbExchangePoint=webConfigInfoService.getXiaocaiWebConfig().getOne_rmb_exchange_point();
		xiaocaiUser.setUserPoint(xiaocaiUser.getUserPoint()
				- (float) inputPoint);
		xiaocaiUser.setUserMoney(xiaocaiUser.getUserMoney()
				+ ((float) inputPoint / oneRmbExchangePoint));
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUser);
		// 添加交易记录
		XiaocaiCharge xiaocaiCharge = new XiaocaiCharge();
		xiaocaiCharge.setSellUserId(xiaocaiUser.getId());
		xiaocaiCharge.setChargePoint((float) inputPoint);
		xiaocaiCharge.setChargeMoney(((float) inputPoint / oneRmbExchangePoint));
		xiaocaiCharge.setDealDate(new Date());
		xiaocaiCharge.setSellDate(new Date());
		xiaocaiChargeService.addXiaocaiChargeService(xiaocaiCharge);

		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_SELL_POINT, "出售积分" + inputPoint + "个");
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_SELL_POINT_GET_MONEY, "出售积分得到" + inputPoint
						/ oneRmbExchangePoint + "元");

		writerJson(response, successMsg("兑换成功"));
	}

	@RequestMapping(value = "/point_move")
	public ModelAndView pointMove(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/point_move", context);
	}

	@RequestMapping(value = "/point_move.json")
	@ResponseBody
	public void point_Move(float inputMovePoint, String authCodeReg,
			String inputMoveUserName, float inputChargeMoney,
			HttpServletRequest request, HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		if (userObject.getUserLevel()<=0) {
			writerJson(response, errorMsg("只有代理才可进行积分转让"));
			return;
		}
		String vcode = SessionUtils.getValidateCode(request);
		if (null != vcode && !"".equals(vcode)) {
			// 清除验证码，确保验证码只能用一次
			SessionUtils.removeValidateCode(request);
			if (StringUtils.isBlank(authCodeReg)) {
				writerJson(response, errorMsg("验证码不能为空"));
				return;
			}
			// 判断验证码是否正确
			if (!(vcode.toLowerCase()).equals(authCodeReg)
					&& !authCodeReg.equals(vcode)) {
				writerJson(response, errorMsg("验证码输入错误"));
				return;
			}
		} else {
			writerJson(response, errorMsg("验证码已失效，请刷新验证码"));
			return;
		}
		// 查找是否存在接收积分的帐号
		XiaocaiUser xiaocaiUserBuy = xiaocaiUserService
				.selectByXiaocaiUserName(inputMoveUserName);
		if (null == xiaocaiUserBuy || xiaocaiUserBuy.getId() == 0) {
			writerJson(response, errorMsg("接收积分帐号不存在"));
			return;
		}
		XiaocaiUser xiaocaiUserOld = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		// 冻结用户积分
		xiaocaiUserOld.setUserPoint(xiaocaiUserOld.getUserPoint()
				- inputMovePoint);
		xiaocaiUserOld.setFrozenPoint(xiaocaiUserOld.getFrozenPoint()
				+ inputMovePoint);
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUserOld);
		// 等待接受方接收积分
		XiaocaiCharge xiaocaiCharge = new XiaocaiCharge();
		xiaocaiCharge.setSellUserId(xiaocaiUserOld.getId());
		xiaocaiCharge.setBuyUserId(xiaocaiUserBuy.getId());
		xiaocaiCharge.setChargePoint(inputMovePoint);
		xiaocaiCharge.setChargeMoney((float) inputChargeMoney);
		xiaocaiCharge.setDealDate(new Date());
		xiaocaiCharge.setSellDate(new Date());
		xiaocaiCharge.setChargeType(2);// 交易类型为积分转移
		xiaocaiCharge.setSellStatus(1);// 标记交易状态为交易中
		xiaocaiChargeService.addXiaocaiChargeService(xiaocaiCharge);
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_MOVE_POINT, "转移积分" + inputMovePoint + "个");
		writerJson(response, successMsg("转让成功"));
	}

	/**
	 * 积分转让取消
	 * 
	 * @param id
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/cancle_point_move.json")
	@ResponseBody
	public void canclePointMove(int id, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		XiaocaiCharge xiaocaiCharge = xiaocaiChargeService
				.selectByPrimaryKey(id);
		if ((null == xiaocaiCharge) || (xiaocaiCharge.getSellStatus() != 1)) {
			writerJson(response, errorMsg("此交易不可撤销"));
			return;
		}
		if (xiaocaiCharge.getSellUserId() != userObject.getId()) {
			writerJson(response, errorMsg("越权操作"));
			return;
		}
		xiaocaiCharge.setSellStatus(2);// 设置交易退回
		xiaocaiChargeService.updateXiaocaiChargeService(xiaocaiCharge);
		XiaocaiUser xiaocaiUserOld = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		// 冻结用户积分退回
		xiaocaiUserOld.setUserPoint(xiaocaiUserOld.getUserPoint()
				+ xiaocaiCharge.getChargePoint());
		xiaocaiUserOld.setFrozenPoint(xiaocaiUserOld.getFrozenPoint()
				- xiaocaiCharge.getChargePoint());
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUserOld);
		xiaocaiUserLogService.addXiaocaiUserLogService(userObject.getId(),
				XiaocaiUserLog.USER_MOVE_POINT_CANCLE,
				"转移积分取消" + xiaocaiCharge.getChargePoint() + "个");

		writerJson(response, successMsg("撤销成功"));
	}

	/**
	 * 接收积分转让
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/accept_point")
	public ModelAndView AcceptPointMove(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/accept_point", context);
	}

	@RequestMapping(value = "/list_accept_point.json")
	@ResponseBody
	public void listAcceptPoint(Criteria criteria, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("buyUserId", userObject.getId());
		criteria.setCondition(condition);
		int total = xiaocaiChargeService.getTotal(criteria);
		List<XiaocaiCharge> list = xiaocaiChargeService
				.selectByCriteriaPagination(criteria);
		writerJson(response, new GridReturn(total, list).toString());
	}

	@RequestMapping(value = "/accept_point_move.json")
	@ResponseBody
	public void acceptPointMove(int id, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		XiaocaiCharge xiaocaiCharge = xiaocaiChargeService
				.selectByPrimaryKey(id);
		if ((null == xiaocaiCharge) || (xiaocaiCharge.getSellStatus() != 1)) {
			writerJson(response, errorMsg("此交易不可继续"));
			return;
		}
		if (xiaocaiCharge.getBuyUserId() != userObject.getId()) {
			writerJson(response, errorMsg("越权操作"));
			return;
		}
		// 设置买出积分的人信息
		XiaocaiUser buyXiaocaiUserOld = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		if (buyXiaocaiUserOld.getUserMoney() < xiaocaiCharge.getChargeMoney()) {
			writerJson(response, errorMsg("可用金额不足，请充值后操作"));
			return;
		}
		buyXiaocaiUserOld.setUserPoint(buyXiaocaiUserOld.getUserPoint()
				+ xiaocaiCharge.getChargePoint());
		buyXiaocaiUserOld.setUserMoney((float) (buyXiaocaiUserOld
				.getUserMoney() - xiaocaiCharge.getChargeMoney()));
		xiaocaiUserService.updateByPrimaryKey(buyXiaocaiUserOld);
		// 设置卖的人的信息
		XiaocaiUser sellXiaocaiUserOld = xiaocaiUserService
				.selectByPrimaryKey(xiaocaiCharge.getSellUserId());
		sellXiaocaiUserOld.setFrozenPoint(sellXiaocaiUserOld.getFrozenPoint()
				- xiaocaiCharge.getChargePoint());
		sellXiaocaiUserOld.setUserMoney((float) (sellXiaocaiUserOld
				.getUserMoney() + xiaocaiCharge.getChargeMoney()));
		xiaocaiUserService.updateByPrimaryKey(sellXiaocaiUserOld);
		// 更新交易信息
		xiaocaiCharge.setSellStatus(0);// 设置交易完成
		xiaocaiChargeService.updateXiaocaiChargeService(xiaocaiCharge);
		// 日志
		xiaocaiUserLogService.addXiaocaiUserLogService(
				buyXiaocaiUserOld.getId(),
				XiaocaiUserLog.USER_ACCEPT_BUY_POINT,
				"成功接收积分" + xiaocaiCharge.getChargePoint());
		xiaocaiUserLogService.addXiaocaiUserLogService(
				buyXiaocaiUserOld.getId(),
				XiaocaiUserLog.USER_ACCEPT_BUY_POINT,
				"接收积分花费" + xiaocaiCharge.getChargeMoney() + "元");

		xiaocaiUserLogService.addXiaocaiUserLogService(
				sellXiaocaiUserOld.getId(),
				XiaocaiUserLog.USER_ACCEPT_BUY_POINT,
				"成功卖出积分" + xiaocaiCharge.getChargePoint());
		xiaocaiUserLogService.addXiaocaiUserLogService(
				sellXiaocaiUserOld.getId(),
				XiaocaiUserLog.USER_ACCEPT_BUY_POINT,
				"卖出积分赚取" + xiaocaiCharge.getChargeMoney() + "元");

		writerJson(response, successMsg("接收积分成功"));
	}

	/**
	 * 积分明细
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/point")
	public ModelAndView pointEdit(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/point", context);
	}

	@RequestMapping(value = "/list_point.json")
	@ResponseBody
	public void listPoint(Criteria criteria, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("sellUserId", userObject.getId());
		criteria.setCondition(condition);
		int total = xiaocaiChargeService.getTotal(criteria);
		List<XiaocaiCharge> list = xiaocaiChargeService
				.selectByCriteriaPagination(criteria);
		writerJson(response, new GridReturn(total, list).toString());
	}

	/**
	 * 用户升级
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/level")
	public ModelAndView levelEdit(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		XiaocaiWebConfig xiaocaiWebConfig = webConfigInfoService
				.getXiaocaiWebConfig();
		context.put("Level1", xiaocaiWebConfig.getLevel1_upgrade());
		context.put("Level2", xiaocaiWebConfig.getLevel2_upgrade());
		context.put("Level3", xiaocaiWebConfig.getLevel3_upgrade());
		// 赠送积分数
		context.put("Level1_upgrade_gift_point",
				xiaocaiWebConfig.getLevel1_upgrade_gift_point());
		context.put("Level2_upgrade_gift_point",
				xiaocaiWebConfig.getLevel2_upgrade_gift_point());
		context.put("Level3_upgrade_gift_point",
				xiaocaiWebConfig.getLevel3_upgrade_gift_point());
		// 优惠折扣
		context.put("Point_price_2", xiaocaiWebConfig.getPoint_price_2());// 铜牌
		context.put("Point_price_3", xiaocaiWebConfig.getPoint_price_3());
		context.put("Point_price_4", xiaocaiWebConfig.getPoint_price_4());// 金牌

		return forword("user/level", context);
	}

	/**
	 * 用户升级表单提交
	 * 
	 * @param criteria
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/user_level.json")
	@ResponseBody
	public void userLevel(int userLevel, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		XiaocaiUser xiaocaiUserOld = xiaocaiUserService
				.selectByPrimaryKey(userObject.getId());
		// 查看用户升级的级别比原来的级别要高
		if (xiaocaiUserOld.getUserLevel() >= userLevel) {
			writerJson(response, errorMsg("当前级别比要升级的级别高，无需升级！"));
			return;
		}
		switch (userLevel) {
		case 1: {
			float needPayOutMoney = getNeedPayOutMoney(
					xiaocaiUserOld.getUserLevel(), 1);
			if (xiaocaiUserOld.getUserMoney() < needPayOutMoney) {
				writerJson(response, errorMsg("资金不足,资金余额应大于" + needPayOutMoney
						+ "元"));
				return;
			} else {
				xiaocaiUserOld.setUserMoney(xiaocaiUserOld.getUserMoney()
						- needPayOutMoney);
				// 赠送积分
				xiaocaiUserOld.setUserPoint(xiaocaiUserOld.getUserPoint()
						+ getGiftPoint(xiaocaiUserOld.getUserLevel(), 1));
				xiaocaiUserOld.setUserLevel(1);
				// 升级日志
				xiaocaiUserLogService.addXiaocaiUserLogService(
						xiaocaiUserOld.getId(), XiaocaiUserLog.USER_UPGRADE,
						"升级到铜牌代理,扣款" + needPayOutMoney + "元");
				xiaocaiUserLogService
						.addXiaocaiUserLogService(
								xiaocaiUserOld.getId(),
								XiaocaiUserLog.USER_UPGRADE,
								"升级到铜牌代理,赠送积分"
										+ getGiftPoint(
												xiaocaiUserOld.getUserLevel(),
												1) + "");
				// 给推荐人奖励现金
				addAwardToRefer(xiaocaiUserOld.getReferUserId(),
						needPayOutMoney);
			}
			break;
		}
		case 2: {
			float needPayOutMoney = getNeedPayOutMoney(
					xiaocaiUserOld.getUserLevel(), 2);
			if (xiaocaiUserOld.getUserMoney() < needPayOutMoney) {
				writerJson(response, errorMsg("资金不足,资金余额应大于" + needPayOutMoney
						+ "元"));
				return;
			} else {
				xiaocaiUserOld.setUserMoney(xiaocaiUserOld.getUserMoney()
						- needPayOutMoney);
				// 赠送积分
				xiaocaiUserOld.setUserPoint(xiaocaiUserOld.getUserPoint()
						+ getGiftPoint(xiaocaiUserOld.getUserLevel(), 2));
				xiaocaiUserOld.setUserLevel(2);
				xiaocaiUserLogService.addXiaocaiUserLogService(
						xiaocaiUserOld.getId(), XiaocaiUserLog.USER_UPGRADE,
						"升级到银牌代理,扣款" + needPayOutMoney + "元");
				xiaocaiUserLogService
						.addXiaocaiUserLogService(
								xiaocaiUserOld.getId(),
								XiaocaiUserLog.USER_UPGRADE,
								"升级到银牌代理,赠送积分"
										+ getGiftPoint(
												xiaocaiUserOld.getUserLevel(),
												2) + "");
			}
			addAwardToRefer(xiaocaiUserOld.getReferUserId(), needPayOutMoney);
			break;
		}
		case 3: {
			float needPayOutMoney = getNeedPayOutMoney(
					xiaocaiUserOld.getUserLevel(), 3);
			if (xiaocaiUserOld.getUserMoney() < needPayOutMoney) {
				writerJson(response, errorMsg("资金不足,资金余额应大于" + needPayOutMoney
						+ "元"));
				return;
			} else {
				xiaocaiUserOld.setUserMoney(xiaocaiUserOld.getUserMoney()
						- needPayOutMoney);
				// 赠送积分
				xiaocaiUserOld.setUserPoint(xiaocaiUserOld.getUserPoint()
						+ getGiftPoint(xiaocaiUserOld.getUserLevel(), 3));
				xiaocaiUserOld.setUserLevel(3);
				xiaocaiUserLogService.addXiaocaiUserLogService(
						xiaocaiUserOld.getId(), XiaocaiUserLog.USER_UPGRADE,
						"升级到金牌代理,扣款" + needPayOutMoney + "元");
				xiaocaiUserLogService
						.addXiaocaiUserLogService(
								xiaocaiUserOld.getId(),
								XiaocaiUserLog.USER_UPGRADE,
								"升级到金牌代理,赠送积分"
										+ getGiftPoint(
												xiaocaiUserOld.getUserLevel(),
												3) + "");

			}
			addAwardToRefer(xiaocaiUserOld.getReferUserId(), needPayOutMoney);
			break;
		}
		default: {
			writerJson(response, errorMsg("升级失败"));
			return;
		}
		}
		xiaocaiUserService.updateByPrimaryKey(xiaocaiUserOld);
		writerJson(response, successMsg("升级成功"));
	}

	private void addAwardToRefer(int referUserId, float needPayOutMoney) {
		// 获取推荐人信息
		XiaocaiUser xiaocaiUser = xiaocaiUserService
				.selectByPrimaryKey(referUserId);
		XiaocaiWebConfig xiaocaiWebConfig=webConfigInfoService.getXiaocaiWebConfig();
		float precent=0;
		if (null != xiaocaiUser) {
			switch (xiaocaiUser.getUserLevel()) {
			case 0:
				precent=(float)xiaocaiWebConfig.getLevel_refer_agent1()/100;
				break;
			case 1:
				precent=(float)xiaocaiWebConfig.getLevel_refer_agent2()/100;
				break;
			case 2:
				precent=(float)xiaocaiWebConfig.getLevel_refer_agent3()/100;
				break;
			case 3:
				precent=(float)xiaocaiWebConfig.getLevel_refer_agent4()/100;
				break;
			default:
				break;
			}
		}
		//获取会员应该赚到的钱
		float referMoney=needPayOutMoney*precent;
		xiaocaiUserService.addMoney(referUserId, referMoney);
		xiaocaiUserLogService.addXiaocaiUserLogService(referUserId, XiaocaiUserLog.REFER_AGENT, "推广会员升级，赚取"+referMoney+"元");
	}

	private float getGiftPoint(int oldLevel, int newLevel) {
		XiaocaiWebConfig xiaocaiWebConfig = webConfigInfoService
				.getXiaocaiWebConfig();
		float giftPoint = 0;
		switch (newLevel) {
		case 1:
			giftPoint = xiaocaiWebConfig.getLevel1_upgrade_gift_point();
			break;
		case 2:
			if (oldLevel == 0) {
				giftPoint = xiaocaiWebConfig.getLevel2_upgrade_gift_point();
			} else {
				giftPoint = xiaocaiWebConfig.getLevel2_upgrade_gift_point()
						- xiaocaiWebConfig.getLevel1_upgrade_gift_point();
			}
			break;
		case 3:
			if (oldLevel == 0) {
				giftPoint = xiaocaiWebConfig.getLevel3_upgrade_gift_point();
			}
			if (oldLevel == 1) {
				giftPoint = xiaocaiWebConfig.getLevel3_upgrade_gift_point()
						- xiaocaiWebConfig.getLevel1_upgrade_gift_point();
			}
			if (oldLevel == 2) {
				giftPoint = xiaocaiWebConfig.getLevel3_upgrade_gift_point()
						- xiaocaiWebConfig.getLevel2_upgrade_gift_point();
				;
			}
			break;
		default:
			giftPoint = xiaocaiWebConfig.getLevel1_upgrade_gift_point();
			break;
		}
		return giftPoint;
	}

	private float getNeedPayOutMoney(int oldLevel, int newLevel) {
		XiaocaiWebConfig xiaocaiWebConfig = webConfigInfoService
				.getXiaocaiWebConfig();
		float payMoney = 0;
		switch (newLevel) {
		case 1:
			payMoney = xiaocaiWebConfig.getLevel1_upgrade();
			break;
		case 2:
			if (oldLevel == 0) {
				payMoney = xiaocaiWebConfig.getLevel2_upgrade();
			} else {
				payMoney = xiaocaiWebConfig.getLevel2_upgrade()
						- xiaocaiWebConfig.getLevel1_upgrade();
			}
			break;
		case 3:
			if (oldLevel == 0) {
				payMoney = xiaocaiWebConfig.getLevel3_upgrade();
			}
			if (oldLevel == 1) {
				payMoney = xiaocaiWebConfig.getLevel3_upgrade()
						- xiaocaiWebConfig.getLevel1_upgrade();
			}
			if (oldLevel == 2) {
				payMoney = xiaocaiWebConfig.getLevel3_upgrade()
						- xiaocaiWebConfig.getLevel2_upgrade();
				;
			}
			break;
		default:
			payMoney = xiaocaiWebConfig.getLevel1_upgrade();
			break;
		}
		return payMoney;
	}

	/**
	 * 我的推广下线
	 * 
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/myrefer")
	public ModelAndView myrefer(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/myrefer", context);
	}

	@RequestMapping(value = "/list_refer.json")
	@ResponseBody
	public void listMyRefer(Criteria criteria, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("referUserId", userObject.getId());
		criteria.setCondition(condition);
		int total = xiaocaiUserService.getTotal(criteria);
		List<XiaocaiUser> list = xiaocaiUserService
				.selectByCriteriaPagination(criteria);
		writerJson(response, new GridReturn(total, list).toString());
	}
	/**
	 * 用户操作日志
	 * @param xiaocaiUser
	 * @return
	 */
	@RequestMapping(value = "/operLog")
	public ModelAndView operLog(XiaocaiUser xiaocaiUser) {
		Map<String, Object> context = new HashMap<String, Object>();
		return forword("user/operLog", context);
	}
	
	@RequestMapping(value = "/list_operLog.json")
	@ResponseBody
	public void listOperLog(Criteria criteria, HttpServletRequest request,
			HttpServletResponse response) {
		XiaocaiUser userObject = (XiaocaiUser) request.getSession()
				.getAttribute(WebConstants.CURRENT_USER);
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("userIdP", userObject.getId());
		criteria.setCondition(condition);
		int total = xiaocaiUserLogService.getTotal(criteria);
		List<XiaocaiUserLog> list = xiaocaiUserLogService.selectByCriteriaPagination(criteria);
		writerJson(response, new GridReturn(total, list).toString());
	}
	
}
