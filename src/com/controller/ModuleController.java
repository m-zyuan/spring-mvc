package com.controller;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.common.MsgReturn;
import com.model.User;
import com.model.XiaocaiAdmin;
import com.util.WebConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.base.BaseControllers;
import com.model.Module;
import com.service.ModuleService;
import com.util.JSONUtil;

@Controller
@RequestMapping("/module") 
public class ModuleController extends BaseControllers {
	@Resource
	private ModuleService moduleService;

	@RequestMapping("/tree") 
	public void  tree(Module module,HttpServletRequest request,HttpServletResponse response) throws Exception{
       /* User user=(User)request.getSession(true).getAttribute(WebConstants.CURRENT_USER);
        if(null!=user)
        {
            module.getCondition().put("userid",user.getId());
            List<Module> list = moduleService.selectByCriteria(module);
            writerJson(response, JSONUtil.toJson(list));
        }else {
            writerJson(response,errorMsg("检测到您还没有登录，请重新登录"));
        }*/
		XiaocaiAdmin xiaocaiAdmin=(XiaocaiAdmin)request.getSession(true).getAttribute(WebConstants.CURRENT_ADMIN);
        if(null!=xiaocaiAdmin)
        {
            module.getCondition().put("userid",xiaocaiAdmin.getId());
            List<Module> list = moduleService.selectByCriteria(module);
            writerJson(response, JSONUtil.toJson(list));
        }else {
            writerJson(response,errorMsg("检测到您还没有登录，请重新登录"));
        }


	}

}
