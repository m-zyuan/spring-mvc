package com.dao;

import java.util.List;

import com.base.BaseMapper;
import com.common.Criteria;
import com.model.XiaocaiUrls;

public interface XiaocaiUrlsMapper extends BaseMapper<XiaocaiUrls>  {
	List<XiaocaiUrls> selectByRandom(Criteria criteria);
	void addPoint(Criteria criteria);
	int passBatch(int [] ids);
	void updataFinishedTimeZero();
}
