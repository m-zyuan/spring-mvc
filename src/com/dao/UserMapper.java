package com.dao;

import com.base.BaseMapper;
import com.model.User;
/**
 * 数据访问层(实现层由Mybatis生成)
 * @author tiangai
 */
public interface UserMapper extends BaseMapper<User> {

}
