package com.dao;

import com.base.BaseMapper;
import com.model.XiaocaiUser;

public interface XiaocaiUserMapper extends BaseMapper<XiaocaiUser>{
	void addPoint(XiaocaiUser xiaocaiUser);
	void addMoney(XiaocaiUser xiaocaiUser);
	void addWarned(XiaocaiUser xiaocaiUser);
}
