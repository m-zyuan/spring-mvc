package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiUrls;

public interface XiaocaiUrlsService {
	List<XiaocaiUrls> selectByCriteria(XiaocaiUrls xiaocaiUrls);
    List<XiaocaiUrls> selectByCriteriaPagination(Criteria criteria);
    XiaocaiUrls getXiaocaiUrlsById(long id);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);
    public XiaocaiUrls add(XiaocaiUrls xiaocaiUrls);
    int updateByPrimaryKey(XiaocaiUrls xiaocaiUrls);
    public XiaocaiUrls getRandomUrls();
    List<XiaocaiUrls> getRandomUrlsByLimit(int topLimit);
    void addPoint(long urlsId,long point,int hour);
    int passBatch(int [] ids);
    public List<XiaocaiUrls> getXiaocaiUrlsListFromRedis();
    public void updataFinishedTimeZero();
}
