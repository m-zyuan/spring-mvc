package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.User;

public interface UserService {
	List<User> selectByCriteria(User user);
    List<User> selectByCriteriaPagination(Criteria criteria);
	User login(User user);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);

}
