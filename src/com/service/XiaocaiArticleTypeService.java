package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiArticleType;

public interface XiaocaiArticleTypeService {
	List<XiaocaiArticleType> selectByCriteria(XiaocaiArticleType xiaocaiArticleType);
    List<XiaocaiArticleType> selectByCriteriaPagination(Criteria criteria);
    XiaocaiArticleType selectByPrimaryKey(int Id);
    int addxiaocaiInputService(XiaocaiArticleType xiaocaiArticleType);
    int editxiaocaiInputService(XiaocaiArticleType xiaocaiArticleType);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);
}
