package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.UrlsMapper;
import com.model.Urls;
import com.service.UrlsService;
@Service
public class UrlsServiceImpl implements UrlsService{

	@Resource
	private UrlsMapper urlsMapper;
	
	@Override
	public List<Urls> selectByCriteria(Urls urls) {
		// TODO Auto-generated method stub
		 return urlsMapper.selectByCriteria(urls);
	}

	@Override
	public List<Urls> selectByCriteriaPagination(Criteria criteria) {
		return urlsMapper.selectByCriteriaPagination(criteria);
	}

	

	@Override
	public int getTotal(Criteria criteria) {
		return urlsMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		urlsMapper.deleteBatch(ids);
	}

}
