package com.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.common.Criteria;
import com.dao.XiaocaiChargeMapper;
import com.model.XiaocaiCharge;
import com.service.XiaocaiChargeService;
@Service
public class XiaocaiChargeServiceImpl implements XiaocaiChargeService{
	@Resource
	private XiaocaiChargeMapper xiaocaiChargeMapper;

	@Override
	public List<XiaocaiCharge> selectByCriteria(XiaocaiCharge xiaocaiCharge) {
		// TODO Auto-generated method stub
		return xiaocaiChargeMapper.selectByCriteria(xiaocaiCharge);
	}

	@Override
	public List<XiaocaiCharge> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiChargeMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public XiaocaiCharge selectByPrimaryKey(int Id) {
		// TODO Auto-generated method stub
		return xiaocaiChargeMapper.selectByPrimaryKey(Id);
	}

	@Override
	public int addXiaocaiChargeService(XiaocaiCharge xiaocaiCharge) {
		// TODO Auto-generated method stub
		return xiaocaiChargeMapper.insert(xiaocaiCharge);
	}

	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiChargeMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		// TODO Auto-generated method stub
		xiaocaiChargeMapper.deleteBatch(ids);
	}

	@Override
	public int updateXiaocaiChargeService(XiaocaiCharge xiaocaiCharge) {
		return xiaocaiChargeMapper.updateByPrimaryKey(xiaocaiCharge);
	}
	
	
}
