package com.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import com.common.Criteria;
import com.dao.XiaocaiUrlsMapper;
import com.google.gson.Gson;
import com.model.XiaocaiUrls;
import com.service.XiaocaiUrlsService;

@Service
public class XiaocaiUrlsServiceImpl implements XiaocaiUrlsService {

	@Resource
	private XiaocaiUrlsMapper xiaocaiUrlsMapper;
	@Resource
	private RedisBase redisBase;

	@Override
	public List<XiaocaiUrls> selectByCriteria(XiaocaiUrls xiaocaiUrls) {
		// TODO Auto-generated method stub
		return xiaocaiUrlsMapper.selectByCriteria(xiaocaiUrls);
	}

	@Override
	public List<XiaocaiUrls> selectByCriteriaPagination(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiUrlsMapper.selectByCriteriaPagination(criteria);
	}

	@Override
	public int getTotal(Criteria criteria) {
		// TODO Auto-generated method stub
		return xiaocaiUrlsMapper.countByCriteria(criteria);
	}

	@Override
	public void deleteBatch(int[] ids) {
		// TODO Auto-generated method stub
		xiaocaiUrlsMapper.deleteBatch(ids);
	}

	@Override
	public XiaocaiUrls add(XiaocaiUrls xiaocaiUrls) {
		// TODO Auto-generated method stub
		int count = xiaocaiUrlsMapper.insert(xiaocaiUrls);
		if (count > 0) {
			return xiaocaiUrls;
		} else {
			return null;
		}
	}

	@Override
	public XiaocaiUrls getXiaocaiUrlsById(long id) {
		// TODO Auto-generated method stub
		return xiaocaiUrlsMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKey(XiaocaiUrls xiaocaiUrls) {
		// TODO Auto-generated method stub
		return xiaocaiUrlsMapper.updateByPrimaryKey(xiaocaiUrls);
	}

	/***
	 * 获取随机的url进行跳转 可从redis数据库中获取数据
	 */
	@Override
	public XiaocaiUrls getRandomUrls() {
		 java.util.Random r=new java.util.Random(); 
		List<XiaocaiUrls> listXiaocaiUrls = getXiaocaiUrlsListFromRedis();
		if (null != listXiaocaiUrls && listXiaocaiUrls.size() > 0) {
			return listXiaocaiUrls.get(r.nextInt(listXiaocaiUrls.size()));
		}
		if (null == listXiaocaiUrls)
			listXiaocaiUrls = getRandomUrlsByLimit(5);
		if (null != listXiaocaiUrls && listXiaocaiUrls.size() > 0) {
			return listXiaocaiUrls.get(r.nextInt(listXiaocaiUrls.size()));
		}
		return null;
	}

	public List<XiaocaiUrls> getRandomUrlsByLimit(int topLimit) {
		// TODO Auto-generated method stub
		Criteria criteria = new Criteria();
		Map<String, Object> condition = new HashMap<String, Object>();
		// 获取当前小时
		Calendar c = Calendar.getInstance();// 可以对每个时间域单独修改
		int hour = c.get(Calendar.HOUR_OF_DAY);
		condition.put("timerHour", hour);
		condition.put("passIs", 1);//查找审核通过的网址
		criteria.setCondition(condition);
		criteria.setPageStart(0);
		criteria.setPageSize(topLimit);
		List<XiaocaiUrls> listXiaocaiUrls = xiaocaiUrlsMapper
				.selectByRandom(criteria);
		return listXiaocaiUrls;
	}

	/**
	 * 从redis中获取可以刷的网址(另外启动一个servlet线程来不断从sql中获取数据存到redis)
	 * 
	 * @return
	 */
	public List<XiaocaiUrls> getXiaocaiUrlsListFromRedis() {
		List<XiaocaiUrls> listXiaocaiUrls = new ArrayList<XiaocaiUrls>();
		Gson gson = new Gson();
		String key="urls";
		long total=50;
		//从list中pop出来50个
		if (redisBase.LISTS.llen(key)<50) {
			total=redisBase.LISTS.llen(key);
		}
		for (int i = 0; i < total; i++) {
			XiaocaiUrls temp = (XiaocaiUrls) gson.fromJson(redisBase.LISTS.lindex(key, i),XiaocaiUrls.class);
			if (null != temp) {
				listXiaocaiUrls.add(temp);
			}
		}
		//移除list中的数据
		for (int i = 0; i < total; i++) {
			redisBase.LISTS.rpop(key);
		}
		return listXiaocaiUrls;
	}

	@Override
	public void addPoint(long urlsId,long point,int hour) {
		Criteria criteria=new Criteria();
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("timerHour", hour);
		condition.put("add_points", point);
		condition.put("urlId", urlsId);
		criteria.setCondition(condition);
		xiaocaiUrlsMapper.addPoint(criteria);
	}

	@Override
	public int passBatch(int[] ids) {
		// TODO Auto-generated method stub
		return xiaocaiUrlsMapper.passBatch(ids);
	}

	@Override
	public void updataFinishedTimeZero() {
		// TODO Auto-generated method stub
		xiaocaiUrlsMapper.updataFinishedTimeZero();
	}
}
