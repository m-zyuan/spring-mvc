package com.service.impl;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.springframework.stereotype.Service;

import com.model.XiaocaiWebConfig;
import com.service.WebConfigInfoService;

@Service
public class WebConfigInfoServiceImpl implements WebConfigInfoService {
	private static Properties props;
	private static String iniPathString;

	public WebConfigInfoServiceImpl() {
		// 读取配置文件
		iniPathString = this.getClass().getClassLoader().getResource("/")
				.getPath()
				+ "app.properties";
		loadProp(iniPathString);
	}

	private static void loadProp(String filePath) {
		props = new Properties();
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(
					filePath));
			props.load(in);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 写入properties信息
	public static boolean writeProperties(String parameterName,
			String parameterValue) {
		props = new Properties();
		try {
			InputStream fis = new FileInputStream(iniPathString);
			// 从输入流中读取属性列表（键和元素对）
			props.load(fis);
			// 调用 Hashtable 的方法 put。使用 getProperty 方法提供并行性。
			// 强制要求为属性的键和值使用字符串。返回值是 Hashtable 调用 put 的结果。
			OutputStream fos = new FileOutputStream(iniPathString);
			props.setProperty(parameterName, parameterValue);
			// 以适合使用 load 方法加载到 Properties 表中的格式，
			// 将此 Properties 表中的属性列表（键和元素对）写入输出流
			props.store(fos, "Update '" + parameterName + "' value");
			return true;
		} catch (IOException e) {
			System.err.println("Visit " + iniPathString + " for updating "
					+ parameterName + " value error");
		}
		return false;
	}

	// 根据key读取value
	public static String readValue(String key) {
		if (null == props) {
			loadProp(iniPathString);
		}
		String value = props.getProperty(key);
		return value;
	}

	public XiaocaiWebConfig getXiaocaiWebConfig() {
		XiaocaiWebConfig xiaocaiWebConfig = new XiaocaiWebConfig();
		xiaocaiWebConfig.setWeb_name(readValue("web_name"));
		xiaocaiWebConfig.setWeb_domain(readValue("web_domain"));
		xiaocaiWebConfig.setLevel1_upgrade(Integer
				.valueOf(readValue("level1_upgrade")));
		xiaocaiWebConfig.setLevel2_upgrade(Integer
				.valueOf(readValue("level2_upgrade")));
		xiaocaiWebConfig.setLevel3_upgrade(Integer
				.valueOf(readValue("level3_upgrade")));
		xiaocaiWebConfig.setLevel_point_refer_four(Integer
				.valueOf(readValue("level_point_refer_four")));
		xiaocaiWebConfig.setLevel_point_refer_one(Integer
				.valueOf(readValue("level_point_refer_one")));
		xiaocaiWebConfig.setLevel_point_refer_three(Integer
				.valueOf(readValue("level_point_refer_three")));
		xiaocaiWebConfig.setLevel_point_refer_two(Integer
				.valueOf(readValue("level_point_refer_two")));

		xiaocaiWebConfig.setLevel_refer_agent1(Integer
				.valueOf(readValue("level_refer_agent1")));
		xiaocaiWebConfig.setLevel_refer_agent2(Integer
				.valueOf(readValue("level_refer_agent2")));
		xiaocaiWebConfig.setLevel_refer_agent3(Integer
				.valueOf(readValue("level_refer_agent3")));
		xiaocaiWebConfig.setLevel_refer_agent4(Integer
				.valueOf(readValue("level_refer_agent4")));
		
		xiaocaiWebConfig.setMin_buy_point(Integer
				.valueOf(readValue("min_buy_point")));
		xiaocaiWebConfig.setMin_exchange_point(Integer
				.valueOf(readValue("min_exchange_point")));
		xiaocaiWebConfig.setMin_out_pay_money(Integer
				.valueOf(readValue("min_out_pay_money")));
		xiaocaiWebConfig.setOne_rmb_exchange_point(Integer
				.valueOf(readValue("one_rmb_exchange_point")));
		xiaocaiWebConfig.setPoint_price_1(Integer
				.valueOf(readValue("point_price_1")));
		xiaocaiWebConfig.setPoint_price_2(Integer
				.valueOf(readValue("point_price_2")));
		xiaocaiWebConfig.setPoint_price_3(Integer
				.valueOf(readValue("point_price_3")));
		xiaocaiWebConfig.setPoint_price_4(Integer
				.valueOf(readValue("point_price_4")));
		xiaocaiWebConfig.setWarn_times_for_ip(Integer
				.valueOf(readValue("warn_times_for_ip")));
		xiaocaiWebConfig.setLevel1_upgrade_gift_point(Integer
				.valueOf(readValue("level1_upgrade_gift_point")));
		xiaocaiWebConfig.setLevel2_upgrade_gift_point(Integer
				.valueOf(readValue("level2_upgrade_gift_point")));
		xiaocaiWebConfig.setLevel3_upgrade_gift_point(Integer
				.valueOf(readValue("level3_upgrade_gift_point")));

		return xiaocaiWebConfig;
	}

	public boolean setXiaocaiWebConfig(XiaocaiWebConfig xiaocaiWebConfig) {
		try {
			writeProperties(("web_name"), xiaocaiWebConfig.getWeb_name());
			writeProperties(("web_domain"), xiaocaiWebConfig.getWeb_domain());
			writeProperties(("level1_upgrade"),
					String.valueOf(xiaocaiWebConfig.getLevel1_upgrade()));
			writeProperties(("level2_upgrade"),
					String.valueOf(xiaocaiWebConfig.getLevel2_upgrade()));
			writeProperties(("level3_upgrade"),
					String.valueOf(xiaocaiWebConfig.getLevel3_upgrade()));
			writeProperties(
					("level_point_refer_four"),
					String.valueOf(xiaocaiWebConfig.getLevel_point_refer_four()));
			writeProperties(("level_point_refer_one"),
					String.valueOf(xiaocaiWebConfig.getLevel_point_refer_one()));
			writeProperties(("level_point_refer_three"),
					String.valueOf(xiaocaiWebConfig
							.getLevel_point_refer_three()));
			writeProperties(("level_point_refer_two"),
					String.valueOf(xiaocaiWebConfig.getLevel_point_refer_two()));
			
			writeProperties(("level_refer_agent4"),
					String.valueOf(xiaocaiWebConfig.getLevel_refer_agent4()));
			writeProperties(("level_refer_agent3"),
					String.valueOf(xiaocaiWebConfig.getLevel_refer_agent3()));
			writeProperties(("level_refer_agent2"),
					String.valueOf(xiaocaiWebConfig.getLevel_refer_agent2()));
			writeProperties(("level_refer_agent1"),
					String.valueOf(xiaocaiWebConfig.getLevel_refer_agent1()));
			
			
			writeProperties(("min_buy_point"),
					String.valueOf(xiaocaiWebConfig.getMin_buy_point()));
			writeProperties(("min_exchange_point"),
					String.valueOf(xiaocaiWebConfig.getMin_exchange_point()));
			writeProperties(("min_out_pay_money"),
					String.valueOf(xiaocaiWebConfig.getMin_out_pay_money()));
			writeProperties(
					("one_rmb_exchange_point"),
					String.valueOf(xiaocaiWebConfig.getOne_rmb_exchange_point()));
			writeProperties(("point_price_1"),
					String.valueOf(xiaocaiWebConfig.getPoint_price_1()));
			writeProperties(("point_price_2"),
					String.valueOf(xiaocaiWebConfig.getPoint_price_2()));
			writeProperties(("point_price_3"),
					String.valueOf(xiaocaiWebConfig.getPoint_price_3()));
			writeProperties(("point_price_4"),
					String.valueOf(xiaocaiWebConfig.getPoint_price_4()));
			writeProperties(("warn_times_for_ip"),
					String.valueOf(xiaocaiWebConfig.getWarn_times_for_ip()));
			writeProperties(("level1_upgrade_gift_point"),
					String.valueOf(xiaocaiWebConfig
							.getLevel1_upgrade_gift_point()));
			writeProperties(("level2_upgrade_gift_point"),
					String.valueOf(xiaocaiWebConfig
							.getLevel2_upgrade_gift_point()));
			writeProperties(("level3_upgrade_gift_point"),
					String.valueOf(xiaocaiWebConfig
							.getLevel3_upgrade_gift_point()));
			loadProp(iniPathString);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public String getWebName() {
		// TODO Auto-generated method stub
		return readValue("web_name");
	}

	@Override
	public boolean changeWebName(String webName) {
		// TODO Auto-generated method stub
		return writeProperties("web_name", webName);
	}

}
