package com.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.dao.ModuleMapper;
import com.model.Module;
import com.service.ModuleService;

@Service
public class ModuleServiceImpl implements ModuleService{
	@Resource
	private ModuleMapper moduleMapper;

	public List<Module> selectByCriteria(Module module) {
		// TODO Auto-generated method stub
		return moduleMapper.selectByCriteria(module);
	}
	

}
