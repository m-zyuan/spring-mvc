package com.service;

import java.util.List;

import com.common.Criteria;
import com.model.XiaocaiCharge;

public interface XiaocaiChargeService {
	List<XiaocaiCharge> selectByCriteria(XiaocaiCharge xiaocaiCharge);
    List<XiaocaiCharge> selectByCriteriaPagination(Criteria criteria);
    XiaocaiCharge selectByPrimaryKey(int Id);
    int addXiaocaiChargeService(XiaocaiCharge xiaocaiCharge);
    int updateXiaocaiChargeService(XiaocaiCharge xiaocaiCharge);
    int getTotal(Criteria criteria);
    void deleteBatch(int [] ids);
}
