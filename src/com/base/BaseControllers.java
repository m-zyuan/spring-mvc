package com.base;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;
import com.common.MsgReturn;
import com.common.MyEditor;
import com.util.JSONUtil;
import com.util.URLUtils;
public abstract class BaseControllers{// extends AbstractController {
	public static final String SUCCESS="success";
	public static final String LOGIN="login";
	/**
	 * 输出JSON
	 * @param response
	 * @param jsonStr
	 */
	protected  void writerJson(HttpServletResponse response,String jsonStr) {
		writer(response,jsonStr);
	}
	/**
	 * 输出JSON
	 * @param response
	 * @param object
	 */
	protected  void writerJson(HttpServletResponse response,Object object){
		try {
			response.setContentType("text/html");
			writer(response,JSONUtil.toJson(object));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 输出HTML
	 * @param response
	 * @param htmlStr
	 */
	protected  void writerHtml(HttpServletResponse response,String htmlStr) {
		writer(response,htmlStr);
	}
	/**
	 * 获取writer
	 * @param response
	 * @param str
	 */
	protected  void writer(HttpServletResponse response,String str){
		try {
			//设置页面不缓存
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out= null;
			out = response.getWriter();
			out.print(str);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} 
	/**
	 * 输出xml
	 * @param xmlStr
	 */
	protected void outXMLString(HttpServletResponse response,String xmlStr) {
		response.setContentType("application/xml;charset=UTF-8");
		writer(response,xmlStr);
	}

	@InitBinder  
	protected void initBinder(WebDataBinder binder) {  
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));  
		binder.registerCustomEditor(int.class,new MyEditor()); 
	}  

	/**
	 * 获取IP地址
	 * @param request
	 * @return
	 */
	protected String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	/**
	 * 所有ActionMap 统一从这里获取
	 * @return
	 */
	protected Map<String,Object> getRootMap(){
		Map<String,Object> rootMap = new HashMap<String, Object>();
		rootMap.putAll(URLUtils.getUrlMap());
		return rootMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected ModelAndView forword(String viewName,Map context){
		return new ModelAndView(viewName,context); 
	}

	protected ModelAndView error(String errMsg){
		return new ModelAndView("error"); 
	}
	protected String successMsg(String msg)
	{
		 return new MsgReturn(true,msg).toString();
	}
	protected String errorMsg(String msg)
	{
		 return new MsgReturn(false,msg).toString();
	}
	
}
