package com.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

public class TestJedis {
	public static void main(String[] args) {
		try {
			//直连模式
			Jedis jedis = new Jedis("127.0.0.1");
			String keys = "name";
			// 删数据
			jedis.del(keys);
			// 存数据
			jedis.set(keys, "snowolf");
			// 取数据
			String value = jedis.get(keys);
			System.out.println(value);
			
			
			
			//连接池模式
			ApplicationContext app =  new ClassPathXmlApplicationContext("classpath:/spring*.xml"); 
			ShardedJedisPool pool; 
			pool = (ShardedJedisPool) app.getBean("shardedJedisPool");  
			// 从池中获取一个Jedis对象  
		    ShardedJedis jedis1 = pool.getResource();
		    jedis.flushDB();
		    
		    
		    
		    String keys1 = "name";  
		    String value1 = "BBBBBBBBBBBBBBBBBBBBBBBBBBB";  
		    // 删数据  
		    jedis1.del(keys1);  
		    // 存数据  
		    jedis1.set(keys1, value1);  
		    jedis1.hset("hashs", keys1, value1);
		    jedis1.hset("hashs", "vv", value);
		    // 取数据  
		    String v = jedis1.get(keys1);  
		    System.out.println(v);  		 
		    
		    
		  //打印所有的key  
		    System.out.println( jedis1.hkeys("hashs"));//列出所有的key，查找特定的key如：redis.keys("foo")
		  
		    // 释放对象池  
		    pool.returnResource(jedis1); 
			
			
			
	    	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	   
	  
	}

}
