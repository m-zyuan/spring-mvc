package com.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dao.UserMapper;
import com.model.User;
import com.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:spring.xml", "classpath:spring-mybatis.xml" })
public class MybatisTest
{
    @Resource
	private UserService service;
	//@Resource
	//private User user;
	@Resource
	private UserMapper userMapper;

	@Test
	public void selectByCriteria() {
		System.out.println("=========================");
		System.out.println(service);
		//System.out.println(user);
	    System.out.println(userMapper);
	    System.out.println("____:"+userMapper.selectByCriteria(null).size());
		
		
	}

	
}
