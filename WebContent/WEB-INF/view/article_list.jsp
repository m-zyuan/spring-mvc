<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/artlist.css">
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w978 mt10 mc b">
	<div class="left">
          <div class="w610 b">
              <div class="title">使用教程<input type="hidden" id="articleTypeIdP" name="articleTypeIdP" value="${articleTypeIdP}" /></div>
              <ul class="newslist">
              	  <c:forEach items="${list}" var="article" varStatus="vs">
              		<s:property value="#vs.index+1"/>
                  <li><span><fmt:formatDate value="${article.articleCreateDate}" type="date" dateStyle="medium"/></span>· <a href="/article/${article.id}" title="${article.articleTitle}" >${article.articleTitle}(点击：${article.articleClickTimes} 次)</a></li>
                  </c:forEach>
              </ul>
              <div class="c mt10 list_page">
              <a href="javascript:firstPage();">首页</a> <a
					href="javascript:prePage();">上一页</a> <a>第<input id="current_page"
					name="page" type="text" value="${articlePageStart}" />页</a> 共<span id="span_total">${total}</span>条
				<input type="hidden" id="pageSize" name="pageSize" value="10" /> <input
					type="hidden" id="pageStart" name="pageStart" value="${articlePageStart}" /> <a
					href="javascript:nextPage();">下一页</a> <a href="javascript:lastPage();">尾页</a>
			  </div>
          </div>
      </div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>

<script>
	function firstPage() {
		var pageSize = $("#pageSize").val();
		findPage(pageSize, 1);
	}
	function prePage() {
		var pageSize = $("#pageSize").val();
		var current_page = parseInt($("#current_page").val()) ;
		if (current_page <= 1) {
			alert("已经是第一页");
			return;
		}
		findPage(pageSize, current_page- 1);
	}
	function nextPage() {
		var pageSize = $("#pageSize").val();
		var current_page = parseInt($("#current_page").val()) ;
		var total_page = $("#span_total").html() / pageSize;
		if (current_page >= total_page) {
			alert("已经是最后一页");
			return;
		}
		findPage(pageSize, current_page+ 1);
	}
	function lastPage() {
		var pageSize = $("#pageSize").val();
		var current_page = $("#current_page").val();
		var total_page =parseInt(($("#span_total").html() / pageSize)+1);
		findPage(pageSize, total_page);
	}

	function findPage(pageSize, pageCurrent) {
		$("#pageSize").val(pageSize);
		$("#current_page").val(pageCurrent);
		$("#pageStart").val((pageCurrent-1) * pageSize);
		var url="/article_list/"+$("#articleTypeIdP").val()+"/"+pageCurrent;
		window.location.href=url;
	}
</script>
</body>
</html>