<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/home.css">
<title>${article.articleTitle}</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w978 mt10 mc b">
<h1>${article.articleTitle}</h1>
<div>${article.articleContent}</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>