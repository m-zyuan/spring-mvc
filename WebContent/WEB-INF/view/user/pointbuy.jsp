<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"regFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#inputPoint").formValidator({onShow:"请输入购买积分数",onFocus:"请输入整数",onCorrect:"正确"}).inputValidator({min:10000,max:${user.userMoney*price},type:"value",emptyError:"购买积分不能为空",onErrorMin:"购买积分最少10000",onErrorMax:"购买积分不能超过可购买积分",onError:"购买积分填写不正确，请检查"}).regexValidator({ regExp: "intege1", dataType: "enum", onError: "正整数数格式不正确" });
});
function registFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/user/pointbuy.json',
		params,
		callbackChangePassword,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('提交成功！','success', {closed:function(){location.href="/user/point";}});
	}
}
</script>
<title>积分购买</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
             <div class="subject">积分购买</div>
             <div class="p35">
                <form id="regFrom">
                <ul id="reg">
                	<li><span>注：</span><em id="userMoney">普通会员1元可购买${price1}积分</em></li>
                	<li><span>&nbsp;</span><em id="userMoney">铜牌会员1元可购买${price2}积分</em></li>
                	<li><span>&nbsp;</span><em id="userMoney">银牌会员1元可购买${price3}积分</em></li>
                	<li><span>&nbsp;</span><em id="userMoney">金牌会员1元可购买${price4}积分</em></li>
                	<li><span>可用金额：</span><em id="userMoney">${user.userMoney}</em>元</li>
                    <li><span>可购买积分：</span><em id="user_can_buy_point"><fmt:formatNumber value="${user.userMoney*price}" pattern="#.000"/></em></li>
                    <li><span>购买积分数：</span><input type="text" name="inputPoint" id="inputPoint" class="ip w01 c_password" maxlength="16" value="<fmt:formatNumber value="${user.userMoney*price}" pattern="#.000"/>" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" /><em class="tip" id="inputBankTip">请输入要购买的积分</em></li>
                    <dd><input type="button" value="确认" class="bnt_blue" name="bnt" onClick="registFormCommit();" /> <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" /></dd>
                </ul>
                </form>
             </div>
         </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>