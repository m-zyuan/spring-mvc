<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_add_url.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"addUrlFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#targetUrl").formValidator({onShow:"请输入网址",onFocus:"例如:http://www.youyangwang.com/",onCorrect:"输入正确"}).regexValidator({regExp: "url", dataType: "enum",onerror:"请确认输入的是网址！"}).inputValidator({min:12,max:250,empty:"任务网址不能为空",onError:"任务网址填写不正确"});
	<%for(int i=0;i<24;i++){%>
	$("#time<%=(i+1)%>IpCount").formValidator({ onShow: "请输入时间段IP数",onFocus:"0-100000之间", onCorrect:"正确" }).inputValidator({min:0,max:100000,type:"value",onerror:"必须在0-100000之间，请确认"});
    <%}%>
});
function addUrlsFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) 
	return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#addUrlFrom").serialize();
	$.post('add_urls.json',
		params,
		callbackRegister,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackRegister(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('添加成功！','success', {closed:function(){location.href="/user/urls_list";}});
	}
}
</script>


</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
	    <div class="b">
                <div class="subject">任务添加</div>
                <div class="add_url">
                    <form id="addUrlFrom">
                    <ul id="onlinepay">
                        <li>任务网址：<input class="input_url" type="text" name="targetUrl" id="targetUrl" class="ip" maxlength="250" value="http://" required="true" />
                        	<em id="targetUrlTip"></em>
                        </li>
                        <li>是否开启来源页：<input type="radio" name="sourceIsJump" value="1" >是<input type="radio" name="sourceIsJump" value="0" checked>否
                        	<em id="sourceIsJumpTip"></em>
                        </li>
                        <li>来源网址：<textArea name="sourceUrls" id="sourceUrls" class="ip"></textArea>
                        	<em id="sourceUrlsTip">一行一个，没有可不填</em>
                        </li>
                        <li>是否内转：<input type="radio" name="innerIsJump" value="1" >是<input type="radio" name="innerIsJump" value="0" checked>否
                        	<em id="innerIsJumpTip">内转页即网站内部跳转（增加PV）</em>
                        </li>
                        <li>充入积分数：<input type="text" name="currentPoint" id="currentPoint" class="ip" 
                        onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="10" value="10000" />
                        	<span>可用${user.userPoint}积分</span>
                        	<em id="currentPointTip">任务有足够的积分才可运行</em>
                        </li>
                        <li>时间段IP数：
                        	<div class="ip_div">
                        	<ul>
                        	<%
							for(int i=0;i<24;i++){
							%><li>
                        		<%=i%>-<%=(i+1)%>时:<input type="text" name="time<%=(i+1)%>IpCount" id="time<%=(i+1)%>IpCount" class="ip" 
                        		onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="10" value="10000" />
                        		<em id="time<%=(i+1)%>IpCountTip"></em>
                        		</li>
                        	<%
							}
							%>
							</ul>
                        	</div>
                        </li>
                         <dd><input type="button" value="添加" class="bnt_blue" name="bnt" onClick="addUrlsFormCommit();" />
                         <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" />
                        </dd>
                    </ul>
                    </form>
                
                </div>
            </div>
	    
	    
	    
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>