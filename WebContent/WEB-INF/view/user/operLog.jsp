<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<script>
function list() {
	var params = $("#listFrom").serialize();
	$.post('list_operLog.json',
		params,
		callbackRegister,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackRegister(objResult) {
	$("#span_total").html(objResult.total);
	var listHtml="";
	var list=objResult.rows;
	if(null!=list&&list.length>0){
		for(var i=0;i<list.length;i++){
			listHtml+="<tr><td  align=\"center\"><input type='checkBox' name='ids' value='"+list[i].id+"'/></td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].logDate+"</td>";
			listHtml+="<td class=\"item\" align=\"center\">"+list[i].logContent+"</td></tr>";
		}
	}
	$("#list").html(listHtml);
}
$(document).ready(function() {
	 list() ;
});
</script>
<title>操作记录</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
                <div class="subject">操作记录</div>
                <form id="listFrom">
                <div class="p12">
	     			<table id="tablelist">
                        <tr>
                        	<th width="5%">选择</th>
                            <th width="35%">操作时间</th>
                            <th width="60%">说明</th>
                        </tr>
                        <tbody id="list">
	                        <tr>
	                            <td colspan="7" class="item" align="center">没有记录</td>
	                        </tr>
                        </tbody>
                    </table>
                     <%@ include file="/resources/common/jsp/page.jsp" %>
                </div>
                </form>
        </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>