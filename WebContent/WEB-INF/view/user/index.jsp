﻿<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<title>星云流量互换系统-会员中心</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div id="position">您所在的位置：<a href="/user">会员中心</a></div>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
		  <div class="b">
                <div class="subject">个人资料</div>
                <div class="p35">
                <form onsubmit="return checkdb(this)">
                <ul id="onlinepay">
                     <li>登录用户：<span>${user.userEmail}</span></li>
                     <li>剩余积分：<span><fmt:formatNumber value="${user.userPoint}" pattern="#.000"/></span></li>
                     <li>冻结积分：<span><fmt:formatNumber value="${user.frozenPoint}" pattern="#.000"/></span></li>
                     <li>可用金额：<span><fmt:formatNumber value="${user.userMoney}" pattern="#.000"/></span></li>
                     <li>冻结金额：<span>${user.frozenMoney}</span></li>
                     <li>用户级别：<span>${user.userLevelName}</span></li>
                     <li>推广链接：<input type="text" id="refer" value="http://${domain}/reg/${user.id}" /><u>推广奖励规则：<a href="#">查看</a></u></li>
                     <li>说明：<span></span><u>1.普通会员推荐他人为代理将获取代理升级金额的<font color="red">${refer_percent1}%</font>作为奖励；获取推荐人赚取积分的<font color="red">${refer_percent1}%</font>作为奖励</u></li>
                     <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span></span><u>2.铜牌代理推荐他人为代理将获取代理升级金额的<font color="red">${refer_percent2}%</font>作为奖励；获取推荐人赚取积分的<font color="red">${refer_percent2}%</font>作为奖励</u></li>
                     <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span></span><u>3.银牌代理推荐他人为代理将获取代理升级金额的<font color="red">${refer_percent3}%</font>作为奖励；获取推荐人赚取积分的<font color="red">${refer_percent3}%</font>作为奖励</u></li>
                     <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span></span><u>4.金牌代理推荐他人为代理将获取代理升级金额的<font color="red">${refer_percent4}%</font>作为奖励；获取推荐人赚取积分的<font color="red">${refer_percent4}%</font>作为奖励</u></li>
                     <li>警告次数：<span>${user.warnedTimes}次    &nbsp;&nbsp;&nbsp;&nbsp;</span><u>注：一分钟内同一个IP刷以下赚分任务链接超过10次被警告一次，警告次数过多，帐号将被禁用，积分作废！</u></li>
                     <li>赚分任务链接：<input type="text" id="get_point_url" value="http://${domain}/urls/jump/${user.id}" /><u>赚取积分办法：<a href="#">查看</a></u></li>
                	 <li>说明：<span></span><u>普通会员刷一次任务链接赚取<font color="red">${percent1}</font>个积分，铜牌会员刷一次任务链接赚取<font color="red">${percent2}</font>个积分,银牌会员刷一次赚取<font color="red">${percent3}</font>个积分，金牌会员刷一次任务链接赚取<font color="red">${percent4}</font>个积分</u></li>
                	 
                </ul>
                </form>
                </div>
          </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>