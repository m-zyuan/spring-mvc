<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_reg.css">
<script>
$(document).ready(function() {
	$.formValidator.initConfig({formID:"regFrom",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#inputPoint").formValidator({onShow:"请输入兑换积分数",onFocus:"请输入正整数，例如10000",onCorrect:"输入正确"}).inputValidator({min:10000,max:3000000,type:"value",emptyError:"兑换积分数不能为空",onError:"兑换积分数不正确(10000到3000000之间)，请检查"}).regexValidator({ regExp: "intege1", dataType: "enum", onError: "正整数数格式不正确" }) ;
});
function registFormCommit() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	var params = $("#regFrom").serialize();
	$.post('/user/exchange.json',
		params,
		callbackChangePassword,
		'json'
	).error(function() { $.jBox.tip('服务器异常','error'); });
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		$.jBox.tip(objResult.msg,'error');
	}else{
		$.jBox.tip('提交成功！','success', {closed:function(){location.href="/user/point";}});
	}
}
function refresh(){
	setTimeout("_refresh()", 700);
}
function _refresh() {
	var src = document.getElementById("authImg").src;
	document.getElementById("authImg").src = src + "?now="
			+ new Date().getTime();
}
function changeInputPoint(){
	var input=parseFloat($("#inputPoint").val())/${oneRmbExchangePoint};
	var htmlTemp=input+"元";
	$("#money_can_able").html(htmlTemp);
}

</script>
<title>积分兑换</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
	<%@ include file="/resources/common/jsp/user_meau.jsp" %>
	<div class="right_big">
 		<div class="b">
             <div class="subject">积分兑换</div>
             <div class="p35">
                <form id="regFrom">
                <ul id="reg">
               		<li><span>可用积分：</span><fmt:formatNumber value="${user.userPoint}" pattern="#.000"/></li>
                    <li><span>兑换积分：</span><input type="text" name="inputPoint"  id="inputPoint"  value="<fmt:formatNumber value="${user.userPoint}" pattern="#.000"/>" class="ip w01 c_oldpass" maxlength="16" onkeyup="changeInputPoint();" />&nbsp;&nbsp;&nbsp;<em class="tip" id="inputPointTip">请填写兑换积分数</em></li>
                    <li><span>可兑换RMB：</span><lable id="money_can_able">${(user.userPoint/oneRmbExchangePoint)}元</lable><em class="tip">${oneRmbExchangePoint}积分可兑换1元RMB</em></li>
                    <li><span>验证码：</span><input type="text" name="authCodeReg" id="authCodeReg" class="ip w03 c_code" maxlength="4" required="true" /> 
                       <img onclick="refresh();" id="authImg" src="/authimg" align="absmiddle" /><em class="tip" id="authCodeRegTip">请输入左侧图片上的文字</em></li>
                    <dd><input type="button" value="确认" class="bnt_blue" name="bnt" onClick="registFormCommit();" /> <input type="button" value="取消" onclick="location.href='javascript:history.go(-1)'" class="bnt_blue" /></dd>
                </ul>
                </form>
             
             </div>
         </div>
	</div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>