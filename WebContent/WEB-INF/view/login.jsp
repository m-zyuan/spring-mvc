<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
<%@ include file="/resources/common/jsp/headlibs.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_login.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/user_base.css">
<script type="text/javascript">
$(document).ready(function() {
	$.formValidator.initConfig({formID:"frmLogin",onError:function(msg){$.jBox.tip(msg, 'error')}});
	$("#userEmail").formValidator({ onShow: "请输入你的email", onfocus: "请注意你输入的email格式，例如:wzmaodong@126.com", onCorrect: "谢谢你的合作，你的email正确" }).regexValidator({ regExp: "email", dataType: "enum", onError: "email格式不正确" });
	$("#userPassword").formValidator({onShow:"请输入密码",onFocus:"6-32位数字或字母",onCorrect:"输入正确"}).inputValidator({min:6,max:32,empty:{leftEmpty:false,rightEmpty:false,emptyError:"密码两边不能有空符号"},onError:"密码输入位数不正确，请检查"});
});
function doCheck() {
	if(!jQuery.formValidator.pageIsValid()) return false;
	$.jBox.tip('系统正在处理中，请稍后!','loading');
	$.ajax({
		type : 'POST',
		url : '${ctx}/login.json',
		data : $('#frmLogin').serialize(),
		dataType : 'json',
		async : false,
		error : function(request) {
			$.jBox.tip('服务器请求异常！','error');
			refresh();
		},
		success : function(data) {
			if (data.success == true||data.success == 'true') {
				$.jBox.tip('登录成功！','success', {closed:function(){location.href="${ctx}/user/index";}});	
			} else {
				$.jBox.tip(data.msg,'error');
				refresh();
			}
		}
	});
}
function resets() {
	document.frmLogin.reset();
}
function refresh() {
	setTimeout("_refresh()", 700);
}

function _refresh() {
	var src = document.getElementById("authImg").src;
	document.getElementById("authImg").src = src + "?now="
			+ new Date().getTime();
}
</script>
<title>星云流量互换系统-登录</title>
</head>
<body>
<%@ include file="/resources/common/jsp/head.jsp" %>
<div class="w980 mt10 mc">
    <div class="w900 bt">
    <form id="frmLogin" action="${ctx }/login_post" name="frmLogin"
		method="post">
       <div class="title">会员登录</div>
       <div id="userbase">
           <div class="left">
                <ul id="login">
                    
                    <li>邮箱：<input type="text" id="userEmail" name="userEmail" class="ip" required="true" /></li>
                    <li>密码：<input type="password" id="userPassword" name="userPassword" class="ip" /></li>
                    <li class="p42"><input type="button" value="登录" class="bnt_yellow" onClick="doCheck();" />
                    <input type="button" value="忘记密码" onclick="location.href='/return_pwd'" class="bnt_blue" /></li>
                </ul>
           </div>
           <div class="midline"></div>
           <div class="info">
               <p>还没有帐号？</p>
               <p class="bnt"><a href="/reg">立即注册</a></p>
               <p>想快速体验？使用以下帐号登录： </p>
           </div>
       </div>
       </form>
    </div>
</div>
<%@ include file="/resources/common/jsp/foot.jsp" %>
</body>
</html>
