<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/resources/common/jsp/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/javascript" src="${ctx}/resources/js/admin/urls.js"></script>
</head>
<body class="easyui-layout" >
<div id="body" region="center" >
    <!-- 查询条件区域 -->
    <form action="/"  id="search_form">
    <div id="search_area" >
        
    </div>
    </form>
    <!-- 数据表格区域 -->
    <table id="list" style="table-layout:fixed;" ></table>
    <!-- 表格顶部工具按钮 -->
    <div id="list_btn">
        <a href="javascript:void(0)"  id="pass" class="easyui-linkbutton" iconCls="icon-remove" plain="true">审核</a>
        <a href="javascript:void(0)"  id="delete" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
        <a href="javascript:void(0)" id="multi_check" class="easyui-linkbutton"  plain="true">多选</a>
        <a href="javascript:void(0)" id="single_check" class="easyui-linkbutton"  plain="true" disabled="true" >单选</a>
    </div>
</div>
</body>
</html>

