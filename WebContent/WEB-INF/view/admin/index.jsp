﻿<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/resources/common/jsp/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>TNT(小菜流量管理系统)</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/resources/jquery-zTree-3.5.15/css/zTreeStyle/zTreeStyle.css">
    <script type="text/javascript" src="${ctx}/resources/jquery-zTree-3.5.15/jquery.ztree.all-3.5.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/js/main.js"></script>
    
    <script type="text/javascript">
    function makeSure(){
   	    var params = $("#config_form").serialize();
    	$.post('/admin/makeConfig.json',
		params,
		callbackMakeConfig,
		'json'
	).error(function() { errorMsg('服务器异常'); });
    }
    function callbackMakeConfig(objResult) {
		if(objResult.success == false) {
			errorMsg(objResult.msg);
		}else{
			infoMsg('修改成功！');
		}
	}
    </script>
    <style type="text/css">
    .config_ul{
    	height: auto;
    }
    .config_ul span{
    	display: -moz-inline-box;
		display: inline-block;
		width: 300px;
		float:left;
    }
    .config_ul input{
    	width: 300px;
    }
    .config_ul li{
    	margin:0; padding:0; list-style-type:none;
    }
    </style>
</head>

<body class="easyui-layout">
<!-- 头部标题 -->
<div data-options="region:'north',border:false" style="height:60px; padding:5px; background:#F3F3F3">
    <a href="index"><span class="northTitle">TNT(小菜流量管理系统)</span></a>
    <span class="loginInfo">登录用户：${xiaocaiAdmin.adminName}&nbsp;&nbsp;</span>
</div>

<!-- 左侧导航 -->
<div data-options="region:'west',split:true,title:'导航菜单', fit:false,tools:'#tree_top_btn'" style="width:200px;">
    <ul id="menuTree" class="ztree">
    </ul>
</div>
<div id="tree_top_btn">
    <a href="javascript:void(0)"  id="save" class="easyui-reload" iconCls="icon-add" plain="true"></a>
</div>
<!-- 页脚信息 -->
<div data-options="region:'south',border:false"
     style="height:20px; background:#F3F3F3; padding:2px; vertical-align:middle;">
    <span id="sysVersion">系统版本：V1.0</span>
    <span id="nowTime"></span>
</div>

<!-- 内容tabs -->
<div id="center" data-options="region:'center'" >
    <div id="tabs" class="easyui-tabs" style="height: 565px;">
        <div title="首页" style="padding:5px;">
        	<form id="config_form">
            <p>网站参数配置</p>
            <ul class="config_ul">
                <li><span>网站名称:</span><input type="text" name="web_name" value="${xiaocaiWebConfig.web_name}" /></li>
                <li><span>网站域名:</span><input type="text" name="web_domain" value="${xiaocaiWebConfig.web_domain}" /></li>
                <li><span>普通会员刷一个网址获取到的积分百分比:</span><input type="text" name="level_point_refer_one" value="${xiaocaiWebConfig.level_point_refer_one}" />%</li>
                <li><span>铜牌会员刷一个网址获取到的积分百分比:</span><input type="text" name="level_point_refer_two" value="${xiaocaiWebConfig.level_point_refer_two}" />%</li>
                <li><span>银牌会员刷一个网址获取到的积分百分比:</span><input type="text" name="level_point_refer_three" value="${xiaocaiWebConfig.level_point_refer_three}" />%</li>
                <li><span>金牌会员刷一个网址获取到的积分百分比:</span><input type="text" name="level_point_refer_four" value="${xiaocaiWebConfig.level_point_refer_four}" />%</li>
                
                <li><span>普通会员推广下线为代理赚取的百分比:</span><input type="text" name="level_refer_agent1" value="${xiaocaiWebConfig.level_refer_agent1}" />%</li>
                <li><span>铜牌会员推广下线为代理赚取的百分比:</span><input type="text" name="level_refer_agent2" value="${xiaocaiWebConfig.level_refer_agent2}" />%</li>
                <li><span>银牌会员推广下线为代理赚取的百分比:</span><input type="text" name="level_refer_agent3" value="${xiaocaiWebConfig.level_refer_agent3}" />%</li>
                <li><span>金牌会员推广下线为代理赚取的百分比:</span><input type="text" name="level_refer_agent4" value="${xiaocaiWebConfig.level_refer_agent4}" />%</li>
                
                
                <li><span>普通会员花1元可购买10000积分</span><input type="text" name="point_price_1" value="${xiaocaiWebConfig.point_price_1}" /></li>
                <li><span>铜牌会员花1元可购买12000积分</span><input type="text" name="point_price_2" value="${xiaocaiWebConfig.point_price_2}" /></li>
                <li><span>银牌会员花1元可购买15000积分</span><input type="text" name="point_price_3" value="${xiaocaiWebConfig.point_price_3}" /></li>
            	<li><span>金牌会员花1元可购买18000积分</span><input type="text" name="point_price_4" value="${xiaocaiWebConfig.point_price_4}" /></li>
                <li><span>最小提现金额</span><input type="text" name="min_out_pay_money" value="${xiaocaiWebConfig.min_out_pay_money}" /></li>
                <li><span>最少购买积分数</span><input type="text" name="min_buy_point" value="${xiaocaiWebConfig.min_buy_point}" /></li>
                <li><span>最少兑换积分数10000</span><input type="text" name="min_exchange_point" value="${xiaocaiWebConfig.min_exchange_point}" /></li>
                <li><span>20000积分可以兑换1元rmb</span><input type="text" name="one_rmb_exchange_point" value="${xiaocaiWebConfig.one_rmb_exchange_point}" /></li>
                <li><span>铜牌升级价格</span><input type="text" name="level1_upgrade" value="${xiaocaiWebConfig.level1_upgrade}" /></li>
                <li><span>银牌升级价格</span><input type="text" name="level2_upgrade" value="${xiaocaiWebConfig.level2_upgrade}" /></li>
                <li><span>金牌升级价格</span><input type="text" name="level3_upgrade" value="${xiaocaiWebConfig.level3_upgrade}" /></li>
                <li><span>铜牌升级赠送积分</span><input type="text" name="level1_upgrade_gift_point" value="${xiaocaiWebConfig.level1_upgrade_gift_point}" /></li>
                <li><span>银牌升级赠送积分</span><input type="text" name="level2_upgrade_gift_point" value="${xiaocaiWebConfig.level2_upgrade_gift_point}" /></li>
                <li><span>金牌升级赠送积分</span><input type="text" name="level3_upgrade_gift_point" value="${xiaocaiWebConfig.level3_upgrade_gift_point}" /></li>
                <li><span>一个IP1分钟内刷多少次警告</span><input type="text" name="warn_times_for_ip" value="${xiaocaiWebConfig.warn_times_for_ip}" /></li>
                <li><button type="button" onClick="makeSure();">修改</button></li>
            </ul>
            
            
            
            </form>
        </div>
    </div>
</div>

<!-- 用于弹出框 -->
<div id="parent_win"></div>
<!-- tab 右击菜单 -->
<div id="mm" class="easyui-menu" style="width:120px;">
    <div id="tabupdate">刷新</div>
    <div class="menu-sep"></div>
    <div id="close">关闭</div>
    <div id="closeall">全部关闭</div>
    <div id="closeother">关闭其他</div>
    <div class="menu-sep"></div>
    <div id="closeright">关闭右侧</div>
    <div id="closeleft">关闭左侧</div>
    <div class="menu-sep"></div>
    <div id="exit">退出</div>
</div>
</body>
</html>
