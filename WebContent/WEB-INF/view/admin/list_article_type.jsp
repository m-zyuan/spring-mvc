<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/resources/common/jsp/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/javascript" src="${ctx}/resources/js/admin/list_article_type.js"></script>


</head>
<body class="easyui-layout" >
<div id="body" region="center" >
    <!-- 查询条件区域 -->
    <form action="/"  id="search_form">
    <div id="search_area" >
        
    </div>
    </form>
    <!-- 数据表格区域 -->
    <table id="list" style="table-layout:fixed;" ></table>
    <!-- 表格顶部工具按钮 -->
    <div id="list_btn">
        <a href="javascript:void(0)"  id="add" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>
        <a href="javascript:void(0)"  id="edit" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
        <a href="javascript:void(0)"  id="delete" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
        <a href="javascript:void(0)" id="multi_check" class="easyui-linkbutton"  plain="true">多选</a>
        <a href="javascript:void(0)" id="single_check" class="easyui-linkbutton"  plain="true" disabled="true" >单选</a>
    </div>
    

    
<style type="text/css">
    #fm{
        margin:0;
        padding:10px 30px;
    }
    .ftitle{
        font-size:14px;
        font-weight:bold;
        padding:5px 0;
        margin-bottom:10px;
        border-bottom:1px solid #ccc;
    }
    .fitem{
        margin-bottom:5px;
    }
    .fitem label{
        display:inline-block;
        width:80px;
    }
</style>
<div id="dlg" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
            closed="true" buttons="#dlg-buttons">
        <div class="ftitle">文章类型</div>
        <form id="fm" method="post" novalidate>
        	<input type="hidden" name="id" value="0" required="true" />
        	<div class="fitem">
                <label>类型名称:</label>
                <input name="articleTypeName" class="easyui-validatebox" required="true" />
                <em id="articleTypeNameTip"></em>
            </div>
            <div class="fitem">
                <label>类型排序:</label>
                <input name="articleTypeOrder" class="easyui-validatebox" value="0" required="true"/>
            	<em id="articleTypeOrderTip"></em>
            </div>
        </form>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveArticleType()">确定</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">取消</a>
    </div>
</div>
        
    
</div>
</body>
</html>

