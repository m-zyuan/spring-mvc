<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/resources/common/jsp/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/javascript" src="${ctx}/resources/js/admin/list_article.js"></script>
	<link rel="stylesheet" href="${ctx}/resources/common/kindeditor-4.1.7/themes/default/default.css" />
	<script charset="utf-8" src="${ctx}/resources/common/kindeditor-4.1.7/kindeditor-min.js"></script>
	<script charset="utf-8" src="${ctx}/resources/common/kindeditor-4.1.7/lang/zh_CN.js"></script>
	<script>
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="articleContent"]', {
				uploadJson : '/admin/upload_json.json',
				allowFileManager : true,
				items : ['source','fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', 'clearhtml','|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons', 'image', 'link'],
				afterBlur: function(){this.sync();}
			});
		});
	</script>


</head>
<body class="easyui-layout" >
<div id="body" region="center" >
    <!-- 查询条件区域 -->
    <form action="/"  id="search_form">
    <div id="search_area" >
        
    </div>
    </form>
    <!-- 数据表格区域 -->
    <table id="list" style="table-layout:fixed;" ></table>
    <!-- 表格顶部工具按钮 -->
    <div id="list_btn">
        <a href="javascript:void(0)"  id="add" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>
        <a href="javascript:void(0)"  id="edit" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
        <a href="javascript:void(0)"  id="delete" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
        <a href="javascript:void(0)" id="multi_check" class="easyui-linkbutton"  plain="true">多选</a>
        <a href="javascript:void(0)" id="single_check" class="easyui-linkbutton"  plain="true" disabled="true" >单选</a>
    </div>
</div>
    
<style type="text/css">
    #fm{
        margin:0;
        padding:10px 30px;
    }
    .ftitle{
        font-size:14px;
        font-weight:bold;
        padding:5px 0;
        margin-bottom:10px;
        border-bottom:1px solid #ccc;
    }
    .fitem{
        margin-bottom:5px;
    }
    .fitem label{
        display:inline-block;
        width:80px;
    }
</style>
<div id="dlg" class="easyui-dialog" style="width:750px;height:480px;padding:10px 20px"
            closed="true" buttons="#dlg-buttons">
        <div class="ftitle">文章</div>
        <form id="fm" method="post" novalidate>
        	<input type="hidden" name="id" value="0" required="true" />
        	<div class="fitem">
                <label>标题:</label>
                <input name="articleTitle" class="easyui-validatebox" required="true" />
                <em id="articleTypeNameTip"></em>
            </div>
            <div class="fitem">
                <label>类型:</label>
               	<select name="articleTypeIdP" id="articleTypeIdP"> 
               		<c:forEach items="${typeList}" var="type" varStatus="vs">
               			<option value="${type.id}" >${type.articleTypeName}</option> 
               		</c:forEach>
		        </select>
            </div>
            <div class="fitem">
                <label>内容:</label>
                <textArea name="articleContent" id="articleContent" style="width:500px;height:350px;visibility:hidden;"></textArea>
            	<em id="articleTypeOrderTip"></em>
            </div>
        </form>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveArticleType()">确定</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">取消</a>
    </div>
</div>

</body>
</html>

