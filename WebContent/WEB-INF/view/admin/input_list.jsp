<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/resources/common/jsp/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/javascript" src="${ctx}/resources/js/admin/input_list.js"></script>
</head>
<body class="easyui-layout" >
<div id="body" region="center" >
    <!-- 查询条件区域 -->
    <form action="/"  id="search_form">
    <div id="search_area" >
        
    </div>
    </form>
    <!-- 数据表格区域 -->
    <table id="list" style="table-layout:fixed;" ></table>
    <!-- 表格顶部工具按钮 -->
    <div id="list_btn">
        <a href="javascript:void(0)"  id="deal" class="easyui-linkbutton" iconCls="icon-remove" plain="true">审核</a>
        <a href="javascript:void(0)"  id="delete" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
        <a href="javascript:void(0)" id="multi_check" class="easyui-linkbutton"  plain="true">多选</a>
        <a href="javascript:void(0)" id="single_check" class="easyui-linkbutton"  plain="true" disabled="true" >单选</a>
    </div>
    
    
<style type="text/css">
    #fm{
        margin:0;
        padding:10px 30px;
    }
    .ftitle{
        font-size:14px;
        font-weight:bold;
        padding:5px 0;
        margin-bottom:10px;
        border-bottom:1px solid #ccc;
    }
    .fitem{
        margin-bottom:5px;
    }
    .fitem label{
        display:inline-block;
        width:80px;
    }
</style>
<div id="dlg" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
            closed="true" buttons="#dlg-buttons">
        <div class="ftitle">充值/提现</div>
        <form id="fm" method="post" novalidate>
        	<input type="hidden" name="id" value="0" required="true" />
        	<input type="hidden" id="isPass" name="isPass"  value="0" />
        	<div class="fitem">
                <label>用户ID:</label>
                <input name="userIdP" class="easyui-validatebox" required="true" readOnly />
            </div>
            <div class="fitem">
                <label>充值/提现金额:</label>
                <input name="inputMoney" class="easyui-validatebox" required="true" readOnly />
            </div>
            <div class="fitem">
                <label>账户:</label>
                <input name="inputBank" class="easyui-validatebox" required="true" readOnly />
            </div>
            <div class="fitem">
                <label>姓名:</label>
                <input name="inputUserName" class="easyui-validatebox" value="0" required="true"/>
            </div>
        </form>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="pass(1)">通过</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="pass(0)">不通过</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">取消</a>
    </div>
</div>
    
</div>
</body>
</html>

