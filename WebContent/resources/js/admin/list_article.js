var url="";
$(function(){
    var list = $('#list');
    $("#list").datagrid({
        height:$("#body").height()-$('#search_area').height()-5,
        width:$("#body").width(),
        idField:'id',
        url:"list_article.json",
        singleSelect:true,
        nowrap:true,
        fitColumns:true,
        rownumbers:true,
        columns:[[
            {field:'id',title:'ID',width:100,halign:"center", align:"left",hidden:true},
            {field:'checkbox',title:'checkbox',width:100,halign:"center", align:"left",checkbox:true},
            {field:'articleTitle',title:'标题',width:100,halign:"center", align:"left"},
            {field:'articleTypeIdP',title:'文章类型',width:100,halign:"center", align:"left"}
        ]],
        toolbar:'#list_btn',
        pagination:true,
        onDblClickRow:function(rowIndex, rowData){
            viewDetail(rowData.id);
        }
    });
    $("#add").on("click", function(){
    	$('#dlg').dialog('open').dialog('setTitle','新增文章');
    	$('#fm').form('clear');
        url = 'add_article.json';
	});
    $("#edit").on("click", function(){
    	var row = $('#list').datagrid('getSelected');
        if (row){
	    	$('#dlg').dialog('open').dialog('setTitle','修改文章');
	    	//$('#fm').form('load',row);
	        url = 'edit_article.json';
	        $.post('get_article.json',
	        		"id="+row.id,
	        		callbackGetArticle,
	        		'json'
	        ).error(function() { errorMsg("服务器异常");});
        }
    });
    //删除
    $("#delete").on("click", function(){
        //获取选中的数据
        var ids = getSelections(list);
        confirmMsg('确定删除这'+ids.length+'条数据',function(){
            $.ajax({
                type: 'POST',//请求方式
                url: 'delete_article.json',//请求的action路径
                data: "ids="+$.toJSON(ids),//请求参数
                dataType: 'json',//返回参数类型
                error: function () {//请求失败处理函数
                    errorMsg('请求失败');
                },
                success: function (data) { //请求成功后处理函数。
                    if(data.success)
                    {
                        infoMsg(data.msg);
                        //刷新datalist
                        list.datagrid('reload');
                    }else
                    {
                        errorMsg(data.msg);
                    }
                }
            });
        })
    });
    //多选
    $("#multi_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:false});
        $(this).linkbutton('disable');
        $('#single_check').linkbutton('enable');
    });
    //单选
    $("#single_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:true});
        $(this).linkbutton('disable');
        $('#multi_check').linkbutton('enable');
    });
    //查询按钮事件
    $("#search_btn").click(function(){
        list.datagrid('load',{
            "condition['account']":$("#account").val(),
            "condition['sex']":$("#sex").val()
        });

    });
    //重置按钮
    $("#reset_btn").click(function(){
        $("#search_form")[0].reset();
    });
});
$(document).ready(function() {
	$.formValidator.initConfig({formID:"fm",onError:function(msg){errorMsg(msg);}});
	$("#articleTitle").formValidator({onShow:"请输入文章类型",onFocus:"请输入正浮点数，例如1",onCorrect:"输入正确"}).inputValidator({min:1,max:100,emptyError:"文章类型不能为空",onError:"文章类型不正确，请检查"});
	//$("#articleContent").formValidator({onShow:"请输入文章类型排序",onFocus:"例如：123",onCorrect:"输入正确"}).inputValidator({min:1,max:10,type:"value",emptyError:"不能为空",onError:"类型排序"});
});
function saveArticleType(){
	if(editor.isEmpty()){
		errorMsg("内容不能为空");
		return false;
	}
	if(!jQuery.formValidator.pageIsValid()) return false;
	var params = $("#fm").serialize();
	$.post(url,
		params,
		callbackChangePassword,
		'json'
	).error(function() { errorMsg("服务器异常");});
}
function callbackChangePassword(objResult) {
	if(objResult.success == false) {
		infoMsg(objResult.msg);
	}else{
		infoMsg(objResult.msg);
		$("#dlg").dialog("close");
        $("#list").datagrid("reload");
	}
}
function getSelections(list){
    var ids = [];
    var rows = list.datagrid('getSelections');
    for(var i=0; i<rows.length; i++){
        ids.push(rows[i].id);
    }
    return ids;
 }


function callbackGetArticle(objResult) {
	$('#fm').form('load',objResult);
	editor.html(objResult.articleContent); 
}
function viewDetail(date, id){
    $parent.messager.alert("提示","查询详细", "info");
}

//监听窗口大小变化
window.onresize = function(){
    setTimeout(domresize,300);
};
//改变表格宽高
function domresize(){
    $('#list').datagrid('resize',{
        height:$("#body").height()-$('#search_area').height()-5,
        width:$("#body").width()
    });
}