var url="";
$(function(){
    var list = $('#list');
    $("#list").datagrid({
        height:$("#body").height()-$('#search_area').height()-5,
        width:$("#body").width(),
        idField:'id',
        url:"user_list.json",
        singleSelect:true,
        nowrap:true,
        fitColumns:true,
        rownumbers:true,
        columns:[[
            {field:'id',title:'ID',width:100,halign:"center", align:"left",hidden:true},
            {field:'checkbox',title:'checkbox',width:100,halign:"center", align:"left",checkbox:true},
            {field:'userEmail',title:'用户名',width:100,halign:"center", align:"left"},
            {field:'userQQ',title:'QQ',width:100,halign:"center", align:"left"},
            /*{field:'sex',title:'性别',width:100,halign:"center", align:"left",
                formatter:function(value){
                    if(value == 1){
                        return "男";
                    }
                    if(value == 0){
                        return "女";
                    }
                }},*/
            {field:'userPoint',title:'积分',width:100,halign:"center", align:"left"},
            {field:'frozenPoint',title:'冻结积分',width:100,halign:"center", align:"left"},
            {field:'userMoney',title:'金额',width:100,halign:"center", align:"left"},
            {field:'frozenMoney',title:'冻结金额',width:100,halign:"center", align:"left"},
            {field:'userLevelName',title:'用户等级',width:100,halign:"center", align:"left"}
        ]],
        toolbar:'#list_btn',
        pagination:true,
        onDblClickRow:function(rowIndex, rowData){
            viewDetail(rowData.id);
        }
    });

    //修改
    $("#update").on("click", function(){
        //$('#list').datagrid('clearSelections');
        var row = $('#list').datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('setTitle','充值加积分');
            $('#fm').form('load',row);
            url = 'add_point_update_user.json';
        }
        //$parent.messager.alert("提示","update", "info");
    });
    
    //删除
    $("#delete").on("click", function(){
        //获取选中的数据
        var ids = getSelections(list);
        confirmMsg('确定删除这'+ids.length+'条数据',function(){
            $.ajax({
                type: 'POST',//请求方式
                url: 'delete',//请求的action路径
                data: "ids="+$.toJSON(ids),//请求参数
                dataType: 'json',//返回参数类型
                error: function () {//请求失败处理函数
                    errorMsg('请求失败');
                },
                success: function (data) { //请求成功后处理函数。
                    if(data.success)
                    {
                        infoMsg(data.msg);
                        //刷新datalist
                        list.datagrid('reload');
                    }else
                    {
                        errorMsg(data.msg);
                    }
                }
            });
        })
    });
    //多选
    $("#multi_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:false});
        $(this).linkbutton('disable');
        $('#single_check').linkbutton('enable');


    });
    //单选
    $("#single_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:true});
        $(this).linkbutton('disable');
        $('#multi_check').linkbutton('enable');
    });
    //查询按钮事件
    $("#search_btn").click(function(){
        list.datagrid('load',{
            "condition['userEmail']":$("#userEmail").val()
        });

    });
    //重置按钮
    $("#reset_btn").click(function(){
        $("#search_form")[0].reset();
    });
});

$(document).ready(function() {
	$.formValidator.initConfig({formID:"fm",onError:function(msg){errorMsg(msg);}});
	$("#userMoney").formValidator({onShow:"金额不能为空",onFocus:"例如1",onCorrect:"输入正确"});
	$("#userPoint").formValidator({onShow:"积分不能为空",onFocus:"例如1",onCorrect:"输入正确"});
});
function saveUser(){
	if(!jQuery.formValidator.pageIsValid()) return false;
	var params = $("#fm").serialize();
	$.post(url,
		params,
		callbacksaveUser,
		'json'
	).error(function() { errorMsg("服务器异常");});
}
function callbacksaveUser(objResult) {
	if(objResult.success == false) {
		infoMsg(objResult.msg);
	}else{
		infoMsg(objResult.msg);
		$("#dlg").dialog("close");
        $("#list").datagrid("reload");
	}
}








function viewDetail(date, id){
    $parent.messager.alert("提示","查询详细", "info");
}

//监听窗口大小变化
window.onresize = function(){
    setTimeout(domresize,300);
};
//改变表格宽高
function domresize(){
    $('#list').datagrid('resize',{
        height:$("#body").height()-$('#search_area').height()-5,
        width:$("#body").width()
    });
}