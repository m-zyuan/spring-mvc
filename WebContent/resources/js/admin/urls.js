$(function(){
    var list = $('#list');
    $("#list").datagrid({
        height:$("#body").height()-$('#search_area').height()-5,
        width:$("#body").width(),
        idField:'urlId',
        url:"urls_list.json",
        singleSelect:true,
        nowrap:true,
        fitColumns:true,
        rownumbers:true,
        columns:[[
            {field:'urlId',title:'ID',width:100,halign:"center", align:"left",hidden:true},
            {field:'checkbox',title:'checkbox',width:100,halign:"center", align:"left",checkbox:true},
            {field:'targetUrl',title:'目标网址',width:100,halign:"center", align:"left"},
            {field:'sourceIsJump',title:'是否有来源页',width:100,halign:"center", align:"left",
                formatter:function(value){
                    if(value == 1){
                        return "是";
                    }
                    if(value == 0){
                        return "否";
                    }
                }},
            {field:'innerIsJump',title:'是否内转',width:100,halign:"center", align:"left",
                formatter:function(value){
                    if(value == 1){
                        return "是";
                    }
                    if(value == 0){
                        return "否";
                    }
                }},
            {field:'passIs',title:'是否审核',width:100,halign:"center", align:"left",
                formatter:function(value){
                    if(value == 1){
                        return "是";
                    }
                    if(value == 0){
                        return "否";
                    }
                }}
        ]],
        toolbar:'#list_btn',
        pagination:true,
        onDblClickRow:function(rowIndex, rowData){
            viewDetail(rowData.id);
        }
    });
    //审核pass
    $("#pass").on("click", function(){
        //获取选中的数据
        var ids = getSelections(list);
        confirmMsg('确定审核通过这'+ids.length+'条数据',function(){
            $.ajax({
                type: 'POST',//请求方式
                url: 'url_pass.json',//请求的action路径
                data: "ids="+$.toJSON(ids),//请求参数
                dataType: 'json',//返回参数类型
                error: function () {//请求失败处理函数
                    errorMsg('请求失败');
                },
                success: function (data) { //请求成功后处理函数。
                    if(data.success)
                    {
                        infoMsg(data.msg);
                        //刷新datalist
                        list.datagrid('reload');
                    }else
                    {
                        errorMsg(data.msg);
                    }
                }
            });
        })
    });
    //删除
    $("#delete").on("click", function(){
        //获取选中的数据
        var ids = getSelections(list);
        confirmMsg('确定删除这'+ids.length+'条数据',function(){
            $.ajax({
                type: 'POST',//请求方式
                url: 'url_delete.json',//请求的action路径
                data: "ids="+$.toJSON(ids),//请求参数
                dataType: 'json',//返回参数类型
                error: function () {//请求失败处理函数
                    errorMsg('请求失败');
                },
                success: function (data) { //请求成功后处理函数。
                    if(data.success)
                    {
                        infoMsg(data.msg);
                        //刷新datalist
                        list.datagrid('reload');
                    }else
                    {
                        errorMsg(data.msg);
                    }
                }
            });
        })
    });
    //多选
    $("#multi_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:false});
        $(this).linkbutton('disable');
        $('#single_check').linkbutton('enable');
    });
    //单选
    $("#single_check").on("click", function(){
        clearSelections(list);
        list.datagrid({singleSelect:true});
        $(this).linkbutton('disable');
        $('#multi_check').linkbutton('enable');
    });
    //查询按钮事件
    $("#search_btn").click(function(){
        list.datagrid('load',{
            "condition['account']":$("#account").val(),
            "condition['sex']":$("#sex").val()
        });

    });
    //重置按钮
    $("#reset_btn").click(function(){
        $("#search_form")[0].reset();
    });
})

function getSelections(list){
    var ids = [];
    var rows = list.datagrid('getSelections');
    for(var i=0; i<rows.length; i++){
        ids.push(rows[i].urlId);
    }
    return ids;
 }







function viewDetail(date, id){
    $parent.messager.alert("提示","查询详细", "info");
}

//监听窗口大小变化
window.onresize = function(){
    setTimeout(domresize,300);
};
//改变表格宽高
function domresize(){
    $('#list').datagrid('resize',{
        height:$("#body").height()-$('#search_area').height()-5,
        width:$("#body").width()
    });
}