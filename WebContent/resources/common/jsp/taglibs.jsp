<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!-- 通用标签 -->
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@ taglib prefix="fn" uri="/WEB-INF/tld/fn.tld"%>
<%@ taglib prefix="fmt" uri="/WEB-INF/tld/fmt.tld"%>
<!-- 通用变量 -->
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="user" value="${sessionScope.CURRENT_USER}" />
<c:set var="xiaocaiAdmin" value="${sessionScope.CURRENT_ADMIN}" />

<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/default.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/jquery-easyui-1.3.5/themes/gray/easyui.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/jquery-easyui-1.3.5/themes/icon.css" />
<!-- Jquery -->
<script type="text/javascript" src="${ctx}/resources/common/jquery/jquery-1.10.2.min.js"></script>
<!-- jquery-json 插件 -->
<script type="text/javascript" src="${ctx}/resources/common/jquery/jquery.json-2.4.min.js"></script>
<!-- jquery-easyui -->
<script type="text/javascript" src="${ctx}/resources/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/jquery-easyui-1.3.5/locale/easyui-lang-zh_CN.js"></script>
<!-- easyui扩展 -->
<script type="text/javascript" src="${ctx}/resources/common/js/extends.js"></script>
<!-- formvalidator -->
<link type="text/css" href="${ctx}/resources/common/formvalidator/style/validator.css" rel="stylesheet"/>
<script type="text/javascript" src="${ctx}/resources/common/formvalidator/formValidator-4.0.1.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="${ctx}/resources/common/formvalidator/formValidatorRegex.js" charset="UTF-8"></script>


<!-- 公用方法 -->
<script type="text/javascript" src="${ctx}/resources/common/js/common.js"></script>
<script type="text/javascript">
<!--easyui封装的提醒消息-->
function infoMsg(msg,fun)
{
	$.messager.alert('消息提醒',msg,'info',fun );
}
function errorMsg(msg,fun)
{
	$.messager.alert('错误提醒', msg,'error',fun);
}
function warningMsg(msg,fun)
{
	$.messager.alert('警告提醒',msg,'warning',fun);
}
function confirmMsg(msg,fun)
{
	$.messager.confirm('消息提醒', msg,fun );
}
function showMsg(msg,fun)
{
	$.messager.alert({
		showType:'slide',   //定义如何将显示消息窗口。可用的值是：null,slide,fade,show。默认值是slide。
		showSpeed:'600',    //定义消息窗口完成的时间（以毫秒为单位）， 默认值600。
		width:'250',        //定义消息窗口的宽度。 默认值250。
		height:'100',       //定义消息窗口的高度。 默认值100。
		msg:msg,            //定义显示的消息文本。
		title:'友情提醒',   //定义显示在标题面板显示的标题文本。
		timeout:'500'       //如果定义为0，消息窗口将不会关闭，除非用户关闭它。如果定义为非0值，当超时后消息窗口将
	});
} 
</script>

