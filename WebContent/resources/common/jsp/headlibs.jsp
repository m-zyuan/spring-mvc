<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!-- 通用标签 -->
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@ taglib prefix="fn" uri="/WEB-INF/tld/fn.tld"%>
<%@ taglib prefix="fmt" uri="/WEB-INF/tld/fmt.tld"%>
<!-- 通用变量 -->
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="user" value="${sessionScope.CURRENT_USER}" />
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/base.css">
<link rel="stylesheet" type="text/css" href="${ctx}/resources/common/css/public.css">

<!-- Jquery -->
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>

<link type="text/css" href="${ctx}/resources/common/formvalidator/style/validator.css" rel="stylesheet"/>
<script type="text/javascript" src="${ctx}/resources/common/formvalidator/formValidator-4.0.1.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="${ctx}/resources/common/formvalidator/formValidatorRegex.js" charset="UTF-8"></script>
<script type="text/javascript" src="${ctx}/resources/common/jBox/jquery.jBox-2.3.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/common/jBox/i18n/jquery.jBox-zh-CN.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/resources/common/jBox/Skins2/Blue/jbox.css"/>

