<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="usermenu">
    <div class="b">
        <div class="subject_user">会员中心</div>
        <ul>
            <li id="user_menu_01"><a href="/user">会员首页</a></li>
            <li id="user_menu_01"><a href="/user/level">会员升级</a></li>
            <li id="user_menu_01"><a href="/user/myrefer">我的推广</a></li>
        </ul>
     </div>
     <div class="b mt5">
        <div class="subject_user">积分财务</div>
        <ul>
            <li id="user_menu_02"><a href="/user/pay">在线充值</a></li>
            <li id="user_menu_02"><a href="/user/payout">在线提现</a></li>
            <li id="user_menu_03"><a href="/user/money">财务明细</a></li>
            <li id="user_menu_04"><a href="/user/point_buy">积分购买</a></li>
            <li id="user_menu_04"><a href="/user/exchange">积分兑换</a></li>
            <li id="user_menu_04"><a href="/user/point_move">积分转账</a></li>
            <li id="user_menu_04"><a href="/user/accept_point">接收积分</a></li>
            <li id="user_menu_05" class="bn"><a href="/user/point">积分明细</a></li>
        </ul>
     </div>
     <div class="b mt5">
        <div class="subject_user">任务管理</div>
        <ul>
            <li id="user_menu_06"><a href="/user/add_url">添加流量任务</a></li>
            <li id="user_menu_07"><a href="/user/urls_list">管理流量任务</a></li>
            <li style="display:none;" id="user_menu_08"><a href="#">添加CPA任务</a></li>
            <li style="display:none;" id="user_menu_09"><a href="#">管理CPA任务</a></li>
        </ul>
     </div>
      <div class="b mt5">
        <div class="subject_user">会员信息</div>
        <ul>
            <li id="user_menu_12"><a href="/user/password">修改密码</a></li>
            <li id="user_menu_12"><a href="/user/operLog">操作记录</a></li>
            
            <li class="bn"><a href="/logout">退出登录</a></li>
        </ul>
     </div>
</div>


